# Culture
------

> Angular c'est un **Freamwork** javascript qui nous permet de développer **des singles pages application**. **L'acronyme** c'est **SPA**.

> Sa première version nommé **angular js** est publié par **miss ko en 2010** donc elle s'appelais **angular js** elle pert le **js**, car la mise à jour entre la version 1 jusqu'à la version2 étais impossible. Il ya eu tellement de modification sur **angular 2** qu'on a du changer le nom pour **angular et perdre le js**.

> Donc presque depuis sa création **google sponsorise et soutient ce projet open source**.

> GOOGLE utilise activement ce projet, pour des **projets en interne et public**. On peut dire que **angular** c'est la **techno web de google** et **React la techno web de facebook**.

> Pour développer une **application angular** il faut utiliser **le langage de programmation typescript**.
-----
- Une single page application est principalement **composé de javascipt**. La première caractéristique c'est que **la single page application va être exécuté coté client**, le serveur va nous renvoyer du **javascript** et c'est le navigateur qui va exécuter ce **javascript**.

- Donc le **côté client ça peut être soit le navigateur de l'ordinateur de l'utilisateur**, soit le **navigateur du téléphone de l'utilisateur** ou le **téléphone de l'utilisateur**.

- Donc c'est du **javascript** qui va être exécuté **sur l'ordinateur de l'utilisateur** et non pas sur **nos serveurs**.

- Le **deuxième point** c'est qu'une **single page application** est **une seule page qu'on charge** et c'est **une petite coquille de html** et **beaucoup de javascript** et tout le **javascript contient notre application sera exécuté sur le client de l'utilisateur**.

> Comme le **javascript contient tous les changements de route** (les pages, les composants..) on n'a besoin de charger **qu'une seule page**.
-----
> Angular nous **permet de développer des singles page, des applications qui sont chargé dans une seule page**.
-----

- Ensuite il va avoir **un rafraîchissement partiel, c'est le framework javascript qui va identifier les interactions** (qui est la nécessité de rafraîchir la page à certains endroits) .

> Dans une **navigation normale c'est le navigateur qui va renvoyer une page html**. Dans notre cas **on ne demande jamais à notre serveur de nous renvoyer du html**, en demandent l'application **c'est ensuite l'application qui se charge de mettre à jour le html**.
----

- Troisième point c'est qu'une **single page application** va être en **liaison avec un serveur** (elle va communiquer avec un serveur) avec **des requêtes http**. Car le premier et le deuxième point c'est **qu'une single page est exécuté sur l'ordinateur du client**, et on **ne va pas lui donner accès à notre page de données** (**il ne va pas pouvoir écrire et lire dans notre base de données**).
-----
- Donc pour cela on doit **communiquer notre application single page**, **communiquer avec un serveur via une api**, et c'est ce **Serveur là qui va se charger d'enregistrer, de lire des données ou réaliser des calculs qui ne sont pas possibles par l'ordinateur ou le téléphone de l'utilisateur**.

Exemple: 

         - Faire des Calculs de prévision bancaire.
      
         - Faire des calculs de prévisions météologiques.

         - Autre complexes au Calculs.
----
> Les projets très favorisée par l'utilisation d'**angular** ce sont **des projets D'application web**.

- Cela peut être:

         - Un CRM,

         - Une interface,

         - Une application de gestion de compte bancaire,

         - Une application de gestion de facture etc..
-----
> TOUTES LES APPLICATIONS XEB QUI ON DES INTERFACES COMPLEXES QUI NÉCESSITENT DES RÉACTIONS DE L'INTÉRACTIONS ETC... PEUVENT ÊTRE GÉRÉ AVEC ANGULAR.

> ANGULAR EST TRÈS UTILE POUR LE DÉVELOPPEMENT DES APPLICATIONS WEB.

-----
- Le développement du site **e- commerce**, **c'est la récupération de produits et l'enregistrement d'achat.** Angular va **être utilisé comme interface**. On va **développer une interface pour afficher les produits, afficher les pages produits** et **interagir avec les produits, donc les mettre dans le shop, afficher notre shop et afficher un moyen de paiement.**

- On peut très bien utiliser angular pour développer des **dashbords**, qui vont **Récupérer des données de source différentes et les afficher sur une interface**, par rapport à tout ce qu'on n'a vu **il ya des inconvénients à utiliser angular et à développer une single page application**.

- **On ne va pas utiliser angular pour développer un site web basé sur du contenu**, car **il a des anciens navigateurs qui ne sont pas capables d'exécuter du javascript**, et sur **le référencement (les sites de contenues, de blog etc..) le fait que **tout notre site soit du javascript cela pose aussi problème**. Donc sur certains **moteur de recherche on ne pourrait pas être référencé** puisque notre **single page application c'est une seule page qui est chargé dans du javascript et il faut que les moteurs de recherches sache comprendre le javascript**.
----
- lE DEUXIÈME TYPE DE PROJET SERAIS EXEMPLE: SI VOUS AVEZ DÉJÀ UN SITE WEB BASIC AVEC PHP PAR EXEMPLE ET VOUS VOULEZ DYNAMISER UN ONGLET, POUR CELA IL FAUDRAIT CHARGER TOUT LE FREAMWORK ANGUILAR CELA EST BEAUCOUP TROP.

> On utilise angular pour **pour une seule partie, pour développer une application complète** et **non une petite partie sur un site déjà existant**( pour cela il va y avoir d'autre techno qui seront préférable d'utiliser).
> 
> // Rappel\\  => **Il faut choisit la technologie en fonction du besoin**.

-----
> lE SITE OFFICIEL D'ANGULAR => [https://angular.io]

> On y retrouve les **fonctionnalités proposées** => [https://angular.io/features]

        - On peut développer UNE PROGRESSIVE WEB APPLICATION qui peut être aussi bien sur les navigateurs, sur le navigateur du téléphone mais aussi télécharger, AVEC UN + ON PEUT INSTALLER L'APPLICATION.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-08%20à%2015.36.48.png)

        - On peut télécharger sur le téléphone une APPLICATION PROGRESSIVE WEB APPS, c'est un raccourci à l'application web. Don on n'a UN SEUL CODE qui est accessible sur le navigateur via TELEPHONE ET ORDINATEUR donc cela est PROGRESSIVE.

        - On peut faire du NATIF, pour cela il faut utiliser d'autre techno comme CORDOVA ET IONIC ou NATIFSCRIPT.

        - On peut faire du DESKTOP, 
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-08%20à%2015.49.22.png)

        - On peut faire de l'animation, tasting etc..

        - On n'a une partie documentation 
> Lien de la documentation angular =>[https://angular.io/docs]

- Un GETTING STARTED qui nous permet de nous mettre avec le FREAMWORK => LIEN [https://angular.io/guide/setup-local]

- On va avoir une documentation avec les éléments les plus poussées.

> Il faut savoir que la communauté d'angular est très forte, il y a énormément d'article, énormément de vidéo sur angular, car **la communauté d'angular est très active et elle défend très bien typescript**.

> Lien de la video => [https://www.youtube.com/watch?v=AFyW-uGxHlc]
        








