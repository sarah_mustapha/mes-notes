#Event Binding
> **La liaison des événements d'angular**

- Pour mettre à jour le html **angular va écouter toutes les modifications qui sont faite dans notre composant**

- Et pour lui dire de remettre à jour on peut lui **lier des événements** dont on **n'a vu javascript natif ajouter des écoutes d'événement à des éléments html**. Donc là on va voir la même chose avec la syntaxe **angular**.
----
- J'ai un élément **html button**, pour cela en attribut avant la fermeture chevron je dois lui indiquer **quelle est l'événement que je vais écouter**, pour cela il faut **que j'ouvre des parenthèses et que je les ferme**.

- Entre ses parenthèses **il faut que je lui indique quelle événement que je veux écouter**, donc dans mon cas je veux **écouter lorsque je click**, je**change la couleur**

- Pour cela il faut que je **le déclare** la fonction **Change color** et cette **fonction va afficher une alerte qui dit button cliqué**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-24%20à%2020.16.12.png)
  
- Dans **mon composant AppComponent je vais lui déclarer la fonction changeColor**

> Donc là on voit bien que la syntaxe à changer de couleur.

> Donc là **je lie l'événement click**, cela peut être **un autre événement**

- Moi je veux écouter click et au click cette fonction **Va afficher une alerte qui dit button cliqué**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-25%20à%2012.35.48.png)
  
- **J'ai pu lier un événement qui est l'événement du clique**

------

-**À la place de changer une alerte je peux changer le titre**
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-25%20à%2012.42.57.png)

> ATTENTION **au placement de syntaxe, a ne pas mettre des crochets autours du click! car les crochets cest pour les propriétés**

- **Pour écouter un événement on utilise des parenthèses**
----

-** Dans mon second exemple est d'écouter les modifications de mon INPUT**.

- Avant toute chose je vais déclarer une propriété **text qui sera une chaine de caractère vide**.

- Maintenant dans mon html je **Vais vouloir interpoler / afficher ma variable text avec des accolades**

> **Pout écouter un événement j'ouvre des parenthèses et je dis quelle événement je veux écouter**, **dans le cas c'est les modifications, qui sera la variable setText**.
> 
> Et dans mon composant il faut que je lui déclare la variable **setText**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-25%20à%2013.39.08.png)

- Pour créer une **référence on utilise le **# hashtag** et on donne le nom que l'on veut à notre référence.

- Donc **moi je vais l'appeler input référence**, grâce à cette syntaxe je vais créer la variable**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-25%20à%2013.58.37.png)
  
---
``$event est une varibale qui est fournis par angular``

> Lien de la vidéo =>[https://www.youtube.com/watch?v=27ZaLRwBsOc]


