# Structural directive ngIf 

> Le ngIf nous permet d'écrire des conditions dans notre **Dome**, pour **supprimer ou ajouter des éléments dans notre dome**

- Je peux lui donner le retour de la méthode **inShown**
![img£.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-25%20à%2022.22.47.png)
  
> Le **ngIf** va me servir de **Garde**

- On peut faire le **cumule des conditions*
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-25%20à%2022.31.27.png)
  
- Rappelle une référence c'est une variable qui pointe vers un élément **Html**

- **Ng- template** est **un élément d'angular**, **si je ne le met pas il y aura des erreurs**
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-26%20à%2012.58.10.png)
  
----
>LIEN DE LA VIDÉO =>[https://www.youtube.com/watch?v=kuJLIVPito4]
