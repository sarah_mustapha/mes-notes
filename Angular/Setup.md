#Setup

> Dans **getting started** il y a un lien appelé **setup**.

- Voici les prérequis avant de développer une **application angular** :
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-08%20à%2017.04.19.png)
  
- On nous dit que les connaissances sur **Typescript** sont utile mais pas **obligatoire**.

- Sur notre ordinateur on doit avoir installé **nodejs** ainsi que notre **package manager**.

- On va installer le **package Angular CLI**de manière globale ce qui va nous **Donner accès à la commande ng**. .
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-08%20à%2017.08.58.png)
  
> Dans mon code je veux **créer une nouvelle application dans le dossier angular**.
-----
> Lien de la page pour créer un projet angular => [https://angular.io/guide/setup-local]

- J'ai toute mon application à l'intérieur du dossier **my-app**.

- Dans package.json je vais **retrouver toutes les dépendances qu'a besoin Angular pour fonctionner correctement**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-08%20à%2017.46.47.png)
  
- Le dossier **nodemodules** comprend toutes les dépendances, il va avoir  tout le code **dont ont on dépend**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-10%20à%2015.20.31.png)
  
- C'est dans le **dossier source qu'il va y avoir notre application**
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-10%20à%2015.22.22.png)
  
        - Donc tout les **Assets**
        - Fichies d'environnements
        - Fichier index de base.

> Une single page appplication c'est un peux de **html**, une **coquille vide** et **beaucoup de javascript**.

- Dans le fichier **my-app** j'ai pleins de fichier qui vont configurer , exemple **typescript** j'ai un fichier de configuration.

- J'ai un **sous fichier de configuration** exemple les tests.

- J'ai un **tsllint** pour **modifier la syntaxe**.

> Dans notre cas on va lancer le terminal, en utilisant la commande **ng serve**. Il faut que je rentre dans mon dossier pour cela j'utilise la commande **cd my-app**, c'est dans le dossier **my-app que j'utilise la commande ng serve**.

> Donc maintenant mon serveur est lancé en **local sur mon ordinateur, sur le port 4200**, donc je peux cliquer et on voit que j'ai ma première application **angular**.
---
> LIEN DE LA VIDÉO => [https://www.youtube.com/watch?v=Hhg5iKRv9pw]