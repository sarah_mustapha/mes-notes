# Presentation de la structure du projet 

> Lien du CLI => [https://angular.io/cli]
---
- Voici les **alias / les raccourcis** 
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-10%20à%2015.44.53.png)
  
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-10%20à%2015.45.10.png)

----
- Dans notre projet on n'a un dossier **e2e** qui veut dire **ntwon** c'est un dossier qui va contenir **des tests ntwon**

- Les **ntwend** ce sont des tests qui **vont être écrit coter **navigateur**.

> Par exemple **un test coté navigateur** c'est avoir récupéré le titre de mon élément **app-rout** qui se trouve dans **l'élément qui à la classe content dans la spam**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-10%20à%2015.52.44.png)

----
- On va créer une nouvelle **AppPage** et récupérer son **getTitle**.

> Donc le test sera : ***j'espère que le getTitle de ma page est égale à my- app is running**, donc là cela va être des tests **n2n**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-10%20à%2016.50.13.png)

- Ici on va pouvoir écrire des tests qui vont directment pouvoir **tester le html**.
----
- On n'a un dossier **node-modules** qui va contenir **toutes les dépendances de notre projet**.
----
- On n'a notre dossier **source** ou **il ya tout notre projet**.
---
> À LA RACINE de notre fichier **angular on n'a deux fichiers intéréssant**:
> 1. Le fichier **angular.json**  
> 2. Le fichier **package.json**

> Le **Fichier angular.json** c'est un fichier qui contient des **informations sur notre projet**.

- Le **Fichier angular.json** il va déjà contenir **les projets qui sont présents dans notre dossier**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-10%20à%2016.59.40.png)
  
> Dans un même projet d'application **on pourrait très bien avoir plusieurs projets**.

- On n'a un seul projet qui s'appelle **my-app** et dons on va avoir des informations **sur son architecture, comment le constuire , comment compiler notre projet, ou est-ce qu'on peut compiler notre projet (dans le dossier **dist/my-app**), quelle est le le **fichier index** etc..**

> Donc là on n'a des informations qui sont relative au **projet my-app**.

> Donc c'est un **Fichier très intéressent à modifier pour atteindre certains objectifs**.
----
> Le fichier **package.json**, car on va pouvoir y voir **qu'elle son nôs dépendance**, ou on n'a **Des scripts qui peuvent être appelé**.

- J'ouvre mon **terminal**, je vais **arrêter le processus pour cela c'est controle c**, ou sinon on **peut laisser continuer et ouvrir une autre instance de terminal**.

- Dans mon cas ce que je veux faire c'est **D'éxecuter la commande start (ng serve)**.

- Je peux faire un **ng build** qui va **build mon application**.
---
> Dans le **dossier src** on n'a :
> 1. un dossier **styles.scss** qui contient **tous les styles de notre applications**.
-----
- On n'a un dossier **environnement** ou va pouvoir enregistrer nos **variables d'environnement dans un objet environment**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-10%20à%2017.43.40.png)
  
-> Es-ce que c'est une application pour mobile etc.. **On met toutes les informations d'environnement que l'on souhaite**.
> **Les informations d'environnement sont censée changer en fonction de l'environnement**, lorsque l'on **développe on insère nos environnement dans le fichier environnement**.

- Si l'application doit **être exécutés pour la production les variables peuvent très bien contenir des clés google, des clés d'api**.
------
- On n'a le dossier **Assets** ou pn va pouvoir mettre les **images**, les **icones** etc..

- Le plus important c'est le dossier **app**, c'est dans ce dossier que nous allons retrouver **notre application**, c'est ici que l'on va **modifier le plus souvent notre application**.

- Donc on voit que l'on n'a **six fichiers**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-10%20à%2017.53.56.png)
  
- Les **quatre premier fichier correspond à un composant**, car dans le nom il y a **.component**
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-10%20à%2017.56.04.png)
  
- On n'a les tests qui finissent tous par **.spec**, ce sont les testes du composant **app**. On n'attend que le titre de mon **app** soit égal a **my-app**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-10%20à%2017.58.56.png)
  
- On n'a notre **fichier composant**, on n'a le **titre qui est bien égale à my-app**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-10%20à%2018.02.14.png)
  
- Ensuite On n'a notre **module et notre module de routine**
-----
> Lien de la vidéo =>[https://www.youtube.com/watch?v=S6IHK2iFZcs]
