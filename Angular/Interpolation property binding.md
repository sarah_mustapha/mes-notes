#Interpolation

> L'interpolation c'est le fait d'afficher le contenu d'une variable dans le **html**.

- Dans notre composant **app.component.ts** on va déclarer une **variable du titre title**, dans le cas il y a **une chaîne de caractère par défaut**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-20%20à%2015.58.22.png)
  
> **Je veux l'afficher dans mon composant (composent.html)**.

- Donc je me rends dans le fichier **app composent.html**.

- Pour cela j'utilise l'interpellation qui est **2 accolades {{ }}**, et à l'intérieur des accolades **je vais indiquer quel élément je veux afficher**. **ex=>{{title}}**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-20%20à%2016.12.33.png)
  
> Donc là **on n'a réalisé une interpolation**, on n'affiche le contenu 

> On peut afficher **le contenu d'une variable ou d'une méthode / fonction**.

- Exemple je vais créer une fonction qui s'appelle **getTitle** qui va me retourner **mon titre**.

> Rappel **pour accéder à une variable j'utilise le mot clé this** et je vais **la retourner dans ma fonction**.

- Sur mon **html** dans mon **h2 je vais vouloir interpeller donc afficher le contenu qui est rendu par la fonction getTitle**, donc là **je n'oublie pas de l'invoquer avec les parenthèses et de passer en paramètre si besoin**.., dans le cas il ne prend rien en paramètre et tout ce qu'il fait **c'est de retourner une chaîne de caractère**.

- **Si on prend l'habitude de typer** on peut dire qu'il prend une chaîne de caractère que le **title c'est une chaîne de caractère**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-20%20à%2016.39.26.png)
  
< **Rappel !** si la variable **title change angular va directement changer**
----------
## **Une propriété Binding**

> Une **property Binding** c'est le fait de lier **un attribut à une variable**

- Donc là je vais créer un autre **h2** et je ne vais pas afficher dans le contenu la variable mais je vais lier **la propriété innerText**, cette propriété je vais la lier avec la variable déclaré dans mon composant **app.component.ts (title)**. Pour cela **j'indique l'attribue dans mon composant.html**, je louvre d'un crochet [et je le ferme d'un crochet].
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-22%20à%2015.47.06.png)
  
> **Mes bracket => crochet**, doivent **être lié avec une variable**, pour cela j'ajoute un égale et des guillemets **=""** et **à l'intérieur des guillemets je lui donne l'attribut que je vais lui donner** (title), donc ce que je dis est dans le texte de l'élément **h2 je veux mettre mon titre**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-22%20à%2016.08.21.png)
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-24%20à%2018.53.29.png)
> Cela peux être intéressant **d'utiliser le innerText** dans le cas ou dans ma variable je vais avoir des éléments html, il faut que je le lie **avec la propriété innerHTMl**
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-24%20à%2018.59.58.png)

- On peut **lier la couleur ou le style d'un élément html**, dans notre **composant html**

- Je veux lier le**Background color** et je vais indiquer avec quoi je veux attribuer cette **attribut AVEC UNE VARIABLE QUE JE VEUX APPELER **bgColor**
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-24%20à%2019.20.58.png)
----
## Pour afficher le contenu d'un objet

- Je vais déclarer une variable que je vais appeler **User** qui va être un objet donc je vais **Typer** avec la **propriété name qui est une chaîne de caractère**, je dois lui insérer un objet **name avec une string**.
  ![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-24%20à%2019.30.34.png)

- Je vais devoir ajouter dans ma **div rouge ** quelle est le nom de l'utilisateur**, pour cela j'ouvre deux accolades et au milieu je vais afficher **user**.

- Il ya un problème je ne peux pas lui donner un **objet à interpréter** il faut que je lui donne **une propriété soit un boolean, soit une chaîne de caractère, soit une variable**.Dans notre cas je vais afficher le **nom**.

> Un **nom c'est comme en javascript normal, c'est avec le point**
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-24%20à%2019.38.01.png)
-----
> Lien de la video =>[https://www.youtube.com/watch?v=WvqbK-_L160]

  




