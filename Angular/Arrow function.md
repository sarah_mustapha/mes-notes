# Arrow Function

> Des **Arrow function** c'est **un moyen décrire des fonctions**.

- Donc on n'a vu comment déclarer une fonction avec le mot clé **function en lui apportant un nom**.

- On voit que dans le nommage on commence par une lettre minuscule, ensuite **la première de chaque nouveau mot est en majuscule** (On n'appelle la notation en **CamelCase**).

> C'est ce que l'on fait pour déclarer une fonction donc on utilise le **mot clé function**.
```javascript
function maFonction() {  //CamelCase
    
}
```
-----
> Les fonctions dans un objet sont appelés **des méthodes**.

- **Une méthode dans un objet c'est juste une fonction (C'est la même chose)**, Cela revient au même mais on préfère **différencier cela pour montrer que c'est dans un objet**.

- Pour déclarer un objet je vais appeler **video** donc c'est **un nouvel objet** qui va avoir **la propriété title qui sera un type titre**, je lui rajoute **une méthode qui va être play** et cette méthode va afficher le mot clé **This**. Car les **Arrow function** on un lien fort avec le **This**.
````javascript
const video = {
    title: 'titre',
    play() {
        console.log(this);
    }
}
````
- Je vais appeler la méthode **play** dans **l'objet vidéo**.
````javascript
const video = {                      // OBJET
    title: 'titre',
    play() {                        // OBJET
        console.log(this);
    }                               // OBJET
};
video.play();  //j'appelle la méthode play dans l'objet vidéo
````
> On voit que **this** renvoie mon objet dans la console.
> 
> LE **THIS** VA FAIRE RÉFÉRENCE À L'OBJET DANS LEQUEL EST APPELÉ MA MÉTHODE, ICI **THIS SERA ÉGAL À MON OBJET**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-02%20à%2023.32.06.png)

----

- Si je déclare une fonction et dans cette fonction je **consolelog le this**, j'appelle ma fonction. On voit que dans le deuxième consolelog **this fait référence à la fenêtre**, cela fait référence à l'objet global car on exécute la fonction **dans l'objet windows(la fenêtre).
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-02%20à%2023.39.01.png)
````javascript
const video = {                     
    title: 'titre',
    play() {                       
        console.log(this);
    }                              
};
video.play();

function maFonction() {
    console.log(this);
}
maFonction();
````
-----
- Je vais déclarer une fonction **constructor**.

- Cette fonction **constructor sera vidéo**.

> Pour déclarer une **fonction constructor** on utilise la notation **PascalCase** (**La première lettre doit être en majuscule et la première lettre des mots suivants doit également être en majuscule**).

- C'est ce qu'on appelle une **constructor function**.

- Je vais **récupérer le titre** et l'enregistrer dans l'objet **This**. Donc j'enregistre la **propriété title dans l'objet this**.
````angularjs
const video = {
    title: 'titre',
    play() {
        console.log(this);
    }
};
video.play();

function Video(title) {     // Constructor function
    this.title = title;    //j'enregistre le titre dans l'objet this
}
````
- Ensuite **je crée une nouvelle vidéo** avec le mot clé **new Video et je lui donne un titre**, je vais également afficher **le this dans ma vidéo pour qu'on puisse voir à quoi fait référence this**.
`````angularjs
const video = {
    title: 'titre',
    play() {
        console.log(this);
    }
};
video.play();

function Video(title) {     // Constructor function
    this.title = title;    //j'enregistre le titre dans l'objet this
    console.log(this);
}

const video = new Video(title:'a');  
`````
- On voit que dans le deuxième console log **this fait référence à un objet créé à partir de vidéo**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-02%20à%2023.55.15.png)
  
> Ce qu'on appelle un **constructor function c'est une fonction qui va créer un objet en définissant des propriétés**.

- Exemple : je peux avoir **play = function**. Donc là je vais créer un objet grâce à l'opérateur **new**, donc l'opérateur **new ce qu'il fait c'est de créer un objet vide** et le **mot clé this va faire référence à cette objet là**.
```angularjs
const video = {
    title: 'titre',
    play() {
        console.log(this);
    }
};
video.play();

// Constructor function 
function Video(title) {     
    this.title = title;   
    this.play = function() {
        
    };
    console.log(this);
}

const video = new Video(title:'a');  // {}
```
-----
- J'utilise une **Factory function**, c'est la même chose l'idée est de **Créer un objet**.

- Exemple: on va créer => createVideo et on va **Récupérer un titre**.

- Cette **Factory** va **envoyer un objet vide** dans lequel on va lui donner **la propriété title**.

> Ce qu'on appelle une **Factory c'est une usine qui nous permet de créer des objets qui envoie les mêmes propriétés**.

- Cette fonction je vais l'appeler **createVideo**, dans la fonction je vais afficher le **this** qui est mon **troisième this**.

> Rappel: on utilise **alt commande l** pour organiser notre code.
```angularjs
const video = {
    title: 'titre',
    play() {
        console.log(this);
    }
};
video.play();

// Constructor function 
function Video(title) {     
    this.title = title;   
    this.play = function() {
        
    };
    console.log(this);
}

const video = new Video(title:'a');  // {}

// factory function
function createVideo(title) {
    console.log('3', this);
    return {
        title: title,
    };
}
const video2 = createVideo();
```
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-03%20à%2021.54.37.png)
- On voit que dans mon **troisième consolelog** le **this** fait référence à la fenêtre et non pas à l'objet, alors que j'ai bien créé un objet **l'objet vidéo**. Par contre le mot clé **This** ne fait pas référence à cet objet là **il fait référence à l'objet global dans lequel on n'a exécuté la fonction createVideo**.

> On n'a un **mot clé qui ne fait pas référence au mêmes éléments**.
````angularjs
const video = {
    title: 'titre',
    play() {
        console.log(this);
    }
};
video.play();

// Constructor function 
function Video(title) {     
    this.title = title;   
    this.play = function() {
        
    };
    console.log('2', this);
}

const video = new Video(title:'a');  // {} // l'opérateur new va créer un objet vide et il va lier le 2 iem this à cette objet'

// factory function
function createVideo(title) {
    console.log('3', this);
    return {
        title: title,
    };
}
const video2 = createVideo();
console.log('video 2:', video2);
````
> C'est important de comprendre la différence entre **, le constructor function et le factory function**. l'opérateur **new va créer un objet vide** et il va **lier le 2 iem this** à cette objet là. Le **new fait la même chose avec les fonctions**.
> 
>  On n'a un **constructor fonction et une factory fonction c'est le fait de créer des objets**. Si on n'utilise pas **new on va avoir une erreur** et on sait que c'est un constructor function **Car j'ai rajouté la première lettre en majuscule**.

- Si j'utilise **le mot clé new** dans ma **factory function**, l'intérpréteur m'indique que la notation n'est pas bonne car **je ne dois pas utiliser un LowerCase mais un CamelCase**. **Malgré tout cela fonctionne lorsque je recharge ma console grâce au mot clé new j'ai un mot clé vide**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-04%20à%2012.28.17.png)
  
```angularjs
````angularjs
const video = {
    title: 'titre',
    play() {
        console.log(this);
    }
};
video.play();

// Constructor function 
function Video(title) {     
    this.title = title;   
    this.play = function() {
        
    };
    console.log('2', this);
}

const video = new Video(title:'a');  // {} // l'opérateur new va créer un objet vide et il va lier le 2 iem this à cette objet'

// factory function
function createVideo(title) {
    console.log('3', this);
    return {
        title: title,
    };
}
const video2 = new createVideo(title:'3');
console.log('video 2:', video2);
```
- Dans ma **factory function je ne Sauvegarde aucune propriété** contrairement **au constructor ou je sauvegarde le titre**, je n'ai pas fait cela **je retourne juste un objet**.

> En fonction d'ou on appelle notre fonction et si on utilise l'opérateur **new** qui va pointer le **this vers notre nouvelle objet**, on va voir qu'on n'aura des résultats différents.
----
- Dans l'objet vidéo je rajoute une propriété **Tags** qui aura **tag1, tag2, tag3**.

- Je vais renommée la méthode **play** en **showTags** et ensuite l'appeler, et je vais afficher dans la console le **This**.
``````angularjs
const video = {
    title: 'titre',
    tags: ['tag1', 'tag2', 'tag3'],
    showTags() {
        console.log(this);
    }
};
Video.showTags();
``````
> Le **this fait toujours référence à l'objet dans laquelle la méthode est appelé**.

- J'aimerais afficher **Tous les tags dans la console**, pour cela j'utilise la **méthode forEach** et je vais passer en **Call back une fonction**.

- **Dans cette fonction je vais pouvoir récupérer en premier paramètre mon tag**, puis je l'affiche dans la console.
``````angularjs
const video = {
    title: 'titre',
    tags: ['tag1', 'tag2', 'tag3'],
    showTags() {
        console.log(this);
        this.tags.forEach(function (tag: T) { 
            console.log(tag);
        });
    }
};
Video.showTags();
``````
> On voit que j'affiche **bien mes 3 éléments**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-04%20à%2013.02.28.png)

- J'aimerais afficher le titre à coter de mon élément il faudrait que j'écris **this.title**, car **le mot clé this fait référence à l'objet**. Cela ne fonctionne pas car **le this ne pointent pas vers la même chose**, car **la fonction callback n'est pas une méthode de mon objet c'est une fonction classique** qui va être appelé dans le contexte de la fenêtre, le mot clé this fait référence à la fenêtre(à l'objet global).

- En deuxième paramètre il récupère **this arg**On peut lui **donner un contexte et le mot clé this pointera vers ce contexte si on lui donne**.

- Cela sera **Author nommé Jean**, si j'affiche le this dans ma console on vera que le **This fait bien référence à l'objet nommé en paramètre**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-04%20à%2013.15.59.png)

``````angularjs
const video = {
    title: 'titre',
    tags: ['tag1', 'tag2', 'tag3'],
    showTags() {
        console.log(this);
        this.tags.forEach(function (tag: T) { 
            console.log(this, tag);
        }, {author: 'jean'});   //nouvelle objet
    }
};
Video.showTags();
`````` 
- A La place de lui donner un nouvelle objet je pourrais lui donner **this**, Car le mot clé **This est au niveau de la méthode qui est bien notre objet**, Donc je peux lui donner le mot clé this et dans **la fonction notre mot clé this fera bien référence à l'objet donné ici**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-04%20à%2013.21.21.png)
  
- Si je recharge le mot clé **this est bien notre objet**.
-----
- Dans ma fonction **Callback** j'ai lié l'objet qui fait bien référence à l'objet**.

> Attention toutes les fonctions non pas cet argument **this** qui leurs permet de passer le contexte comme ***map qui n'a pas cette argument** par exemple.
> 
> Le moyen le plus sur **d'utiliser le contexte** c'est d'utiliser la notation **Arrow function**.
-----
- La notation **arrow function**, on **utilise pas le mot clé function**, on va **ouvrir des parenthèses**, on fait **=> et on ouvre le block d'instruction**.
````angularjs
// function arrow
let arrow = () => {  // notaion arrow function
    
};
````
> Le **=> V'a partager le contexte**
 
> On va passer dans la fonction un **this**.

> **() Entre parenthèse on n'avait le mot clé function**, maintenant **C'est seulement les parenthèse (), pour pouvoir indiquer quel sont les arguments, les paramètres**.

> Ensuite on n'a notre flèche => et dans le block d'instruction on va pouvoir afficher le **nom et l'age**.
```angularjs
// function arrow
let arrow = (name, age) => {            // notaion arrow function
    console.log(name, age);
};
```
- Maintenant je peux **appeler la fonction avec les paramètres**.
```angularjs
// function arrow
let arrow = (name, age) => {            // notaion arrow function
    console.log(name, age);
};
arrow(name: 'jean', age: 12);
```
- Cela m'affiche bien dans ma console le **nom et l'age**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-04%20à%2014.36.33.png)
  
- Je vais afficher le **this**, on voit que le **this reste la fenêtre**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-04%20à%2014.38.49.png)
  
-------

- On revient dans notre exemple, donc je vais utiliser la notation **Arrow function** et après la parenthèse je vais utiliser un **égal et un chevron fermé =>**.

- Donc là je ne lui **passe plus d'objet au mot clé this**, grâce à **Arrow function** le mot clé **this v'a pointé vers l'objet**, lorsque je recharge on voit que cela fonctionne toujours car on peut voir avec la flèche c'est comme un dessin (c'est lier le contexte parent à la fonction).

- Donc **This** c'est notre objet, et **la flèche pointe vers notre this** on va lui donner **le this**, maintenant dans ma fonction malgré quelle ne soit pas apeller

``````angularjs
const video = {
    title: 'titre',
    tags: ['tag1', 'tag2', 'tag3'],
    showTags() {
        console.log(this);    // le this pointe vers notre objet et en va lui envoyer le this
        this.tags.forEach((tag: T) => { 
            console.log(this.title, tag);
        });
    }
};
Video.showTags();
`````` 
- Ma **Fonction** malgré qu'elles ne soient pas appeler dans ma méthode **showTags**, mon mot clé **this pointera vers le même objet**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-04%20à%2015.52.21.png)
  
> Cela est un moyen de lier le mot clé **this avec la fonction parent**.
-----
- Je vais déclarer une fonction qui sera **consoleTag**, je vais lui donner une fonction **Anonyme** qui va prendre en paramètre **tag** et qui va afficher le **this et le tag**.

- Donc je peux **donner référence à une fonction qui sera exécuté / invoqué**, invoqué c'est **ouvrir les parenthèses et donner les arguments**.

> Si on donne justes les références à une fonction, la **fonction callback va être exécuté dans le forEach**.
``````angularjs

let consoleTag = function(tag) {
    console.log(this, tag);
};

const video = {
    title: 'titre',
    tags: ['tag1', 'tag2', 'tag3'],
    showTags() {
        console.log(this);    // le this pointe vers notre objet et en va lui envoyer le this
        this.tags.forEach(consoleTag);
};
Video.showTags();
`````` 
- On affiche le **this** et on voit que le **This** pointe **vers la fenêtre**.

- Donc c'est problématique car **je veux afficher le titre** et ensuite le **Tag**.

- Donc pour cela on peut utiliser le **Arrow fonction**, donn on dit que l'on veut **lier le contexte parent à la fonction**.

> Cela ne fonctionne pas car **le contexte parent c'est la fenêtre**
``````angularjs

let consoleTag = (tag) => {
    console.log(this, title, tag, this);
};

const video = {
    title: 'titre',
    tags: ['tag1', 'tag2', 'tag3'],
    showTags() {
        console.log(this);    // le this pointe vers notre objet et en va lui envoyer le this
        this.tags.forEach(consoleTag);
};
Video.showTags();
`````` 
- Ce qu'on peut faire c'est d'utiliser la méthode **bind**(qui veut dire lier) et **lier le mot clé this** à notre fonction.

> Je change la notation à la déclaration pour qu'il ne lie pas l'objet **Window**.

- Je recharge on voit que cela fonctionne 
``````angularjs

let consoleTag = function (tag) {
    console.log(this, title, tag, this);
};

const video = {
    title: 'titre',
    tags: ['tag1', 'tag2', 'tag3'],
    showTags() {
        console.log(this);    // le this pointe vers notre objet et en va lui envoyer le this
        this.tags.forEach(consoleTag.bind(this));
};
Video.showTags();
`````` 
- Si je donne en argument un **objet** on voit que cela fonctionne.

- Donc là on voit comment régler le proclème du this qui ne fait pas référence à l'objet. Donc cela est une notation si on n'a une **fonction qui est contenu ailleur, on peut l'utilisant en liant grâce à la méthode bind le contexte**.

- Le **This** c'est ce que j'ai passé en **argument (title:'o')**.
``````angularjs

let consoleTag = function (tag) {
    console.log(this, title, tag, this); //le this c'est ce que j'ai passé en argument objet
};

const video = {
    title: 'titre',
    tags: ['tag1', 'tag2', 'tag3'],
    showTags() {
        console.log(this);    // le this pointe vers notre objet et en va lui envoyer le this
        this.tags.forEach(consoleTag.bind({title: 'o'}));
};
Video.showTags();
`````` 
> Souvent on va utiliser les fonctions **Callback** et on veut lier le contexte, pour cela on utilise la méthode **bind**.
------
- Je vais écrire la notation **arrow function** donc je prend en premier paramètre le **Tag**, j'utilise une flèche, **j'ouvre mon block d'instruction**. Donc je lie le **this parent au this de la fonction**.

- Maintenant je peux accéder au **propriétés de mon objet**.
``````angularjs

let consoleTag = function (tag) {
    console.log(this, title, tag, this); //le this c'est ce que j'ai passé en argument objet
};

const video = {
    title: 'titre',
    tags: ['tag1', 'tag2', 'tag3'],
    showTags() {
        console.log(this);    // le this pointe vers notre objet et en va lui envoyer le this
        this.tags.forEach((tag: T) => {
        console.log(this.title, tag);
        })
    };
};
Video.showTags();
`````` 
> Lorsque l'on travaille avec des **freamwork** celà peut poser problème de ne pas utiliser cette notation.

- Je peux utiliser autre chose ce qu'on appelle **Call** pour **appeler une fonction**. Donc en argument il va prendre

> LIEN DE LA VIDÉO => [https://www.youtube.com/watch?v=6N4HZ9JPcEw&feature=youtu.be]