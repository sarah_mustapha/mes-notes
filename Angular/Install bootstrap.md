# Install bootstrap

> Pour installer **Bootstrap** => aller sur le site de bootsrap sur => [https://getbootstrap.com]

> Ensuite aller dans la documentation de bootstrap => [https://getbootstrap.com/docs/5.0/getting-started/introduction/]

> Choisir la version que l'on souhaite, Dans le lien **dowload** on peut installer **bootstrap via le packet**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-11%20à%2012.36.21.png)

- **Je copie le lien pour pouvoir l'installer dans les dépendances de mon projet**, donc **je colle la commande sur mon terminal** c'est **npm install bootstrap**.

- Je ferme le terminal et on voit que **bootstrap est bien dans mes dépendances**.

- Je peux également le trouver dans le dossier **node-modules** si je tape bootstrap.

> Maintenant **bootstrap est bien importé dans notre projet**, On peut utiliser tout les éléments de bootstrap.
----
> Lien de la vidéo => [https://www.youtube.com/watch?v=g-z_LSE7EBM]

