# Mise en Pratique

- Ici lorsque je vais entrer les **contenue d'une tache**, **je veux qu'il se rajoute dans mon tableau**
````html
<div class="container">
    <h1 class="my-5">{{ title }}</h1>
    
    <div class="form-group">
        <input type="text" id="input" placeholder="Le nom de la tâche">
        <button type="button" id="btn" class="btn btn-primary"></button>
    </div>
    <p>La prochaine tâche : </p>
    
    <div id="task">
        <div class="task">Faire la cuisine</div>
        <div class="task">Nettoyer</div>
        <div class="task">Ranger</div>
        </div>
    <button type="button" id="btn-save" class="btn btn-primary"></button>
</div>
````
- je vai lier la valeur [(ngModel)], il faut que je l'importe sur mon **ts** dans **imports**
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-04-12%20à%2011.38.01.png)
  
- Maintenant que j'ai importé le fichier **formModules j'ai accès au mor clé ngModel**.
````html
<div class="container">
    <h1 class="my-5">{{ title }}</h1>

    <div class="form-group">
        <input type="text" placeholder="Le nom de la tâche"[(ngModel)]="text">
        <button type="button" id="btn" class="btn btn-primary"></button>
    </div>
    <p>La prochaine tâche : </p>

    <div id="task">
        <div class="task">Faire la cuisine</div>
        <div class="task">Nettoyer</div>
        <div class="task">Ranger</div>
    </div>
    <button type="button" id="btn-save" class="btn btn-primary"></button>
</div>

````
- si je vais dans **app.component.ts** text est une chaine de caractère vide.
````typescript
import {component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title: string = 'Mettre en pratique les bases';
    
    tasks: string = [
        'Sortir le chien ',
        'Nettoyer le salon',
        'Jetter les poubelles'
        ];
    text: string = '';
}
````
- Je vais dire **Affiche le paragraphe que si texte est différent d'une string vide**.
````html
<div class="container">
    <h1 class="my-5">{{ title }}</h1>

    <div class="form-group">
        <input type="text" placeholder="Le nom de la tâche"[(ngModel)]="text">
        <button type="button" id="btn" class="btn btn-primary"></button>
    </div>
    <p *ngIf="text !=== '' && text">La prochaine tâche : {{ text }} </p>

    <div id="task">
        <div class="task">Faire la cuisine</div>
        <div class="task">Nettoyer</div>
        <div class="task">Ranger</div>
    </div>
    <button type="button" id="btn-save" class="btn btn-primary"></button>
</div>

````
- Lors du **click je veux ajouter une tâche** pour cela je déclare une méthode addTask qui envoie rien et qui va afficher dans la console **this.text** car on sait que **this.text c'est le contenu d'input**.
````html
<div class="container">
    <h1 class="my-5">{{ title }}</h1>

    <div class="form-group">
        <input type="text" id="input" placeholder="Le nom de la tâche"[(ngModel)]="text">
        <button type="button" id="btn" class="btn btn-primary" (click)="addTask"></button>
    </div>
    <p *ngIf="text !=== '' && text">La prochaine tâche : {{ text }} </p>

    <div id="task">
        <div class="task">Faire la cuisine</div>
        <div class="task">Nettoyer</div>
        <div class="task">Ranger</div>
    </div>
    <button type="button" id="btn-save" class="btn btn-primary"></button>
</div>

````
````typescript
import {component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title: string = 'Mettre en pratique les bases';
    
    tasks: string = [  // j'ai un tableau dans mon composant
        'Sortir le chien ',
        'Nettoyer le salon',
        'Jetter les poubelles'
        ];
    text: string = '';
    
    addTask(): void {
        console.log(this.text);
    }
}
````
- Maintenant que j'ai accès à la valeur de **l'input je vais pouvoir l'ajouter à mon tableau**.

- **Ce que moi j'ai dans mon composant c'est un tableau**, donc je vais utiliser **la directive ngFor**, donc je vais créer une variable task à partir du tableau tasks**
````html
````html
<div class="container">
    <h1 class="my-5">{{ title }}</h1>

    <div class="form-group">
        <input type="text" id="input" placeholder="Le nom de la tâche"[(ngModel)]="text">
        <button type="button" id="btn" class="btn btn-primary" (click)="addTask"></button>
    </div>
    <p *ngIf="text !=== '' && text">La prochaine tâche : {{ text }} </p>

    <div id="task">
        <div class="task" *ngFor="let task of tasks">{{ task }}</div>
        <div class="task">Nettoyer</div>
        <div class="task">Ranger</div>
    </div>
    <button type="button" id="btn-save" class="btn btn-primary"></button>
</div>
````
````typescript
import {component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title: string = 'Mettre en pratique les bases';
    
    tasks: string = [  // j'ai un tableau dans mon composant
        'Sortir le chien ',
        'Nettoyer le salon',
        'Jetter les poubelles'
        ];
    text: string = '';
    
    addTask(): void {
        console.log(this.text);
        this.tasks.push(this.text);
    }
}
````
````html
````html
<div class="container">
    <h1 class="my-5">{{ title }}</h1>

    <div class="form-group">
        <input type="text" id="input" placeholder="Le nom de la tâche"[(ngModel)]="text">
        <button type="button" id="btn" class="btn btn-primary" (click)="addTask"></button>
    </div>
    <p *ngIf="text !=== '' && text">La prochaine tâche : {{ text }} </p>

    <div id="task">
        <div class="task" *ngFor="let task of tasks; let id = index" 
             (click)="deleteTask(id)"> 
            {{ id }}{{ task }} // j'affiche la position
        </div>
        <div class="task">Nettoyer</div>
        <div class="task">Ranger</div>
    </div>
    <button type="button" id="btn-save" class="btn btn-primary"></button>
</div>
````
- Je déclare une méthode
````typescript
import {component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title: string = 'Mettre en pratique les bases';
    
    tasks: string = [  // j'ai un tableau dans mon composant
        'Sortir le chien ',
        'Nettoyer le salon',
        'Jetter les poubelles'
        ];
    text: string = '';
    
    addTask(): void {
        console.log(this.text);
        this.tasks.push(this.text);
    }
    
    deletaTask(id: number): void {
        //let arr = [...this.tasks];
       // arr.splice(id, 1);
        //this.tasks = [...arr];
        
        this.tasks.splice(id, 1);
       console.log(id);
    }
}
````
- Je veux écouter le **click** et à chaque click **je veux lancer la fonction save**, donc je vais la déclarer **une fonction qui envoie rien, qui va afficher tous les éléments qu'on aura passé dans notre tableau pour chaques éléments, on va l'afficher dans la console, si on donne une fonction callback pour chaque éléments**.

````typescript
import {component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title: string = 'Mettre en pratique les bases';
    
    tasks: string = [  // j'ai un tableau dans mon composant
        'Sortir le chien ',
        'Nettoyer le salon',
        'Jetter les poubelles'
        ];
    text: string = '';
    
    addTask(): void {
        console.log(this.text);
        this.tasks.push(this.text);
    }
    
    deletaTask(id: number): void {
        //let arr = [...this.tasks];
       // arr.splice(id, 1);
        //this.tasks = [...arr];
        
        this.tasks.splice(id, 1);
       console.log(id);
    }
    
    save(): void {
        this.tasks.forEach(function (task: string) {
            console.log(task);
        });
    }
}
````
````html
````html
<div class="container">
    <h1 class="my-5">{{ title }}</h1>

    <div class="form-group">
        <input type="text" id="input" placeholder="Le nom de la tâche"[(ngModel)]="text">
        <button type="button" id="btn" class="btn btn-primary" (click)="addTask"></button>
    </div>
    <p *ngIf="text !=== '' && text">La prochaine tâche : {{ text }} </p>

    <div id="task">
        <div class="task" *ngFor="let task of tasks; let id = index" 
             (click)="deleteTask(id)"> 
            {{ id }}{{ task }} // j'affiche la position
        </div>
        <div class="task">Nettoyer</div>
        <div class="task">Ranger</div>
    </div>
    <button type="button" id="btn-save" class="btn btn-primary"
    (click)="save()">Enregistrer</button>
</div>
````
- Je vais ajouter une référence que je vais donner à ma méthode **addTask**, c'est mieux **de typer car on va pouvoir développer plus rapidement**
```typescript
    addTask(): void {
        this.tasks.push(this.text);
        this.text = '';
    }
    
    deletaTask(id: number): void {
        //let arr = [...this.tasks];
       // arr.splice(id, 1);
        //this.tasks = [...arr];
        
        this.tasks.splice(id, 1);
       console.log(id);
    }
    
    save(): void {
        this.tasks.forEach(function (task: string) {
            console.log(task);
        });
    }
}
```
-----
> Lien de la vidéo =>[https://www.youtube.com/watch?v=26sDYakDeg0]
