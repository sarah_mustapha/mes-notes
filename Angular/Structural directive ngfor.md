# Structural directive ngfor

--------

> **Le ng for nous permet d'effectuer des 'itérations dans le dome**

- **Dans ma variable user j'ai un tableau vide et moi je vais ajouter deux utilisateurs**

- **Donc là j'ai un tableau d'objet et 3 utilisateurs**
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-26%20à%2013.50.03.png)
  
- **Je veux afficher tous mes utilisateurs dans un paragraphe** et je vais utiliser mon **ngFor**.
`````html
<p *ngFor="let user of users">
    </p>
<p *ngFor="let user of users">
    {{user.name}}   // à l'intérieur de l'élément
</p>
`````
- Je peux afficher à l'intérieur de mon élément p **le nom de l'utilisateur**.

- **Pour itérer au travers d'un tableau je dois déclarer la variable pour cela j'utilise user**, cette **variable contiendra la valeur du tableau user**
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-26%20à%2013.57.36.png)

> Le fait de rajouter la directive **ngFor**, va dupliquer autant de fois l'élément sur lequel il ya **d'élément sut mon tableau**

> Ici mon tableau s'appelle **users**, **j'ai bien 3 éléments**.

-----

- **J'ajoute un point virgule**après la première partie pour avoir **accès à l'index**, je peux **créer une nouvelle variable qui va prendre la valeur de l'index**

- Lorsque j'utilise **ngFor j'ai accès à une variable qui s'appelle l'index (let id = index)**.

- **L'index je vais l'assigner à une nouvelle variable que je crée qui s'appelle id**.

- j'affiche mon **id je fais une interpolation de la variable id**.
````html
{{id}}
````
- J'ajoute un **bouton** **(Click)=''Afficher'''**

- je peux créer une nouvelle constante que je vais appeler **user2** qui sera un objet , qui va prendre toutes les valeurs de l'utilisateur passé en paramètre.**{...user};

````typescript
users: { name: string, color?: string}[] = [
    {name: 'jean', color: 'grey'},
    {name: 'Eva'},
    {name: 'Sam', color: 'red'}
    ];

showAlert(user, id): void {
    const user2 = {...user};
    user2.name = 'ok';
    console.log(user, user 2);
}
````
- **Je peux ajouter l'id** qui est la position de l'élément, grâce à sa je 
`````html
<h1 *ngIf="show">{{title}}</h1>

<div *ngFor="let user of users; let id= index">
<div>{{id}} - {{user.name}}</div>

<button (click)="showAlert(user, id">Afficher</button>
</div>

<button (click)="show = !show">Toggle title</button>
````` 
- Je ne peux pas mettre sur la meme balise **la directive ngFor et ngIF**.

- Je vais déplacer ma directive **ngFor dans mon ng-container**.
````html
<ng-container *ngFor="let user of users; let id= index">
    <div *ngIf="user.color">  
    <div>{{ id }} - {{ user.name }} | {{user.color}} </div>

        <button (click)="showAlert(user, id">Afficher</button>
    </div>

</ng-container>
````
--------
> Lien de la Vidéo => [https://www.youtube.com/watch?v=E2KwZMS6KyI]
