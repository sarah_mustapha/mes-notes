# Notre premier composant 

> Ce premier composant est nommé **app**, chaque fichier qui comporte ce composant là sont des fichiers qui **contienne .component**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-11%20à%2014.40.02.png)

1. htm du composant
2. css du composant
3. les tests du composant
4. le composant du composant

- On va **séparer tout les composant dans un dossier component.** Et on va séparer les **composants des fichiers modules**.

> **Je fais click droit sur app et je crée un nouveau dossier** que je vais appeler **components**, à l'intérieur je crée un **dossier que j'appelle app**.

> **Je sélectionne le premier fichier, j'appuie sur shift** et je déplace dans le dossier app.

> Je dois vérifier dans mon app.module.ts qu'il le récupère bien dans mon nouveau dossier.
-----
- Angular un composant c'est une **class qu'on exporte et qui est à noter d'une directive qui s'appelle Component**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-11%20à%2015.17.39.png)
  
- **Cette directive est importé du corp d'angular**, on n'importe le décorateur component à partir de ce fichier et on vient à noter notre classe avec ce décorateur
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-11%20à%2015.19.07.png)
  
- Donc ce décorateur prend **un paramètre qui est un objet et cet objet à trois propriétés de base, qui nous permettent de définir un conmposant, il sera entré dans un objet dans le décorateur COMPONENT**
        
        1.Selector: 'app-root' (Ce composant est sélectionnés via son selector)
       
        2.templateUrl: './app.component.html', (à la place ou ouvre ``` et on va pouvoir écrire directement du html``)/ 2.templateUrl:``
        
        3.styleUrls: ['./app.component.scss'], (Donc ici on n'a **un tableau de style**, dans notre cas on n'a un fichier css de base), pour avoir un style de manière globale il faut **que je l'insère dans style.scss**.
  
  > En cas de problème je relance le serveur en t'append la commande **ng serve**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-15%20à%2014.53.25.png)
  
> Le **APPComponent** on la déja vu c'est notre **composant de base**, c'est le **composant de notre application**.

>  le Selector: =>'app-root' (Ce composant est sélectionné via son selector) et on l'a déjà vu dans **index.html**.

- Angular réussi à déterminé que **App-root** est un composant alors il **va rentrer le template à l'intérieur du composant**, ensuite il va appliquer le **Style et la logique**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-15%20à%2014.59.54.png)

- Je vais **créer un nouveau composant**(un dossier home)
        
        1.créer un fichier **home.component.ts**).
        
        2.créer un fichier **home.component.css**).=> pour donné du style.
        
        3.créer un fichier **home.component.html**).

- Dans mon **home html** je vais lui donner un **h2** avec le texte **home**.

> Donc là on n'a vu comment **créer un composant via notre premier composant**
-----
- Il faut d'abord importé un component avec un **C** majuscule dans notre **home.component.ts** qui est dans le fichier **@angular/core;**
---
- Ensuite on va créer une class qu'on va appeler **homeComponent**, et **cette classe il faut lui dire que c'est un composant** pour cela il faut :
           
            => @Component 
  
- Et ensuite on invoque la fonction, **dans la fonction component on doit lui donner un argument qui est l'objet**.

- Ces arguments là doivent être **composé de selector** qui sera **app-home** pour pouvoir utiliser le composant **HomeComponent et l'utiliser**.
      
- Ensuite il doit y avoir **un templateUrl**, donc dans le dossier courant je veux accéder au dossier **home.component.html**.

- La dernière propriété c'est **styleUrls**, donc ici c'est un tableau, dans le fichier **css**je veux accéder au dossier **home.component.css**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-15%20à%2015.59.15.png)
> Et là j'ai créé mon premier composant.
----
- Mon application ne connait pas mon composant **home**, pour cela il faut que je le déclare pour cela il faut **aller sur le fichier app.module.ts**.

- On n'a une propriété **Declaration qui nous permet de déclarer tous les composants**, je lui dis que **app.module** tu dois connaitre le **HomeComponent**, pour cela il faut que je **l'importe**.

- Donc j'importe mon **HomeComponent** de mon fichier qui se trouve dans le dossier **component/ dans le dossier home/ dans le fichier home.ts**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-15%20à%2016.30.10.png)
  
> Il y a un problème **Car il n'y a aucun **HomeComponent qui a été exporté**, il faut que j'ajoute **Exporte** dans **mon fichier home.component.ts**
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-15%20à%2016.33.05.png)
-----

> Dans mon **app.component.html**, je vais appeler le **Selector comme une balise**.

> Rappel **lorsque l'on crée un composant il faut absolument le déclarer**
----
- je vais créer un composant avec la commande **ng generate component / ng generate c ou ng g c**.

- Je vais appeler ce composant **about** => **ng generate component about**

> Je regarde ce qui a été créé et je vois qu'il m'a ajouté un dossier **about**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-15%20à%2016.25.58.png)

> J'aimerais avoir tous les composants dans un dossier donc **je vais le déplacer**.

- Donc **automatiquement il me déclare mon composant**
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-16%20à%2014.30.56.png)
  
> LORSQUE JE VAIS DANS **ABOUT COMPONENT** on voit bien qu'il m'a créé une classe about component, qu'il à rendu cela exportable car il le déclare dans **app.module.ts**.
      
        - Son selecteur c'est son nom préfixé par APP_ABOUT
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-16%20à%2014.34.57.png)

---
> À noter que les modules se sont aussi des classes mais ils sont **décorés** avec un **ngModules**cette fois et pas **component**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-19%20à%2018.41.55.png)

- On va décorer se composent en disant que sont **Selector c'est app-module**

    - On va dire que sont **TemplateUrl est about**

    - On va dire que sont **StyleUrls est about**
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-19%20à%2018.47.46.png)
      
----
- Dans mon **app.component.html** j'ouvre une balise et je récupère mon composant **about**.
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-19%20à%2018.49.40.png)
  
> Donc là j'ai appelé mon composant !

> Rappel **ligne de commande est ng generate component ensuite le nom**
----
> Lien de la video =>[https://www.youtube.com/watch?v=KEqbZzY9H2Y]

