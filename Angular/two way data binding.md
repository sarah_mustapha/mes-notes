#Two way data binding

- Voila ce qu'on appelle un **data binding**
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-25%20à%2021.57.09.png)
  

> À chaque modification je vais changer avec la valeur cela grâce au inout**.

- Voici la syntaxe:
![img.gnd](../MESIMAGES/Angular/Capture%20d’écran%202021-03-25%20à%2022.01.15.png)
  
----
> Lien de la vidéo =>[https://www.youtube.com/watch?v=dqIo328Rp2s]


