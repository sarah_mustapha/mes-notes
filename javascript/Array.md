# Array (les tableaux)

`Un tableau est une structure de donné qui permet d'organiser des valeurs numérotées`

---

## Contenu d'un tableau

- Des nombres

- Des strings 

- Des objects

`OU d'autre tableaux contenant eux mêmes d'autres tableaux`

- Toutes les valeurs d'un tableau son **accessibles grâce à leurs index** et leurs **position dans le tableau**.

`EN JAVASCRIPT les index d'un tableau commence toujours par 0`

- La première valeur sera toujours a **l'index 0** et le seconde valeur à **l'index 1**.

---

`On utilise la propriété .lenght pour connaitre la longeur d'un tableau`.

- L'index du dernier élément est égale a la longueur du tableau **-1**.

- Les tableaux sont très utilisés pour **stocker des donnés** on peut aussi y stocker des articles (sur un site de blog).

- On peut y stocker **des chaînes de caractères**, **des prénoms** / **les notes scolaires d'un étudient** des **booléen** ainsi que **d'autre tableaux qui eux même on des valeurs**.

___

/Rappel\ Il est préférable lorsqu'on utilise des tableaux **d'utiliser le même type** ou la **même forme dans le cadre d'objet**.

- on trouve dans la documentation de **mozilla** l'ensemble des propriétés détaillée permettant de manipuler des tableaux.
---
## Création d'un tableau

- Pour créer un tableau on utilise des **crochet []**

- Dans les crochets on insère toutes les valeurs que l'on veut insérer dans notre tableau, en utilisant une **virgule ,** pour **séparer chacune des valeurs**.

/conseille\ On peut y ajouter un espace pour que nos valeurs soit plus lisible.

- On sauvegarde le tableau dans une **constante** nommer **arr**.

- Afficher dans **la console la constante arr** qui contient un tableau.

- On peut verifier dans la console notre **index** et notre propriété.

- On peut afficher la **longueur du tableau en faisant un arr.length**

## Création d'un autre tableau

- En créant une **constante** en utilisant l'objet global **Array** , ouvrir des parenthèses (indiquer le nombre de la longueur de notre tableau), y ajouter le mot-clé **new** devant Array.

- Le **new** permet de créer un nouvel object.

- On utilise **.fill** pour remplir le tableau, ensuite on va remplir le tableau avec la **valeur que l'on veut répéter dans le tableau**.

/Attention\ si entre parenthèse je donne qu'**un seul nombre** il va **considérer que je donne la longueur**.

`Il y a deux syntax en réalité (la syntax 1 nombre / la syntax ou l'on met plusieurs éléments entre parenthèse`.

ex : [1 , 2, 3]
ex:  [50*0 / 0 , 0, 0 ,0] // new Array(50).fill("sarah")

`cela va remplir le tableau d'une valeur 50 et sa va remplir le tableau avec que des sarah 50 *`

---

## Accéder à une valeur grâce à son index

- On crée d'abord une constante que l'on nomme **arr** et dans cette constante on crée un nouveau tableau.

/attention\ chaque position est décalé de **1**, peux importe le nombre de valeurs qu'il ya.

- On peut utiliser un tableau **à la place de constante Array**.

_ Pour accéder à la dernière valeur on fait **-1**.

/ ! \ Pour connaitre le dernier élément **dynamique** on utilise **.lenght -1**.
```javascript
let arr = [1, 2, 3];

console.log( arr[1]);

``
=> sa va afficher **2**

## Insérer une valeur dans le tableau

`On nomme nos constantes et nos valeurs d'une manière explicite par au contenu qu'elle vont contenir `.

` Lorsque l'on créé un tableau il est préférable de ne pas mélanger les types`

- À chaque fois que l'on veut une prise de donné on utilise un tableau.

- Pour insérer un élément dans le tableau on utilise la valeur **.push**.

## Supprimer une valeur avec la méthode ".splice()"

`Lire la liste des méthodes sur mozilla`

- Pour supprimer des données on utilise la méthode **.splice** grace au index que l'on insère dans les parenthèses.
-----
```
----
``j``
 arr.splice(start:0, deleteCount:2).

- On utilise **.pop** pour supprimer le dernier élément du tableau.(Cela est plus rapide).

- On peut utiliser **.lenght-1** et de soustraire 1,aussi.

## Remplacer les éléments

- Lorsque l'on utilise -1 cela supprime le dernier élément.

- On utilise **.item** pour remplacer les éléments. On peut **rajouter plusieurs éléments après la virgule**.

- On peut utiliser la methode **arr.length** pour supprimer le reste des éléments si l'on ne connait pas la longueur du tableau. (c'est plus simple).

---
## L'immutabilité

`Pour étaler les valeurs on utilise le spray d'opérateur [...]`