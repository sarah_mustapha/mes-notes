# Chaînes de caractères

---
## Les strings

- Une chaîne de caractère c'est du **texte**

// Syntax

- ' ' ; (les simples guillemets)

- " " ; (les doubles guillemets)

- les back tic => ``

---

/!\ `Nous devons choisir quelle syntax que l'on va uttiliser, et l'on doit s'y tenir pour nôtre code , ne pas mélanger les chaines de charactères`.

---
/!\ `Toujours ouvrir et fermer la chaîne de charactère`

---

## Échapper des caractère

- On utilise un back slash => \ pour échapper le single côte. (cela permet au lecteur de lire correctement le code).

- On peut faire un `console.log` pour verifier le résultat dans la console.

- Pour faire un saut de ligne on utilise le back slash => ` \n\ `

- Pour ajouter un espace on utilise => \r`

---

## Concaténation

- Une concaténation c'est l'ajout de 2 chaîne de caractères entre elle.

- `Une chaîne de caractère peux être additionnées avec une autre chaîne de charactère (que l'on créé directement par exemple).`

- Pour cela on utilise le => `+`

---
## Moteur de recherche JAVASCRIPT

- Lorsque qu'on effectue une recherche en Javascript `il faut toujours la commencé par javascript.`

## La longueur d'une string

- On utilise => `.lenght` pour connaitre la longueur de notre texte dans la nôtre chaine de caractère.


## Mettre en majuscule une chaîne de caractère

- On utilise => `.toUpperCase` pour metre une chaîne de caractère en Majuscule.

- / ! \ Il ne faut pas oublier d'ouvrir et de fermer une parenthèse = `()`.

- On utilise la constante `const` pour modifier la chaîne de caractère et pour pas que le contenu change.

- / ! \ Il ne faut pas oublier les majuscules dans les noms de méthodes (toUpperCAase)

- Nous devons changer la `constante` en `variable` (let) pour pouvoir la réassigner.

## Mettre en minuscule une chaîne de caractère.

- Pour la metre en minuscule on utilise ma méthode `.toLowerCase`.

- Ne pas oublier une parenthèse ouvrante et fermente pour modifier la méthode.

---
## Accéder à un caractère grâce à sa position

- La première lettre de la chaîne de caractère correspond au chiffre [0] 

- On utilise la méthode `.includes ` pour connaitre le contenu de la chaîne de caractère.

- On utilise la méthode ``startsWith`` pour connaitre le commencement de la chaîne de caractère.
  
- On utilise `[.length -1]`pour vérifier la longueur de la chaîne de caractère de manière dynamique.

## La position d'une ``string dans une autre string``

- On utilise la propriété ``.indexOf`` qui indique la chaîne de caractère que l'on recherche dans les parenthèses().

- On utilise la méthode `.replaceAll` pour remplacer **toutes** les (occurrences **lettre**)de la chaîne de caractère.(mettre les éléments dans une parenthèse avec une virgule).

- On utilise la méthode `.split` pour séparer le contenu de la chaîne de caractère.

- On utilise la méthode `.trim` pour effacer les espaces en **début et fin** d'une chaîne de caractère.