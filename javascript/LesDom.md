> - Sommaire:
> - [S'assurer que le dom est chargé](#sassurer-que-le-dom-est-charg)
> - [Sélectionner un élément grâce à son id](#slectionner-un-lment-grce--son-id)
> - [Sélectionner des éléments grâce à leurs classes](#slectionner-des-lments-grce--leurs-classes)
> - [Sélectionner un ou des éléments](#slectionner-un-ou-des-lments)
> - [Manipuler le style d'un élément](#manipuler-le-style-dun-lment)
> - [Manipuler les classes d'un élément](#manipuler-les-classes-dun-lment)
> - [Créer et ajouter un élément au DOM](#crer-et-ajouter-un-lment-au-dom)
> - [Écouter les événements du dom](#couter-les-vnements-du-dom)
> - [Fonction arrow](#fonction-arrow)
----

# S'assurer que le dom est chargé

![img.gnd](../MESIMAGES/Capture%20d’écran%202021-01-22%20à%2015.10.06.png)
> Avant de manipuler le dom il faut s'assurer que le dom est chargé, lorsque le navigateur est chargé il va récupérer ma page html.
> 
> Le navigateur v'a d'abord interpreter tous les éléments qui sont dans la balise head donc le titre de la page les mes adonnées donc la description des mots clés liés à la page puis ensuite il va s'occuper d'interpréter la structure de ma page internet, les balises textuel comme le h1, la balise de paragraphe etc..., tous cela sera interprété après la balise head.
> 
> **Tous les éléments qui sont interprété dans ma balise head vont ralentir l'interprétation de la page en elle-même pour l'utilisateur**.

- On peut faire un test en **ajoutant une balise ouvrante et fermante **script, dans le head** que je vais afficher dans la console1**.

- Ensuite dans ma balise body je vais également rajouter **une balise une balise script ouvrante et fermente script** qui aura la **tribu type** qui aura **la valeur text javascript et je vais afficher cette fois 2**.

> Dans l'ordre ce sera le 1 puis le 2 qui seront exécuté, car tout les éléments qui sont dans le head son d'abord interprété par le navigateur avant même la page internet.
> 
> Si j'ai des script dans mon aide qui son très complexe cela va ralentir l'interprétation de ma page
![img.gnd](../MESIMAGES/Capture%20d’écran%202021-01-22%20à%2015.10.06.png)

>**/ ! \ On ne lit pas les éléments JAVASCIPT dans le head de la page HTML, mais plutôt à la fin du body car il vaut mieux avoir une page html qui est inactive mais qui est présente plutôt qu'avoir une page html qui peut être active avec des animations mais qui n'a aucun élément animé.

- **Dans l'ordre on préfère d'abord affiché à l'utilisateur même si cela peut prendre plus de temp, puis télécharger et exécuter le javascript**.
![img.gnd](../MESIMAGES/Capture%20d’écran%202021-01-22%20à%2016.56.06.png)
  
- **D'abord on charge le style(tous les éléments) et on l'applique à la structure du Html et en tout dernier c'est Javascript**.
----
- J'ajoute un **fichier index.js**et je le **lis à mon fichier index.html**.

- J'ajoute la **balise script sur ma page html** avec la tribu **src** pour **source**et la valeur sera le chemin d'accès vers mon fichier
![img.gnd](../MESIMAGES/Capture%20d’écran%202021-01-22%20à%2017.05.05.png)
  
`Exemple: Si mon fichier est dans un sous dossier (app.js) c'est le même principe** sauf qu'on ajoute un slash / suivi de mon sous dossier puis /app.js`
![img.gnd](../MESIMAGES/Capture%20d’écran%202021-01-22%20à%2017.09.57.png)

- Donc ici j'ai créé deux ficher => **app.js et index.js**.

- J'affiche dans la console les deux fichiers, on voit **qu'index.js est téléchargé avant le pp.js**.
----
> Comment s'assurer que le dom est chargé ? 

- Le dom ce sont les éléments HTML qui vont ête affiché à l'utilisateur.

- On a vu comment placer le JAVASCRIPT pour s'assurer que le JAVASCRIPT EST INTERPRÉTÉE après le chargement du dom donc après l'interprétation de ma page HTML.

- J'affiche dans mon index.js **la console mon document que j'ai sélectionné suivit de mon titre h1.**
![img.gnd](../MESIMAGES/Capture%20d’écran%202021-01-22%20à%2017.23.26.png)
  
- On voit que cela fonctionne car on n'a placé notre liaison à la fin **de la balise body**.

> Il y a une autre méthode pour s'assurer que notre dom est chargé, **on appelle notre objet global window.**
````javascript
console.log('index.js');

console.log(document.querySelector(selectors:'h1'));

console.log(window);
````
- **Lorsque j'affiche window cela va m'indiquer que c'est un objet avec des propriétés dont lequel j'ai accès, avec une fonction qui me permet de fermer la fenêtre si je le souhaite avec des informations active => hauteur, largeur, j'ai d'autre information et d'autre méthodes qui peuvent être enregistré ou utilisé grâce à l'objet window**.

- J'aimerais **ajouter à l'objet window** la propriété **onLoad qui est déjà présente** et j'aimerais lui donner une **fonction**.
````javascript
console.log('index.js');

console.log(document.querySelector(selectors:'h1'));    // assignasion

console.log(window);
window.onload = function() {
    console.log('4');
}
````
- Si on **recharge onload dans la console** elle est déjà présente et **sa valeur c'est null**

- Cette **fonction sera exécutée** à la fin du chargement du **dom**.
````javascript
console.log('index.js');

console.log(window);
window.onload = function() {
    console.log('4');

    console.log(document.querySelector(selectors:'h1'));     // assignasion
}
````
- Si j'applique mon **querySelector à la fin de ma fonction**, on voit dans la console mon titre h1 est affiché à la fin du dom après le 4 alors que mon **index.js est toujours dans le head** car jai attendu que ma page soit téléchargé ajouté pour ajouter la function **window.onload**.

- Je peux déplacer mon assignation au **début du fichier le résultat sera le même**, car je dois d'abord attendre que ma fenêtre est finis de télécharger pour **exécuter ce que je lui ai donné dans ma fonction**.

> **L'autre méthode, comme pour la première** dans ma fenêtre ajouter une nouvelle écoute d'événements en faisant => **window.addEventListener()**
````javascript
window.onload = function () { 
    console.log('4');
    console.log(document.querySelector(selectors:'h1'));
};

window.addEventListener(type:'DOMContentLoaded', listener:function() {
    console.log('5');
    console.log(document.querySelector(selector:'h1'));
});
````
- Je vais **écouter DOMContentLoaded** qui est le nom de l'événement en **deuxième argument je vais luis passer une fonction callback** et cette **fonction callback** va faire la même chose je vais lui afficher 5 et également sélectionné mon titre.

- On peut voir dans la console que c'est seulement à **la fin du téléchargement du dom il exécutera la fonction qui affiche 5 et qui récupèrent mon titre**.

> Cela sont les deux moyens qui me permet de m'assurer que **le dom est chargé avant de le manipuler**.

- Si j'ai besoin de récupérer le titre pour lui ajouter une couleur, une class ou le faire bouger, j'ai d'abord besoin de m'assurer que le titre est chargé.

> Il faut attendre que le dom soit chargé grâce au **DOMContentLoaded**.
````javascript
window.onload = function () { 
    console.log('4');
    console.log(document.querySelector(selectors:'h1'));
};

window.addEventListener(type:'DOMContentLoaded', listener:function() {   //la fonction qui sera éxécuter sera le deuxième argument
    console.log('5');
    console.log(document.querySelector(selector:'h1'));
});
````
> La fonction qui sera exécuté lorsque l'événement DOMContentLoad sera lancé ce sera la fonction du deuxième argument, donc ici la fonction qui affiche 5 et qui récupère également le titre.

> On voit que le 5 et exécutés après **le index.js** et après l'affichage de l'objet window alors qu'**ils sont définis après sur mon fichier**.
> 
> Il faut bien comprendre l'aspect différé, ici j'assigne simplement des fonctions à des propriétés mais les propriétés ne sont pas exécuté, elles seront exécutés après un certain moment.
> 
> Donc à partir du moment ou **je veux manipuler le DOM** j'ai besoin de **m'assurer que le dom est chargé**, donc tout **le script qui nécessite la manipulation du dom**, il faut que je le mette soit dans **une fonction ici qui sera assigné à la propriété onload de l'objet window**, soit dans la **fonction callback de l'écoute de l'événement de mon côté Loaded** sur la fenêtre (window), donc il faudra toujours entourer notre script qui manipule le dom par soit => (window.onload = function) soit => **window.addEventListener(type:'DOMContentLoaded', listener:function() {**.
>
> Lien de la vidéo => [https://www.youtube.com/watch?v=xlYXBUe3SAQ&feature=youtu.be]
----
# Sélectionner un élément grâce à son id

> Dans mon fichier index.HTML j'ai une div qui a l'**ID "sqr"** et la **class = "square"**.
> 
![img.gnd](../MESIMAGES/Capture%20d’écran%202021-01-22%20à%2021.56.36.png)
> 
> La class square donne à **tous les éléments qui on la class square une largeur de 100px, un hauteur de 100px et une couleur rouge en fond**.

![img.gnd](../MESIMAGES/Capture%20d’écran%202021-01-22%20à%2022.01.17.png)

- On voit **le résultat sur ma page web sa donne un gros carré rouge**.

> J'aimerais récupérer dans mon script JAVASCRIPT cet élément pour cela je vais devoir utiliser **l'objet global document** je vais **afficher cet objet dans la console**, le document va représenter le document de ma page HTML.

> Grâce à l'objet document je peux récupérer et manipulé en JAVASCRIPT
> 
>  J'aimerais récupérer la div **grâce à sont attribué ID**, **grâce à SQR**

- Pour cela on utilise documents dont on n'a accès à des méthodes qui nous permettent de manipuler le document moi je veux récupérer l'élément, donc j'utilise la méthode **document.getElementById** puisque je veux récupérer un élément **grâce à son Id**.

- Entre guillemet je vais devoir indiquer ma recherche **sqr**

- On remarque qu'on tape **Element sans s** car **id** est normalement utilisé pour taguer **un seul élément**.

![img.gnd](../MESIMAGES/Capture%20d’écran%202021-01-22%20à%2022.17.19.png)

- Si on n'a besoin de **taguer plusieurs éléments on utilise des class**, puisque les class peuvent être assigné à plusieurs éléments en même temps.

- Un **id** est **unique à chaque élément**.

- Si je définis **deux éléments qui on les mémes id** on le voit c'est **souligné en rouge**.
![img.gnd](../MESIMAGES/Capture%20d’écran%202021-01-22%20à%2022.23.31.png)
  
> DE PRÉFÉRENCE ON UTILISENT LES **ID** POUR LE **JAVASCRIPT**, pour manipuler les élémentS avec le JAVASCRIPT et **non pas pour styliser les éléments**.

- **Pour styliser les éléments on va favoriser l'utilisation des class**, sur mon html.
----
- Sur ma page JAVSCRIPT je récupère un seul élément que je vais **stocker dans la propriété** **sqr** ou **square qui veut dire carré en anglais* ( on le nomme comme l'on veut).

- J'affiche les éléments récupéré dans la console.
![img.gnd](../MESIMAGES/Capture%20d’écran%202021-01-22%20à%2022.34.56.png)
  
- J'ai bien cet élément dans ma console que **je pourrais manipuler, ajouter une classe, changer sa couleur, faire bouger etc..**

> LIEN DE LA VIDEO => [https://www.youtube.com/watch?v=eHDOuEVDU48&feature=youtu.be]
----
# Sélectionner des éléments grâce à leurs classes

> On va voir comment sélectionner des éléments grâce à une classe ou plusieurs classes en même temps.

````html
<style>
.square {
    width: 100px;
    height: 100px;
    background-color: red;
    display: inline-block;  // aligner les éléments
    margin: 10px;         // écarter les éléments
}
.bigSquare {
    widht: 200px;
    height: 200px;
}
</style>

</head>

<body>
<h1>Javascript</h1>

<div class="square"></div>
<div class="square"></div>
<div class="square"></div>
<div class="square"></div>


<script src="index.js"></script>>

</body>
````
- J'ai quatre éléments qui on la **class square**

- J'ai modifié la propriété je lui ai **rajouté display et une marge**, pour pouvoir aligner les éléments dans la même ligne et les séparer de 20 px, **car chaque carré a une marge de 20 px donc 10 + 10 = 20 px.**

- **J'aimerais sélectionner tous ces éléments grace a la classe square car ils possèdent tous la classe square**.

> Pour cela j'utilise **la méthode getElementsByClassName(avec un s car je vais récupérer plusieurs éléments sur mon fichier JS**
````javascript
console.log("Sélectionner un élément à son id");

window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !")
    
    cont squares = document.getElementsByClassName(className:"square");
    console.log(squares);
});
````
- **Une classe peut s'appliquer sur plusieurs éléments donc je recevrai toujours un tableau HTML**.

- **J'applique en paramètre de ma méthode la class sur lequel je vais effectuer ma recherche, donc ce sera la class square**

- Les éléments retourné par cette méthode je vais les stocker dans une **constante qui sera la class square**, et je vais l'afficher dans la console.

![img.gnd](../MESIMAGES/Capture%20d’écran%202021-01-24%20à%2019.42.03.png)

- Donc là on voit qu'on a bien **récupéré nos éléments grâce à la class square**.

> Il est important de comprendre que cette fois on va récupérer une collection de tableau d'éléments HTML.
> 
> Ensuite il faudra boucler dans le tableau pour pouvoir modifier les propriétés d'un élément.
> 
> LIEN DE LA VIDEO => [https://www.youtube.com/watch?v=esFTGa7WLIU&feature=youtu.be]

----

# Sélectionner un ou des éléments
```html
<body>
<h1>Javascript</h1>

<div class="square"></div>
<div class="square bigSquare"></div>
<div class="square"></div>
<div class="square bigSquare"></div>

<section class="square"></section>
<section class="square"></section>
<section class="square"></section>
<section class="square"></section>



<script src="index.js"></script>>

</body>
```
> ICI j'ai 4 div avec la class square, 2 div avec la class bigSquare.
> 
> J'ai également 4 sections avec la class square.

![img.gnd](../MESIMAGES/Capture%20d’écran%202021-01-24%20à%2019.53.19.png)

> Voici le résultat
>
> J'aimerais récupérer le premier élément qui a la class square, cette fois en utilisant **une autre méthode que => document.getElementsByName**
>
> Cette fois je vais utiliser la méthode de l'objet document qui se nomme querySelector.
````javascript
console.log("Sélectionner un élément à son id");

window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !")
    
    cont square = document.querySelector(selectors:"#sqr"); //Si je veux sélectionner plusieurs éléments grâce à la class square, **je rajoute un point après le square et je nomme la seconde class**.
    console.log(square);
});
````
- En paramètre je veux **lui donner la class square**en ajoutant un point comme cela => **.square**, car **grâce à une class je rajoute un point**.

- Cet élément je vais le nommer dans une constante que je vais nommer square et je vais l'afficher dans une console.

- Si je veux sélectionner plusieurs éléments grâce à plusieurs class square, **je rajoute un point après le square et je nomme la seconde class qui sera bigSquare**.

- Donc il va me sortir **le premier élément qui a les deux classes.**

![img.gnd](../MESIMAGES/Capture%20d’écran%202021-01-24%20à%2020.11.08.png)

- Ici on sélectionne qu'**un seul élément querySelector**
  
- **Pour sélectionner un élément grâce à son id je rajoute un hashtag#** et indiquer **son id** car je veux récupérer lélément sur mon HTML qui a l'ID **sqr**.

> Dans notre cas ici on n'a aucun élément cela va **m'envoyer null**.

```html
<body>
<h1>Javascript</h1>

<div class="square"></div>
<div class="square bigSquare"></div>
<div class="square"></div>
<div class="square bigSquare"></div>

<section id="sqr" class="square"></section> // première section
<section class="square"></section>
<section class="square"></section>
<section class="square"></section>



<script src="index.js"></script>>

</body>
```
> Mais si je donne la **tribu id qui est égale à square à la première section** je réussis à récupérer mon élément **grâce à son id**.

![img.gnd](../MESIMAGES/Capture%20d’écran%202021-01-24%20à%2020.23.13.png)

- Ce que je peux faire également c'est de **sélectionner des éléments grâce au nom du tag**.

`````javascript
console.log("Sélectionner un élément à son id");

window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !")
    
    cont square = document.querySelector(selectors:"section"); 
    console.log(squares);
});
`````
> Dans ce cas je ne rajoute pas de point car **l'interpréteur va considérer que je recherche une class**
> 
> Je ne rajoute pas non plus un # sinon il va considérer que je recherche un id.
> 
> Je met rien car il va considérer de me retrouver **un élément qui a ce nom de balise donc section**, et l'interpréteur va me ressortir le premier élément.
> 
> **Cette règle s'applique également lorsque l'on veut rechercher plusieurs éléments avec la méthode .querySelectorAll**

- Donc ici on ne récupère pas un seul élément, **on va récupérer plusieurs éléments qui on la class square**
![img.gnd](../MESIMAGES/Capture%20d’écran%202021-01-24%20à%2020.34.45.png)
  
- On le voit ici on n'a **tous les éléments qui on la class square**.

> On peut rechercher **seulement les div qui on la class square de cette manière**
````javascript
console.log("Sélectionner un élément à son id");

window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !")
    
    cont square = document.querySelector(selectors:"div.square"); // on utilise cette manière pour rechercher les élément squi on la class square de cette manière.
    console.log(squares);
});
````
> Que l'on utilise le selectors ou le querySelector ici c'est ma même règle de sélection.

- ON peut rechercher **une div en donnant le nom de la balise qui a la class square**.

- Don on va ressortir **seulement les div qui on la class square**.

![img.gnd](../MESIMAGES/Capture%20d’écran%202021-01-24%20à%2020.41.40.png)

- **On peut enchainer en recherchant les div qui on la class square et la class bigSquare**.
````javascript
console.log("Sélectionner un élément à son id");

window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !")
    
    cont square = document.querySelector(selectors:"div.square.bigSquare"); // on utilise cette manière pour rechercher les élément squi on la class square de cette manière.
    console.log(squares);
});
````
- Cela va nous ressortir **toutes les div qui on les deux class et qui on les noms des balises qui est égale à div**.
![img.gnd](../MESIMAGES/Capture%20d’écran%202021-01-24%20à%2020.45.37.png)
  
- **On va faire une recherche au niveau des sections**, ici il n'y aura **aucune section qui a les deux classes sur notre console, par contre il y'aura des sections qui on la class square**
````javascript
console.log("Sélectionner un élément à son id");

window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !")
    
    cont square = document.querySelector(selectors:"section.square.bigSquare"); 
    console.log(squares);
});
````
> Le principal c'est la manière de sélectionner, **si on veuxtsélectionner par classes on ajoute un point**.
> 
> **Si on veut sélectionner par les divs on ajoute un hashtag # avant le nom de l'id**
> 
> **Si on ne met rien on v'a sélectionner grâce au nom de la balise**.

- **Donc si je veux sélectionner le h1, j'utilise le nom de la balise h1**.
````javascript
console.log("Sélectionner un élément à son id");

window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !")
    
    cont square = document.querySelectorALL(selectors:"h1"); 
    console.log(squares);
});
````
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-24%20à%2020.54.22.png)

- **Comme j'ai utilisé la méthode querySelectorAll cela me retourne un NodeList**

- **Si je veux récupérer qu'un seul élément j'utilise la méthode querySelector**

> LIEN DE LA VIDÉO =>[https://www.youtube.com/watch?v=MYdFSsFooeM&feature=youtu.be]
----
# Manipuler le style d'un élément

- j'aimerais modifier le style de **mon premier carré ici pour le rendre jaune**.

![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-24%20à%2021.02.10.png)

````javascript
console.log("Sélectionner un élément à son id");

window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");
    
    document.querySelector(selectors:".square")
});

````
- **Pour modifier mon premier carré il faut d'abord que je le récupère**.

- On n'a déjà vu comment on **récupère en faisant => document.querySelector()**.

- J'avais récupéré **le premier carré qui a la class square**.

- Le premier carré **je vais le stocker dans la constante square**

````javascript
console.log("Sélectionner un élément à son id");

window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");
    
  const square = document.querySelector(selectors:".square");
    console.log(square);
});
````
- **Je vais afficher square dans la console pour m'assurer que j'ai bien mon carré**.
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-24%20à%2021.09.52.png)
  
- Effectivement j'ai bien mon premier carré avec **la class square**.

> **Maintenant que j'ai mon élément je vais pouvoir modifier son style**.
>
> Pour cela j'accède à **la propriété style de mon élément**.

````javascript
console.log("Sélectionner un élément à son id");

window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");
    
  const square = document.querySelector(selectors:".square");
  
  square.style.backgroundColor = "yellow"
    console.log(square);
});
````
- Ensuite je pourrai **modifier toutes les propriétés css de mon élément.**

- Exemple son background color (en css c'est avec un tiret background-color et en kamelcase on remplace par le **C majuscule**).

- **Donc la première lettre de chaque nouveau mot sera mis en majuscule**.

- On va pouvoir modifier la couleur, donc moi **je voulais du jaune**. 
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-24%20à%2021.18.45.png)
  
- Donc ici **j'ai réussis à modifier le style de mon premier élément que j'ai sélectionné**.

- **Je peux également modifier d'autre style de cet élément là**, par exemple **la bordure=> (margin)**.
````javascript
console.log("Sélectionner un élément à son id");

window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");
    
  const square = document.querySelector(selectors:".square");
  
  square.style.backgroundColor = "yellow";
  square.style.border = "1px solid gray";
    console.log(square);
});
````
- Donc la je vais lui **rajouter une bordure solid et de couleur grise**.
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-24%20à%2021.28.44.png)
  
- On voit bien que cala modifie également.

- Je peux également modifier **le style de l'élément en modifiant la position.**
````javascript
console.log("Sélectionner un élément à son id");

window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");
    
  const square = document.querySelector(selectors:".square");
  
  square.style.backgroundColor = "yellow";
  square.style.border = "1px solid gray";
    
    square.style.position = "fixed"; // position fixé
    square.style.top = "200px"; // hauteur/ valeur + unité
        
    console.log(square);
});
````
- **Je vais également modifier sa position au niveau de sa hauteur en utilisant =>top**, ici il faut **donner sa valeur ainsi que l'unité**.
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-24%20à%2021.35.51.png)
  
- Sa position sera fixé par rapport au top, donc **il y aura 200px entre le haut de la page et la position du carré**.

- On peut modifier cela de manière dynamique pour cela **on utilise => setTimeoput** ou un **setInterval**.

- **La fonction setInterval** prend en premier argument **la fonction callback sui sera exécuté en fonction de l'interval**.
````javascript
console.log("Sélectionner un élément à son id");

window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");

    
  const square = document.querySelector(selectors:".square");
  
  square.style.backgroundColor = "yellow";
  square.style.border = "1px solid gray";
    
    square.style.position = "fixed"; // position fixé
    square.style.top = "200px"; // hauteur/ valeur + unité
    
    setInterval(handler:function() {     //Interval c'est celui qui définis le deuxième argumengt
        
    }, timeout:2000);     // toute les 2000 mille seconde
        
    console.log(square);
});
````
- **Interval c'est celui qui définis le deuxième argument**.

> **L'interval est définis en mini seconde** 

- Toute les 2000 mill seconde je vais exécuter cette fonction.
````javascript
  setInterval(handler:function() {     //Interval c'est celui qui définis le deuxième argumengt
        
    }, timeout:20
````
- **Cette fonction** va **modifier la position** de mon élément, **on va dire que mon élément est à la position 0 par apport au top.**
````javascript
  
  const square = document.querySelector(selectors:".square");
  
  square.style.backgroundColor = "yellow";
  square.style.border = "1px solid gray";
    
    square.style.position = "fixed"; // position fixé
    square.style.top = "0px"; // hauteur/ valeur + unité
    let positionY = 0;    // je stock ma position
    
    setInterval(handler:function() {     //Interval c'est celui qui définis le deuxième argumengt
        position++;  // j'incrément la position.
        square.style.top = position + "px";
    }, timeout:2000);     // toute les 2000 mille seconde
        
    console.log(square);
});
````
- **Je vais stocker dans une variable position l'incrémentation de la position**.

- Donc **toutes les 2000 seconde je vais déplacer mon élément avec la valeur de la position**.

- Je vais aussi incrémenter la position, **donc toutes les 2 secondes la position va passer de 0 à 1, etc.. *.

- **Le style de mon carré va être modifié au niveau de la propriété top.**

![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-24%20à%2021.56.25.png)

- **Donc là mon élément va se déplacer toutes les 2000 seconde, cela est vraiment petit. Ce qu'on peut faire c'est de baisser l'interval 0 500 mini seconde**.Donc là on le voit **bouger beaucoup plus rapidement.**

> On peut faire la même chose en ajoutant un **autre interval qui lancera la fonction callback** indiqué en premier argument.
````javascript
 
    setInterval(handler:function() {     //Interval c'est celui qui définis le deuxième argumengt
        positionY++;  // j'incrément la position.
        square.style.top = position + "px";
    }, timeout:100);     // toute les 100 mille seconde
    let positionX = 0;
    setInterval(handler:function() {
    positionX++;  // j'incrément la position.
    square.style.top.left = positionX + "px";  // on utilise la ,propriété left
}, timeout:100);

    ]);
    con1sole.log(square);
````
- Donc **l'interval sera de 100 mini secondes**.

- Ce que cela va faire c'est **d'incrémenter de + 1px toutes les 100 minis secondes**, la position de X.

- Je fais la même chose **déclarer la position X** et **Incrémenter la valeur toutes les 100 minis secondes**.

- On utilise la propriété **left**, car je veux **déplacer l'élément vers la gauche**.
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-24%20à%2022.14.41.png)
  
- On peut changer la valeur qui est incrémenter de +1.

- Ce que je peux faire c'est **d'accélérer cela en disant que mon élément va bouger par apport à la position de 10px**.
````javascript
setInterval(handler:function() {
    positionX = positionX + 10;
    square.style.top.left = positionX + "px";  // on utilise la ,propriété left
}, timeout:100);

    ]);
    con1sole.log(square);
````
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-24%20à%2022.21.17.png)

> Donc on peut créer des cinématiques avec tous les éléments.

- Ce que je peux faire c'est de **rajouter une condition** en disant que si la position de mon élément, donc ma **positionX elle est supérieure ou égale à 500**. 
````javascript
setInterval(handler:function() {
    if (positionX >= 500)
        square.style.position = relative
    }
    positionX = positionX + 10;
    square.style.top.left = positionX + "px";  // on utilise la ,propriété left
}, timeout:100);

    });
    con1sole.log(square);
````
- Ici mon carré je vais **changer son style au niveau de la position**.
, et dire que sa position sera relative.
  
- Donc on va attendre que lorsqu'elle va attendre les 500Px, **la position de mon carré va passer en relatifs.(DIAGONALE)**
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-24%20à%2022.31.35.png)
  
> On aurait aussi pu entourer notre logique, cela va déplacer notre élément tan que **la positionX est inférieur ou égal à 500.**
````javascript
setInterval(handler:function() {
    if (positionX <= 500)
        square.style.position = relative
        positionX = positionX + 10;
        square.style.top.left = positionX + "px";  // on utilise la ,propriété left
    }
}, timeout:100);

    });
    con1sole.log(square);
````
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-24%20à%2022.37.13.png)

- On le voit ici on n'a **une condition qui arrête notre élément**

> D'abord il faut **récupérer avec une bonne requête**, **s'assurer qu'on récupère bien un élément** si on veut qu'un seul élément et non une liste d'élément.
> 
> Ensuite on peut **modifier son style en accédant à la propriété style, en modifiant les styles CSS de notre élément**.

- À la place de le déplacer on peut le sintiller => de changer de couleur toutes les 1 secondes
```javascript
let i = 0;
setInterval(handler:function() {
 
        square.style.backgroundColor = i % 2 ? "red" : "yellow";
        i = i + 1;
    }
}, timeout:100);

    });
    con1sole.log(square);
```
- Ici square c'est son élément, je change son **style de fond et je veux le faire passer en rouge toutes les unes secondes**ou en jaune.

- Pour cela **je vais stocker une variable que je vais nommer i**

- Je vais **incrémenter i + 1** je peux le **réduire avec le i++**

- Si modulo **% de i vaut 1 alors ce sera rouge.**

> Donc la c'est une simple condition ternaire =>  square.style.backgroundColor = i % 2 ? "red" : "yellow"
> 
> Je vérifie que i est pair =>  i % 2 

- Quand **i vaut 0** la valeur sera, donc (le fond du carré vaudra ) **yellow** et quand **i vaudra 1 cela sera red**.
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-24%20à%2022.57.54.png)
  
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-24%20à%2022.58.07.png)

> **On le voit dans la console toutes les une seconde le carré change de fond**. 
> 
> Lien de la vidéo => [https://www.youtube.com/watch?v=H_Lq125g42I&feature=youtu.be]
-----
# Manipuler les classes d'un élément

> J'aimerais modifier mon premier élément pour lui **rajouter la class bigSquare**, pour **le faire grossir toutes les secondes et lui enlever toutes les secondes également.**
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-24%20à%2022.57.54.png)

![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-24%20à%2022.58.07.png)

- Donc ici je vais faire **grossir puis rétrécir mon premier élément**, Comme on n'a fait pour la propriété backgroundcolor.
````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");
    const square = document.querySelector(selectors:".square");
    console.log(square);
});
````
- Pour manipuler les classe d'un élément **il faut d'abord récupérer l'élément.**

- Ici l'élément je vais le stocker dans la **constante square**.

- Je vais le récupérer grâce à **la méthode qui se trouve dans l'objet document qui se nomme querySelector**.

- Je vais **sélectionner mon élément grâce à la classe square**.

- La console va me **récupérer le premier élément qui a cette classe.**
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2010.57.27.png)
  
- j'ai effectivement bien récupérer .

> Pour la suite je veux créer un sentiment
````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");
    const square = document.querySelector(selectors:".square");
    console.log(square);
    
    setInterval(handler:function() {
        
    });
});
````
- Je vais rajouter à **setInterval**, **en premier argument je donne une fonction callback** qui sera exécuté en fonction de l'Interval qui sera défini dans le **Second argument**.
````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");
    const square = document.querySelector(selectors:".square");
    console.log(square);
    
    setInterval(handler:function() {
        
    }, timeout: 1000);  // second argument
});
````
- **Le second argument est un nombre de minis seconde qu'il faut entre chaque exécution de la fonction callback**.

- Cette fonction va ajouter à la classe une nouvelle classe qui sera **la propriété classList** ensuite je vais accéder à **la méthode add**
````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");
    const square = document.querySelector(selectors:".square");
    console.log(square);
    
    setInterval(handler:function() {
        square.classList.add("bigSquare");
    }, timeout: 1000);  // second argument
});
````
- J'ajoute dans **les parenthèses bigSquare**=> **square.classList.add("bigSquare");**

- Ce qu'il va se passer c'est que toutes les secondes il va me **rajouter à la fonction la class bigSquare**
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2011.10.11.png)
  
- On va **rajouter une variable i qu'on va incrémenter de plus 1 +1**
`````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");
    const square = document.querySelector(selectors:".square");
    console.log(square);
    
    let i = 0;
    setInterval(handler:function() {
        if (i % 2) {         // si i est pair
            square.classList.add("bigSquare");
        } else {
            square.classList.
        }
        square.classList.remove(tokens:"bigSquare");
        i++;
    }, timeout: 1000);  // second argument
});
`````
- **On ajoute une condition** classique avec **le mot clé if**.

- Si i est **pair** alors on ajoute **bigSquare**, **sinon on retire la classe bigSquare**.

- Pour retirer la class bigSquare il faut **accéder à cet élément** puis **accéder à sa classList** et utiliser la méthode **remove**.

> **La condition if c'est simplement pour retirer le scintillement, le fait que cela augmente puis que cela rétrécissent** en fonction de l'utilisation.
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2011.23.05.png)
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2011.23.18.png)

- On voit dans la console que cela **augmente puis une seconde après cela rétrécis**.

> Recap => On n'a rajouter la classe bigSquare **square.classList.add("bigSquare");**
> 
> Puis la seconde fois que la fonction est exécuté **square.classList.remove(tokens:"bigSquare");** on **enlève cette classe**.
>
> On n'a seulement **deux méthodes à manipuler** les classes au niveau des éléments.
> 
> On peut ajouter la class **bigSquare**, on peut également retirer cette classe là.

- On peut également rajouter plusieurs classes.
````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");
    const square = document.querySelector(selectors:".square");
    console.log(square);
    
    let i = 0;
    setInterval(handler:function() {
        if (i % 2) {         // si i est pair
            square.classList.add("bigSquare", "ok");
        } else {
            square.classList.
        }
        square.classList.remove(tokens:"bigSquare","ok");
        i++;
    }, timeout: 1000);  // second argument
});
````
> Lien de la vidéo => [https://www.youtube.com/watch?v=b5Th1rw_73Y&feature=youtu.be]
----
# Créer et ajouter un élément au dom

> **Pour créer un élément** on va ajouter la méthode => document.**createElement**.

- En premier paramètre je dois ajouter et indiquer qu'elle est le tag de l'élément.
````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");

    document.createElement(tagName"div");
});
````
1. si je vaux créer une div j'ajoute une **div**.
2. si je vaux créer une section j'ajoute une **section**.
3. si je veux créer un lien je met un **a**.
4. si je veux ajouter un titre je met un **h1**/h2/ etc...
5. si je veux ajouter un paragraphe je met un **p**

- Dans mon cas je veux seulement créer une div.

- **L'élément qui est créé** va mettre envoyer par cette méthode => **document.createElement(tagName"div");** dans une **constante que je vais nommer square**.
````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");

   const square = document.createElement(tagName"div");
   console.log(square);
});
````
- Je peux ensuite afficher square dans ma **console.**
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2011.50.47.png)
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2011.52.22.png)

- Ici on voit bien qu'on n'a **une div qui est vide**.

- Si je crée une section on verra que ce sera **une section qui sera créé.**

- Je veux ajouter à **cet élément la classe square**.
`````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");

   const square = document.createElement(tagName"section");
   
   square.classList.add("square"); //j'ajoute la class square
   
   console.log(square);
});
`````
> On n'a déjà vu comment **ajouter et supprimer des classes**.
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2011.56.41.png)

- On voit bien que notre section ici a **la class square**.

- On peut également **ajouter du texte.**
````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");

   const square = document.createElement(tagName"section");
   
   square.classList.add("square"); //j'ajoute la class square
    
    square.inerText = "ok";         //ajouter du texte
   
   console.log(square);
});
````

- Ici on n'a **ajouté du texte a notre élément** en utilisant => *square.inerText = "ok";** 
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2012.02.05.png)
  
- On voit que le texte a été ajouté.

> Pour accéder au texte pour un élément qu'on n'a de présent cela sera la propriété => **innerText**

- Si ON AJOUTE DU HTML SA NE SERA PAS **innerText**.

> Pour insérer du HTML dans un élément il faut l'insérer dans la propriété **innerHTML**.
````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");

   const square = document.createElement(tagName"section");
   
   square.classList.add("square"); //j'ajoute la class square
    
    square.innerHTML = <strong>ok</strong>";         //ajouter du texte en html
   
   console.log(square);
});
````
> **Dans ce cas là notre html sera considéré comme du html et comme un enfant de notre élément.**
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2012.08.04.png)

> Il faut faire attention à comment on utilise ses deux propriétés.

- On va ajouter notre élément dans notre page, pour cela on d'abord récupérer l'élément sur lequel on veut ajouter.

- Dans mon cas ce sera **document.body**, car je veux **l'ajouter dans le body.** Ensuite on va appeler la méthode **appendChild**et on va lui passer notre **premier élément en paramètre.**

- Comme cela => **document.body.appendChild(square);**
````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");

   const square = document.createElement(tagName"section");
   
   square.classList.add("square"); //j'ajoute la class square
    
    square.innerHTML = <strong>ok</strong>";         //ajouter du texte en html
   
    document.body.appendChild(square);  // ajout de l'élément dans notre page body
        
   console.log(square);
});
````
> document.body.appendChild(square); => ICI cela va ajouter notre carré (enfant) dans le body.

![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2012.17.15.png)

> **On constate qu'on n'a bien notre nouveaux carré qui est dans une section, de couleur bleu et qui a les dimension d'un carré.**

````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");

   const square = document.createElement(tagName"section");
   
   square.classList.add("square"); //j'ajoute la class square
    
    document.body.appendChild(square);  // ajout de l'élément dans notre page body
        
   console.log(square);
});
````
- Si je retire **innerHTML**, le carré ne sera plus **décallé mais alligné avec le reste des carrés.**

![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2012.22.45.png)

- **Je peux créer un autre élément que je vais appeler container** =>   let container = **document.createElement(tagName:"div");**.
````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");

   const square = document.createElement(tagName"section");
   
   square.classList.add("square"); //j'ajoute la class square
    
    document.body.appendChild(square);  // ajout de l'élément dans notre page body
       
    let container = document.createElement(tagName:"div");

    container.innerText = "Container";
    
    document.body.appendChild(container); // j'ajoute container dans mon body
    
   console.log(square);
});
````
- **Ce container je vais l'ajouter dans mon body**

- Je vais juste avant **ajouter du texte à Container** pour qu'on puisse l'identifier (**Savoir ou il se trouve**).

- **Je vérifie si cela fonctionne**.
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2012.38.01.png)
  
- Donc j'ai bien **mon container ajouté.**

- Je vais **ajouter à mon Containe**r en appelant la **méthode Child**.
````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");

   const square = document.createElement(tagName"section");
   
   square.classList.add("square"); //j'ajoute la class square
    
    document.body.appendChild(square);  // ajout de l'élément dans notre page body
       
    let container = document.createElement(tagName:"div");

    container.innerText = "Container";
    
    document.body.appendChild(container); // j'ajoute container dans mon body
    
    container.appendChild(square);   // j'ajoute méthode child à mon container
   
    console.log(square);
});
````
- J'ajoute un enfant à cet élément => **container.appendChild()** 

- **L'élément que je veux ajouter c'est square** qui est notre **carré**.=> **container.appendChild(square);**
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2012.44.53.png)
  
- On voit que dans la div Container il ya :

1.notre div container avec notre texte.
2.on n'a également ajouté avec une section la class square.

> **On peut ajouter des enfants à d'autres que body**
> 
> Lien de la vidéo =>[https://www.youtube.com/watch?v=BQjKZZ2QtpQ&feature=youtu.be]
----
# Écouter les événements du Dom

> On va ajouter un bouton, à chaque fois qu'on cliquera sur ce bouton cela va nous créer un carré.

- D'abord on va **créer une fonction que l'on va nommer createSquare()**.

- Cette fonction va **créer un élément.** Qu'on appellera div.

- On va **ajouter la class square.**

- On va **retourner cet élément de la fonction**.
````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");
});

function creatSquare() {
    const EL = Document.creatElement(tagName:"div");
    el.classList.add("square");
    return el;
    
}
````
> À chaque fois qu'on va appeler **la fonction creatSquare** elle va nous créer un **carré.**

- J'aimerais **écouter le click sur un bouton**.

- La première chose à faire c'est de **rajouter le bouton**.

- Je donne un **id** à ce boutton =>**creatSquare**, pour pouvoir le sélectionner et lui **ajouter une écoute d'événements.**
````html
<body>
<h1>Javascript</h1>

<button id="creatSquare">Ajouter un carré</button>   //on ajoute le bouton ici

<div class="square"></div>
<div class="square bigSquare"></div>
<div class="square"></div>
<div class="square bigSquare"></div>

<section id="sqr" class="square"></section> // première section
<section class="square"></section>
<section class="square"></section>
<section class="square"></section>



<script src="index.js"></script>>

</body>
````
- Ensuite je vais venir **sélectionner ce bouton**.Et je donne le**Id** de **l'élement que je veux récupérer.**
````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");
    
    const btnEl = document.getElementById(elementId:"creatSquare");   //on sélectionne le bouton
    console.log(btnEl);     // j'affiche l'élément dans la console pour m'assurer de bien le récupérer.
});

function creatSquare() {
    const EL = Document.creatElement(tagName:"div");
    el.classList.add("square");
    return el;
}
````
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2016.40.40.png)

- Ici on n'a bien notre **bouton.**

- Je peux **enrouler** tous mes éléments dans des **div**. Pour qu'il n'apparaisse pas sur la mêm ligne.
````html
<body>
<h1>Javascript</h1>

<button id="creatSquare">Ajouter un carré</button>   //on ajoute le bouton ici

<div>

<div class="square"></div>
<div class="square bigSquare"></div>
<div class="square"></div>
<div class="square bigSquare"></div>

</div>

<div>
    
<section id="sqr" class="square"></section> // première section
<section class="square"></section>
<section class="square"></section>
<section class="square"></section>

</div>

<script src="index.js"></script>>

</body>
````
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2016.45.23.png)

- Donc là j'ai mon bouton au dessus à chaque fois que je click dessus je vais écouter le click, lorque je click dessus **je veux ajouter un carré et que sa me l'ajoute dans mon Dom**.

- Pour ajouter une écoute d'événement, j'appelle mon bouton et j'utilise la **méthode aadEventListener**.

- En premier argument () je dois donner **le type de l'événement que je vais écouter**, dans mon cas c'est click (je veux écouter le click).

- En deuxième argument sa sera **la fonction callback**. La fonction qui sera **exécuté lorsque l'événement click sera actif**.
````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");
    
    const btnEl = document.getElementById(elementId:"creatSquare");   //on sélectionne le bouton
    console.log(btnEl);     // j'affiche l'élément dans la console pour m'assurer de bien le récupérer.

    btnEl.addEventListener("click", listener:function() {
         // À chaque fois que je click m'a fonction sera exécuté et affichera ma fonction click
        console.log("click");
    });

});

function creatSquare() {
    const EL = Document.creatElement(tagName:"div");
    el.classList.add("square");
    return el;
}
````
- On écoute L'événement **DOMContentLoaded** Dans notre **window** (fenêtre), Donc on n'a ajouté une **écoute d'événement**.

> On peut avoir plusieurs écoutes d'événement, on n'a une liste sur la documentation de MOZILLA ou il y'a tous les nom d'événement ainsi que leur **description** et comment il son **déclenchés**

> Lien de la liste des événements => [https://developer.mozilla.org/fr/docs/Web/Events]

- À chaque fois que **je click m'a fonction sera exécuté et affichera ma fonction click**.
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2017.09.01.png)
  
- On le voit à chaque fois que je click dessus ça **m'éxécute ma fonction click**.

- Grâce au mot **clé this** je peux avoir la référence dans mon élément
````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");
    
    const btnEl = document.getElementById(elementId:"creatSquare");   //on sélectionne le bouton
    console.log(btnEl);     // j'affiche l'élément dans la console pour m'assurer de bien le récupérer.

    btnEl.addEventListener("click", listener:function() {
         // À chaque fois que je click m'a fonction sera exécuté et affichera ma fonction click
        console.log("click", this);    //this=> référence de mon élément
    });

});

function creatSquare() {
    const EL = Document.creatElement(tagName:"div");
    el.classList.add("square");
    return el;
}
````
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2017.13.46.png)

- Grâce au mot clé **this** l'interpréteur **m'affiche mon élément**.

- Je vais pouvoir **changer le texte du bouton si je le souhaite**, EN UTILISANT **innerText**.
`````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");
    
    const btnEl = document.getElementById(elementId:"creatSquare");   //on sélectionne le bouton
    console.log(btnEl);     // j'affiche l'élément dans la console pour m'assurer de bien le récupérer.

    btnEl.addEventListener("click", listener:function() {
         // À chaque fois que je click m'a fonction sera exécuté et affichera ma fonction click
        console.log("click", this.innerText = "OK");    
    });

});

function creatSquare() {
    const EL = Document.creatElement(tagName:"div");
    el.classList.add("square");
    return el;
}
`````
- Lorsque je **click dessus on voit que le texte a changé**
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2017.18.17.png)
  
> Je peux **manipuler l'événement grâce au this**.

- Je voudrais créer un carré et l'ajouter dans ma liste sur ma console.

- Pour cela **je vais créer une constante** que je vais appeler **square** et je vais **utiliser ma focntion qui me permet de créer des carrés et qui me retourne une nouveau carré**.

- Ici j'utilise **creatSquare**

- j'affiche le carré qui a été créer dans la console.
````javascript
window.addEventListener(type:"DOMContentLoades", listener:function () {
    console.log("DOM CHARGÉ !");
    
    const btnEl = document.getElementById(elementId:"creatSquare");   //on sélectionne le bouton
    console.log(btnEl);     // j'affiche l'élément dans la console pour m'assurer de bien le récupérer.

    btnEl.addEventListener("click", listener:function() {
        console.log("click");    
        
        const square = creatSquare();    //je crée un nouveau carré
        console.log("square");
    });

});

function creatSquare() {
    const EL = Document.creatElement(tagName:"div");
    el.classList.add("square");
    return el;
}
````
- Lorsque je click dessus on le voit sa me crée bien un carré.
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2017.26.48.png)
  
- Maintenant ce **carré je vais pouvoir l'insérer dans ma div**.

- Je vais donner un **id** et venir sélectionner cet élément là pour lui ajouter un nouvel enfant qui sera **à chaque fois un nouveau carré**
````html
<body>
<h1>Javascript</h1>

<button id="creatSquare">Ajouter un carré</button>   //on ajoute le bouton ici

<div id="redSquare">

    <div class="square"></div>
    <div class="square bigSquare"></div>                  // j'ai fait en sorte que mes carrés rouge soit dans une div / dans un containeur
    <div class="square"></div>
    <div class="square bigSquare"></div>

</div>
<hr>
<div>

    <section id="sqr" class="square"></section> // première section
    <section class="square"></section>
    <section class="square"></section>                    // j'ai fait en sorte que mes sections bleu soit dans une div / dans un containeur
    <section class="square"></section>

</div>

<script src="index.js"></script>>

</body>
````
- Je vais créer une référence que je vais stocker dans une variable qui sera **redSquareContainerEl**

- Je vais venir **sélectionner cela grâce à son id**.
````javascript
 const btnEl = document.getElementById(elementId:"creatSquare");   //on sélectionne le bouton
    console.log(btnEl);     // j'affiche l'élément dans la console pour m'assurer de bien le récupérer.

    const redSquareContainerEl = document.getElementById(elementId:"redSquare");

    btnEl.addEventListener("click", listener:function() {
        console.log("click");

        redSquareContainerEl.appendChild(createSquare());  // je donne le retour de la fonction en paramètre
    
        
    });

});
````
- Je vais **ajouter.appendChild**, mon carré qui a été créer.

- Vu que cette constante =>const **square = creatSquare();** n'est pas réutiliser plus d'une fois je peux essayer de donner directement le **retour de la fonction en paramètre**.

- Je les passe directement **dans paramètres de ma fonction ma méthode appendChild**

- Donc ici à chaque fois que **je click sur le bouton ça va me rajouter un carré**.
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2021.12.43.png)
  
> Donc ça c'est une des écoutes d'événements, je peux très bien écouter d'autres événements. Je peux écouter des événements sur la fenêtre ou sur d'autre événements.

- Si je veux ajouter un événement sur la fenêtre, je vais ajouter une écoute d'événement.
````javascript
const btnEl = document.getElementById(elementId:"creatSquare");   //on sélectionne le bouton
    console.log(btnEl);     // j'affiche l'élément dans la console pour m'assurer de bien le récupérer.

    const redSquareContainerEl = document.getElementById(elementId:"redSquare");

    btnEl.addEventListener("click", listener:function() {
        console.log("click");

        redSquareContainerEl.appendChild(createSquare());  // je donne le retour de la fonction en paramètre
    });
    window.addEventListener(type:"mousemove",function(event:MouseEvent) {
        console.log(event);
});
    
});
````
- Je vais écouter l'événement du **mousemove** et la fonction callback qui sera exécuté c'est celle si.

- **Cette fonction va prendre en paramètre l'événement**, donc j'envoie des informations sur l'événement que **je vais ensuite afficher dans la console.**
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2021.22.25.png)
  
-Lorsque ma souris va bouger je vais avoir un **Mousemouve** donc je vais avoir un console.log qui va m'afficher **l'événement je vais l'afficher sur la console**.
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2021.26.14.png)

- Dans mon événement je peux avoir accès à mon événement, **comme la position de ma souris. Avec cette information je pourrais avec ça bouger un carré**.

- J'ai accès à la positon de la souris donc je peux **la donner à un élément pour qu'elle le suive.**

> Lien de la vidéo => [https://www.youtube.com/watch?v=X3HvOQx-hBQ&feature=youtu.be]
-----
# Function arrow

> **Les function arrow** qui veux dire en francais **fonction flécher**, c'est tout simplement **une fonction**. Elle a une particularité c'est qu'elle va **lier le this** du contexte dans lequel elle est **exécuté à son this**.

- On va voir comment déclarer une fonction de manière classique on utilise le momt clé **function**.

- On nomme la fonction, ici c'est une **fonction nommée**.

- On va **déclarer les arguments ainsi que des données si elle retourne des données**.

````javascript
function maFonction(nb) {
    return"ok";
    
};
````
- **Je viens de déclarer ma première fonction**. 

- Pour déclarer une fonction flécher je vais la stocker dans une **variable** ou dans une propriété **dans le cas d'in objet**.

- Ici ce sera **maFn** qui sera égale à une fonction fléchés.

- Pour déclarer une fonction flécher j'ouvre des parenthèses qui vont ne servir à **déclarer les arguments qu'elle va prendre**

- Ici ma fonction flécher elle demande **un nombre pour être exécuter**. Pour cela j'ajoute **une flèche =>** et ensuite je peux **ouvrir des accolades.**

- Je peux **appeler mes deux fonctions en donnant un nombre** et **maFn** également.
````javascript
function maFonction(nb) {
    return"ok";
    
};

let maFn = (nb) => {
    return nb;
};
console.log("maFONCTION:",maFonction(nb:1)) ;       //ces 2 fonnction vont me retourner ok
console.log("maFn:"maFn(nb:2));
````
- **Ces 2 fonctions vont me retourner ok**.

- La première fonction va me retourner **ok** 

- La deuxième peux me donner **le nombre que je lui ais donné**

- j'affiche les retours de ces **deux fonctions dans la console de droite**

- Le retour de mes deux fonctions, donc **maFn et ma fonction vont être affiché dans la console**.
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2022.02.54.png)
  
- On constate que pour **le premier ça me retourne 1 er 2 pour le dexième.**

- **Donc les deux ce sont bien une fonction**, ils ont une syntaxe et un objectif différent, on va **essayer de la conprendre maintenant**.

- Donc dans chaque fonction il ya a **un this**, on peut accéder **au this de la fonction** en l'affichant.

- On va également afficher le this de la fonction fléchés.

- On va exécuter nos fonctions ici ma fn
````javascript
function maFonction(nb) {
    console.log(this);
    return nb;
    
};

let maFn = (nb) => {
    consol.log(this);
    return nb;
};

maFonction(nb:1)) ;     
maFn(nb:2));
````
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2022.13.02.png)

> Tout d'abord le **this** c'est la référence, donc un objet. 
> 
> **This** est la référence dans lequel est exécuté la fonction.

- Ici mes **deux fonctions sont exécuté dans l'objet global**, la fenêtre.
````javascript

maFonction(nb:1)) ;     
maFn(nb:2));             // ici c'est le niveau de la fenêtre
````
- **This** va faire référence à l'objet window (La FENÊTRE).

> À retenir **le this fait référence à la fenêtre dans laquelle la fonction est exécutée, que ce soit une fonction classique ou une fonction fléchés pour l'instant.**
----
- Ici j'ai un objet user avec **une propriété name qui est égale à une string** et **une propriété notes égale à un tableau de nombre**.

- Ainsi qu'une méthode **getNotes** qui **est une fonction** qui ne prend aucun argument et qui va afficher dans la console le **this**.

- Ensuite j'exécute ma méthode **getNotes** à travers mon objet **user**.
```javascript
let user = {
    name: 'Jean',
    notes: [12, 14, 15],
    
    getNotes: function (){
        console.log(this);
    }
};

user.getNotes()
```
- On voit que le this **affiche bien les propriétés **name,notes et getNotes**
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2022.29.32.png)
  
- C'est pour cette raison que dans nos **méthodes on peut accéder au propriétés et les afficher de cette manière**.

- Je peux accéder au notes puisque le **this** fait référence à **un objet dans lequel est exécuté ma méthode(getNotes)**. Et non pas **à la fenêtre**.

````javascript
let user = {
    name: 'Jean',
    notes: [12, 14, 15],
    
    getNotes: function (){
        console.log(this);
        this.notes.forEach(function (note:number) { 
            console.log(note);
        });
    }
};

user.getNotes()
````
- Ici ça va me poser un problème, car **je peux effectivement accéder à mes notes** et **manipuler mes notes**.

- Je vais utiliser la méthode **forEach** dans laquelle je vais passer une **fonction callback en argument**.

- **Cette fonction callback** va pouvoir **récupérer chaque note**

- Ensuite **ma fonction va récupérer chaque note** et les afficher **dans ma console**.
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2022.43.19.png)
  
- ICI si j'affiche le **this** de **la fonction** dans la console
````javascript
let user = {
    name: 'Jean',
    notes: [12, 14, 15],
    
    getNotes: function (){
        console.log(this);
        this.notes.forEach(function (note:number) {
            console.log(this);
            console.log(note);
        });
    }
};

user.getNotes()
````
- La fonction n'est pas afficher dans mon objet, elle est **exécuté de manière globale**.
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2022.48.44.png)
  
- Donc le **this fait référence à la fenêtre**.

- Cela va me poser un problème car dans ma fonction si j'ai envie d'afficher **le nom de l'utilisateur** et de **dire que le nom de l'utilisateur a eu + la note**

- Si j'ai envie de faire une phrase en disant que **jean a eu 12, je ne vais pas pouvoir parceque this ici fait référence à l'objet**.
````javascript
let user = {
    name: 'Jean',
    notes: [12, 14, 15],
    
    getNotes: function (){
        console.log(this);
        this.notes.forEach(function (note:number) {
            console.log(this);
            console.log(this.name + " a eu + note);
        });
    }
};
````
- Car ma fonction ici **this.notes.forEach(function (note:number)** n'est pas **exécutée dans mon objet**. Car le contexte de cette fonction **sera le this (avant le note) de cette objet.

- Le contexte de cette fonction sera l'objet **window**.

> Si on recharge dans la console on va voir que je n'est pas accès à la propriété **name**.

- Pour remédier à ce problème, pour pouvoir **accès à l'objet parent dans une fonction callback**, **à la place de déclarer une fonction classique**, je vais **déclarer une fonction fléché**
````javascript
let user = {
    name: 'Jean',
    notes: [12, 14, 15],
    
    getNotes: function (){
        console.log(this);     // this parent
        this.notes.forEach((note:number) => {    // fonction fléché
            console.log("foreach :", this);   // this fonction
            console.log(this.name + " a eu + note);
        });
    }
};
````
> J'ai tout simplement **enlevé le mot clé function**, donc là c'est ce qu'on **appelle une fonction fléché**

- Cette fonction fléché va lier le **this** du contexte **parent**.

- Le contexte parent c'est tout simplement ce this là => **console.log(this)**.

- On n'a vu que ce this là => **console.log(this)**, c'est tous simplement **notre objet**.

> **Donc elle va lier le this parent au this de la fonction c'est pour cela qu'il y a une flèche**.

- Le **this qui est affiché dans le forEach** ne sera plus égale à **l'objet window** mais bien à l'objet dans lequel est définie ma fonction **arrow**
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2023.08.17.png)

- Ici on voit que le forEach ne sera plus égale à l'objet window mais bien à l'objet dans lequel est **définie ma fonction arraw**/

![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2023.09.16.png)
  
- On voit que **le forEach c'est bien mon objet**.

> Avec cette simple modification, maintenant dans nos fonctions callback on peut également **accéder au propriétés de l'objet**.

- Donc on voit que **le this** fait référence à mon objet **console.log(this.name + " a eu + note);** on peut accéder au nom de l'utilisateur et le manipuler également.

- Don afficher le nom de l'utilisateur **à vu sa note.**
![img.gnd](../MESIMAGES/h1/Capture%20d’écran%202021-01-25%20à%2023.17.38.png)
---
> Si on donne **la fonction callback** (fonction normal) qui st **exécuté pour chaque élément de mon tableau**, ici le **this** ne fait pas référence à **l'objet**, il fait **référence à l'objet dans laquelle est exécuté cette fonction**.
````javascript
let user = {
    name: 'Jean',
    notes: [12, 14, 15],
    
    getNotes: function () {
        console.log(this);
        //contexte parent
        this.notes.forEach((function (note:number) => {  //méthode forEach
            //contexte enfant
            console.log(this.name + " a eu + note);
        });  
    };
};

user.getNotes();
````
- Cette fonction, on voit qu'on n'a passé en argument **la méthode forEach**.

> Ça sera **la méthode forEach** qui va **s'occuper d'exécuter cette fonction**, pas mon objet **user**.

- **Je n'ai pas accès au propriétés name** dans ma console.

- Pour remédier à ça je **retire le mot clé function** et je passe en argument **une fonction flécher**, qui à la particularité de **lier le this du contexte parent** à l'interieur du **contexte enfant**.

- Donc le **this qui se trouve dans ma fonction callback est le même que le this de ma méthode forEach**.

> Rappel le **this** fait référence à **l'objet dans lequel elle est exécuté (l'objet user)**.

- C'est tous simplement un partage de this **pour pouvoir accéder à notre objet**.

> On va énormément voir cette syntaxe **this.notes.forEach((function (note:number) => {**.
----
> Il existe un autre moyen de partager ***le this** d'avoir accès au nom, c'est forEach.

- Car en réalité cette **méthode forEach** prend un deuxième argument qui sera le **contexte** , le this
````javascript
   
    getNotes: function () {
        console.log(this);
        //contexte parent
        this.notes.forEach((function (note:number) {  
            //contexte enfant
            console.log("For:", this);  //forEach qui se trouve ici ne sera plus égale à l'objet window
            console.log(this.name + " a eu + note);
        }),this;   //je pourrais passer le mot clé this
````
- Donc en passant **le this** en deuxième argument je vais tout simplement **remplacer le this** qui se trouve dans **ma fonction fléché**.

- Le forEach qui se trouve ici **console.log("For:", this);** ne sera plus égale à **l'objet window** mais bien à l'objet que j'ai donné ici **}),this;**

> Si je ne donne pas de this au deux!ème argument **il fera référence à l'objet dans lequel est exécuté cette fonction**.
> 
> Rappel **this** ne passe pas en deuxième argument pour **toutes les méthodes**.
>
> Le plus simple en réalité c'est de ne pas donner une fonction normal, mais de **donner une fonction fléché**, c'est le plus simple et sa fait le même travaille que si **on donner le this en deuxième argument**.

> LIEN DE LA VIDÉO =>[https://www.youtube.com/watch?v=sfGKjJUf6KA&feature=youtu.be].