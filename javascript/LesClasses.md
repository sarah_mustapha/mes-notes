> Sommaire:
> - [Déclarer une classe](#dclarer-une-classe)
> - [Propriété pas défaut](#proprit-par-dfaut)
> - [La Fonction Constructor](#la-fonction-constructor)
> - [Ajouter des méthodes](#ajouter-des-mthodes)
> - [Héritage entre les classes](#hritage-entre-les-classes)
> - [Propriétés et méthode static](#proprits-et-mthodes-static)

----
# Déclarer une classe

> **Une classe c'est un type de fonction qui nous permettre de créer des objets**.

````javascript
class UserHasRole
````
> **Pour déclarer une classe on utilise le mot clé class**.
> **Puis on donne le nom de la classe, de préférence avec une lettre majuscule.**
> **La lettre de chaque nouveau mot sera également en majuscules**.
````javascript
class User {}
````
- J'écris une classe User ensuite avoir nommé la classe on ouvre des accolades, et la j'ai déclaré une classe qui est nommé user.

- Je peux créer une nouvelle instance de cet objet, donc un nouvel objet.

- **C'est simplement un objet qui a été créé à partir de la classe User**.

- Pour cela je crée une instance que je vais stocker dans une variable nommé User.

````javascript
class User {
    
}

let user = new
````
- **Pour créer une nouvelle instance**, un nouvel objet **j'utilise le mot clé new**.

- **Le mot clé new va créer un nouvel objet** en lui **assignant le this**.

- Donc j'utilise **le mot clé new** **et je nomme la classe que je veux créer**, **donc l'instance**.

- Donc la classe que je veux créer sera **la classe User**, ensuite **j'ouvre les parenthèses**.

````javascript
class User {
    
}

let user = new User();
````
- Donc la **je viens de créer une nouvelle instance de la classe User**.

- **Je peux ensuite afficher dans la console l'instance créer qui a été sauvegardée dasn la variable User**.

````javascript
class User {
    
}

let user = new User(); // une instance
console.log(user);
````
- On voit bien dans la console **qu'on n'a un nouvel objet qui a été créer à partir de la classe User.**

- **Donc dans la console on n'a le nom de la classe qui a servi pour créer cet objet là.**

`````javascript
// Modèles d'objet
class User {
    
}

let user = new User(); // une instance
let user2 = new user();
console.log(user, user2);

`````
- Je vais créer **une autre instance à partir de cette classe User**.

- Ensuite **je vais afficher cette nouvelle instance également dans la console**.

- Donc là **j'ai deux instances de la classe user**, ce sont **deux objets différents mais créer à partir du même modèle à partir de la même classe**.

- **On peut considérer que les classes ce sont des modèles d'objets.**

- **Grâce à ses modèles là on peut créer des objets**, qui auront **les mêmes propriétés et les même méthodes**.

`````javascript
class User {
    
}

let user = new User(); // une instance
let user2 = new user();

user2.name = "Sarah";  // user2 est un objet
console.log(user, user2);
`````
- Donc ici **user2 c'est un objet**, donc **je peux accéder à des propriétés, ou enregistré des propriétés, pareille pour les méthodes**.

- Maintenant ma **deuxième instance de la classe user** à la **propriété name, alors que la première ne l'a pas**.

> LIEN DE LA VIDÉO => [https://www.youtube.com/watch?v=9_NWpR6xg4I&feature=youtu.be]

-----
# PROPRIÉTÉ PAR DÉFAUT

> On va voir comment ajouter des propriétés avec des valeurs par défaut à nos classes.

````javascript
// Modèle
class User{
    name = "sarah";
    age = 22;
    presence; 
}
````
- Je vais **déclarer une class USER**

- Pour **déclarer une classe j'utilise le mot clé class**.

- Ensuite je nomme la classe ici **c'est une classe User**, puis j'ouvre des accolades.

- **Entre les accolades** je vais pouvoir ici **déclarer des propriétés avec des valeurs par défaut**.

- La première propriété sera name.

- **Je vais lui assigner** donc j'utilise une égale pour lui donner **une nouvelle valeur qui sera sarah**.

- Je vais lui **assigner une autre propriété** qui sera **age et j'assigne la valeur 22**.

- Je peux également **lui assigner une autre propriété qui sera présence** et qui elle **n'aura aucune valeur**.

> **La valeur de presence sera indefined pour indéfinit**.
> 
> **/ Rappel\ Les classes ce sont des modèles d'objet, on utilise les classes pour créer de nouveaux objets.**
>
> Ici comme c'est un Modèle tout les nouveaux objets créer à partir de ce model aurons ses propriétés la => (name, age, precence,etc..) et leurs valeurs enregistrées.
>
> Donc ici ce sont des valeurs par défaut donc chaque nouvelle objet auront ces valeurs là =>(sarah, 22, etc..) et leurs propriétées.

- On va faire le test en déclarant une nouvelle instance **dans la variable user**.

````javascript
// Modèle
class User{
    name = "sarah";
    age = 22;
    presence; 
}

let user = new User()  //nouvel objet
````
- Pour créer une nouvelle instance on utilise **le mot clé new**, ensuite je **nomme la classe** avec laquelle je veux **créer la nouvelle instance**.

- La nouvelle instance c'est le nouvel objet.

- Donc ce nouvel objet je vais le **stocker dans la variable user**.

- **Ensuite j'affiche ma nouvelle instance dans la console**.
````javascript
// Modèle
class User{
    name = "sarah";
    age = 22;
    presence; 
}

let user = new User()  //nouvel objet
console.log(user);
````
> Dans ma console je vois que j'ai un objet qui a été instanciés à la place de la classe user.
>
> Et on voit que ce nouvel objet à des propriétés par défaut donc (age, name, presence qui est indéfinit ensuite leurs valeurs.)
> 
> Donc chaque propriété aura toujours un model et ses valeurs.

````javascript
// Modèle
class User{
    name = "sarah";
    age = 22;
    presence; 
}

let user = new User();  //nouvel objet
let user2 = new User();  // objet
console.log(user);
````


- Ici je vais créer une nouvelle instance que je vais stocker dans la variable **user2**.

- On peut voir dans la console que les deux instances (user1 et user2) on les mêmes propriétés et les mêmes valeurs.

- On rappelle que une instance c'est un objet et ce que je stock dans ma variable ce sont des objets.

> Donc ici je peux accéder au propriétés de mes objets / donc de mes instances de la même manière qu'un objet

````javascript
// Modèle
class User{
    name = "sarah";
    age = 22;
    presence; 
}

let user = new User();  //nouvel objet
let user2 = new User();  // objet

user.name = "sam";

console.log(user);
````
- Je peux accéder au propriété de mes instances de la même manière qu'un objet avec le point.

- Donc je peux afficher ses propriétés, je peux également modifier ses propriétés là.

- Je peux dire que user, le premier utilisateur que son nom c'est sam.

- Ensuite je vais afficher le nom de l'utilisateur ainsi que le nom du second utilisateur **donc sam et sarah**, puisque j'ai modifié le nom du premier.

> LIEN DE LA VIDÉO => [https://www.youtube.com/watch?v=x7j_0Tx9eGc&feature=youtu.be]

-----

# La fonction constructor()

````javascript
// Modèle
class User {     
name = "Sarah";//   propriéte et valeurs
age = 22;
presence;
}

let user = new User();                         // Tom 27 ans 
let user2 = new User(); // instance   // objet  // Sam 34 ans

console.log(user);
console.log(user2);
````
> Ici j'ai une classe uUser qui à des propriétés par défaut ainsi que des valeurs pas défaut.
> 
> Donc toutes les nouvelles instances de cette classe auront ces propriétés là ainsi que ces valeurs là.

- Dans la console on affiche la première instance ainsi que la seconde.

- On voit bien que nos deux objets des deux instance de la classe User on les mêmes propriétés ainsi que les mêmes valeurs.

- Ce que j'aimerais faire c'est de créer un nouvel utilisateur avec un prénom personnalisés .

- J'aimerais que mon premier utilisateur créer s'appelle Tom et qu'il a 27 ans.

- J'aimerais que le deuxième utilisateur créer s'appelle sam et qu'il à 34 ans.

- Pour passer des paramètres lors de l'instanciation d'un nouvel objet, ( lors de la création du nouvel objet), je peux passer entre parenthèse ici => let user2 = new User();

- let user2 = new User(); Les parenthèses ici vont appeler la fonction la méthode constructeurs.

- Dans une classe je ne suis pas obligé de la déclaré mais si j'en n'ai besoin je peux l'insérer.

- Donc je peux insérer la méthode constructor.
````javascript
// Modèle
class User {     
name = "Sarah";//   propriéte et valeurs
age = 22;
presence;

constructor(){
    
}
}

let user = new User();                         // Tom 27 ans 
let user2 = new User(); // instance   // objet  // Sam 34 ans

console.log(user);
console.log(user2);
````
- **Cette méthode constructor est réservé par la méthode de javascript et elle sert à créer le nouvel objet**.

- Donc ici les parenthèses let user = new User(); vont invoquer cette fonction là.

- let user = new User(); <== Ici je vais pouvoir personnaliser mon utilisateur en passent des paramètres entre parenthèse ici.

- **Dans la méthode constructor**( constructor())<== je vais pouvoir **récupérer ces parenthèses et modifier mon utilisateur, le créer**.

> Comme le nom l'indique la méthode constructor me sert à créer, configurer mes nouveaux objets donc je doit déterminer quelle sont les élément qui sont nécessaires pour la création de mon objet.

````javascript
// Modèle
class User {      //user => utilisateur
name = "Sarah";//   propriéte et valeurs
age = 22;
presence;

constructor(name, age, presence){   // pour créer un utilisateur
    
}
}

let user = new User();                         // Tom 27 ans 
let user2 = new User(); // instance   // objet  // Sam 34 ans

console.log(user);
console.log(user2);
````
- Ici mon objet sera un utilisateur, c'est une classe user, donc tous les objets qui vont être créé à partir de cette classe là ce sont **des utilisateurs**.

- **Pour créer un utilisateur j'ai besoin du nom ainsi que de l'age et potentiellement de sa présence ou non**.

````javascript
constructor(name, age, presence){   // pour créer un utilisateur
    
}
````
> **Donc ici je determine tous les arguments qui sont nécessaires à la création d'un utilisateur, pour pourvoir personnaliser à ses paramètres là => (name, age, presence), je vais déclarer dans la fonction constructor et la présence n'est pas obligatoire**.

- Donc si j'ai un paramètre qui n'est pas obliger je peux ajouter un égal et lui donnée une valeur par défaut.

````javascript
constructor(name, age, presence = false){   // pour créer un utilisateur
````
- **Ici la présence sa valeur par défaut sera false**.
  
- Par défaut **l'utilisateur créer n'est pas présent**.
  
- Je pourrais aussi **récupérer le nom, l'age et la présence**.

> Donc pour mon utilisateur numéro 1 je veux qu'il s'appelle tom et qu'il est 27 ans.

`````javascript
// Modèle
class User {      //user => utilisateur
name = "Sarah";//   propriéte et valeurs
age = 22;
presence;

constructor(name, age, presence = false) {   
    
    }
}

let user = new User(name:"Tom", age:27, presence:true);        //()<= ici je luis indique son nom      // Tom 27 ans 
let user2 = new User(name:"sam", age:34); // instance   // objet  // Sam 34 ans

console.log(user);
console.log(user2);
`````
- **En premier argument on le voit j'ai besoin du nom donc pas de l'age**.

- **Et le deuxième argument je lui indique son âge**.

- **La valeur présence n'est pas obligatoire, donc si je ne la met pas la valeur de présence ici ce sera false**.

- Si j'insère un paramètre et que j'indique que la présence est vraie, **ca va surpasser cette valeur par défaut**, **donc présence sera égale à true**.

- Pour mon deuxième utilisateur je souhaite qui s'appelle Sam donc **je ne peux pas indiquer 34**.

- Car en première argument on n'attend le nom pas l'age et l'age c'est le deuxième argument.

- Donc **dans le deuxième utilisateur j'indique son prénom et ensuite son âge**.

> **Je ne vais pas insérer la présence pour qu'on puisse voir comment l'argument par défaut fonctionne**
>
> **On va voir que le deuxième utilisateur ne sera pas présent par défaut**.

- Ensuite j'affiche mes deux instances dans la console.

````javascript
// Modèle
class User {      //user => utilisateur
    name = "Sarah";//   propriéte et valeurs
    age = 22;
    presence;
    
    constructor(name, age, presence = false) {   //objet
    this                 
    }
}

let user = new User(name:"Tom", age:27, presence:true);        //()<= ici je luis indique son nom      // Tom 27 ans 
let user2 = new User(name:"sam", age:34); // instance   // objet  // Sam 34 ans

console.log(user);
console.log(user2);
````
> **Il n'a aucun changement car on n'a juste déclaré la méthode constructor.**
>
> Pour vraiment personnaliser l'utilisateur l'objet créé, je vais devoir y faire référence grâce au mot clé **this**.

- **le mot clé this fait référence à l'objet.** L'objet qui est construit.

- Lorsque je crée une nouvelle instance de l'utilisateur, donc lorsque que je crée un nouvel objet **this** fait **référence à cet objet là**.

- Je vais pouvoir **ajouter à la propriété name une nouvelle valeur**.

````javascript
// Modèle
class User {      //user => utilisateur
    name = "Sarah";//   propriéte et valeurs
    age = 22;
    presence;
    
    constructor(name, age, presence = false) {   //objet
    this .name = name;      
    this.age = age;
    this.presence = presence;
    }
}

let user = new User(name:"Tom", age:27, presence:true);        //()<= ici je luis indique son nom      // Tom 27 ans 
let user2 = new User(name:"sam", age:34); // instance   // objet  // Sam 34 ans

console.log(user);
console.log(user2);
````
- La valeur de name de mon objet **sera le nom que j'ai passé lors de l'instanciation de ce nouvel objet**.

- La propriété age sera la valeur que j'ai donnée en deuxième argument **lorsque j'ai instanciés mon objet**.

- La propriété présence sera **la présence que j'ai indiquée ou non lors de l'instanciation de mon nouvel objet**

````javascript
// objet que j'ai stocker dans user
let user = new User(name:"Tom", age:27, presence:true);        //()<= ici je luis indique son nom      // Tom 27 ans 
let user2 = new User(name:"sam", age:34); // instance   // objet  // Sam 34 ans
````
> **Donc pour cet objet que j'ai stocké dans user sa présence sera true**.

> **Pour mon l'objet stocké dans user2 sa presence donc la valeur par défaut c'est false**.

````javascript
constructor(name, age, presence = false) {   //objet
    this .name = name;      
    this.age = age;                           //propriétés dinamique
    this.presence = presence;
    }
````
> Donc là j'ai des propriétés qui sont dynamique, j'ai des propriétés qui changent en fonction des paramètres que je donne lors de la construction de l'objet.

````javascript
// Modèle
class User {      //user => utilisateur
    name = "Sarah";//   propriéte et valeurs
    age = 22;
    presence;
````
> Ici j'ai des valeurs par défaut, donc des propriétés par défaut.

````javascript
constructor(name, age, presence = false) {   //objet
    this .name = name;                          // ils auront des valeurs
    this.age = age;                           //propriétés dinamique
    this.presence = presence;
    }
````
> **Je peux très bien garder la déclaration de mes propriétés mais je ne leurs donne aucune valeur puisque par défaut ils auront ici des valeurs**.

> Ce que j'aurais pu faire également pour la présence c'est de donnée une valeur par défaut
````javascript
/ Modèle
class User {      //user => utilisateur
    name = "Sarah";//   propriéte et valeurs
    age = 22;
    presence = false;    //<== valeur par défaut
````
> **On peut aussi combiner les deux avoir des propriétés par défaut et des valeurs par défaut**, avoir des propriétés qui sont dynamique qui vont être modifiés.

````javascript
/ Modèle
class User {      //user => utilisateur
    name = "Sarah";//   propriéte et valeurs
    age = 22;
    presence;
    adresse = "";
    pays = "france";
````
- **On peut avoir une autre propriété ui se nomme adresse ou pays**

- **Le pays par défaut sera France**.

> **Lors de l'instanciation je donne pa&s le Pays donc tous les utilisateurs créer viendront du pays France.**

- **On peut voir dans la console que nos deux utilisateurs on des valeurs différentes**

````javascript
// Modèle
class User {      //user => utilisateur
    name = "Sarah";//   propriéte et valeurs
    age = 22;
    presence;
    
    constructor(name, age, presence = false) {   //objet
    this .name = name;      
    this.age = age;
    this.presence = presence;
    }
}

let user = new User(name:"Tom", age:27, presence:true);        //()<= ici je luis indique son nom      // Tom 27 ans 
let user2 = new User(name:"sam", age:34); // instance   // objet  // Sam 34 ans

console.log(user);
console.log(user2);
````
- **On voit dans la console que la présence de l'utilisateur 2 que je ne donne pas lors de la construction, la présence prend la valeur par défaut qui est false**.

> **Même si on retire la valeur false de la propriété presence sa valeur par défaut sera toujours false car dans l'objet elle est false**.
> 
> **Nous ne pouvons pas utiliser la méthode constructor autrement.**
> 
> **Constructor est un nom de méthode ui est réservé par la syntax de JAVASCRIPT.**

> LIEN DE LA VIDÉO => [https://www.youtube.com/watch?v=7aJOq2NvFvA&feature=youtu.be]

-----
# Ajouter des méthodes

> Ajouter des méthodes c'est le même principe que pour des objets.
````javascript
           // nouvelle instance d'une class,On crée en réalité un objet
let user = new User(name:"Tom", age:27, presence:true);        //On crée en réalité un objet
let user2 = new User(name:"sam", age:34); // instance   // objet  // Sam 34 ans

````
> **Je r'appel qu'ici on crée une nouvelle instance d'une classe on crée en réalité un objet.**
>
> **Donc les fonctions qui sont associés à cet objet c'est ce qu'on appelle des méthodes**.
````javascript
 // Modèle
class User {      //user => utilisateur
    name;//   propriéte et valeurs
    age;
    presence;
    pays = "France";

    constructor(name, age, presence = false) {   //objet
        this.name = name;
        this.age = age;
        this.presence = presence;
    }

    isPresent() {// <== On déclare dans les parenthèses les paramètre dont m'a méthode a besoin.

    }
}
let user = new User(name:"Tom", age:27, presence:true);        //()<= ici je luis indique son nom      // Tom 27 ans 
let user2 = new User(name:"sam", age:34); // instance   // objet  // Sam 34 ans

console.log(user);
console.log(user2);
````

> Je déclare **une nouvelle méthode => isPresent**, pour déterminer si l'utilisateur est présent.

- **Je donne ma méthode isPresent ensuite j'ouvre des parenthèses**.

- Si ma méthode à besoin de paramètre **je vais pouvoir déclarer des paramètres dont elle a besoin**.

- **Dans mon cas ici pour déterminer si l'utilisateur est présent je n'ai pas besoin de paramètre**

- Ce que j'ai besoin de faire c'est **d'accéder à la propriété presence de l'utilisateur**.

> **Pour accéder à la propriété presence j'utilise le mot clé this**.
`````javascript
````javascript
 // Modèle
class User {      //user => utilisateur
    name;//   propriéte et valeurs
    age;
    presence;
    pays = "France";

    constructor(name, age, presence = false) {   //objet
        this.name = name;
        this.age = age;
        this.presence = presence;
    }

    isPresent() {// <== On déclare dans les parenthèses les paramètre dont m'a méthode a besoin.
  return this.presence;   //<== this fait référence à l'objet courant
    }
}
let user = new User(name:"Tom", age:27, presence:true);        //()<= ici je luis indique son nom      // Tom 27 ans 
let user2 = new User(name:"sam", age:34); // instance   // objet  // Sam 34 ans

console.log(user);
console.log(user2);
`````
- **This fait référence à l'instance à l'objet courant**.

> DANS MES DEUX VARIABLES USER 1 ET 2 THIS FERA RÉFÉRENCE À MES DEUX OBJET USER.

- **Je veux accéder à la propriété presence de l'objet courant que je veux retourner**.

- Quand **j'appellerais la méthode this** ca me **retournera l'état présence de mon utilisateur**, et je n'ai besoin d'aucun autre paramètre.

- Donc je vais **appeler directement cette méthode**, **pour y accéder c'est de la même manière que pour les objets**.
````javascript

constructor(name, age, presence = false) {   //objet
    this.name = name;
    this.age = age;
    this.presence = presence;
}
    isPresent() {// <== On déclare dans les parenthèses les paramètre dont m'a méthode a besoin.
    return this.presence;   //<== this fait référence à l'objet courant
    }        //Ma méthode isPresent va retourner la propriété present
    
}
let user = new User(name:"Tom", age:27, presence:true);        //()<= ici je luis indique son nom      // Tom 27 ans 
let user2 = new User(name:"sam", age:34); // instance   // objet  // Sam 34 ans

user.isPresent();//<== j'éxécute cette fonction en ouvrant des parenthèses
user2.isPresent();// va retourner la propriété present de user

console.log(user, user.isPresent());
console.log(user2, user2.isPresent());
````
- **J'exécute cette fonction en ouvrant des parenthèses et en y passant les paramètres s'il y en n'a besoin => user.isPresent();**

- Si ma méthode a besoin d'un nom ici => isPresent(), **je vais y passer un nom ici=> user.isPresent()**, dans mon cas j'en n'est pas besoin.

- **Ma méthode isPresent va retourner la propriété present de l'utilisateur (user)**

> **Pour user2 je vais appeler la méthode isPresent et cala va me retourner la présence de l'utilisateur user2*, **grâce au this**

- On veut retourner la méthode de l'utilisateur courant qui est user sur lequel on appelle la méthode .isPresent.

- On affiche ensuite **dans la console les valeurs qui est retourné dans la méthode donc user.isPresent et user2.isPresent pour voir s'il est présent.**

> **Juste après l'affichage de l'objet user dans la console on voit si l'utilisateur est present avec le boolean**.
> 
> **On voit dans la console que ca nous affiche true dans le premier utilisateur user car la propriété presence est true.**
> 
> **On voit dans la console que ca nous affiche false dans le deuxième utilisateur user2 car la propriété presence est false.**
````javascript

constructor(name, age, presence = false) {   //objet
    this.name = name;
    this.age = age;
    this.presence = presence;
}

isPresent() {
    return this.presence;
    
}

getFullName() {
    
}

say(str) {
    console.log(this.name + ' : ' + str);
}

}
let user = new User(name:"Tom", age:27, presence:true);        //()<= ici je luis indique son nom      // Tom 27 ans 
let user2 = new User(name:"sam", age:34); // instance   // objet  // Sam 34 ans

````
> **Grâce au isPresent j'ai déclaré une nouvelle méthode**, je peux avoir **d'autre méthode**s comme par exemple la méthode **getFullName**, ou**say** comme on l'a fait pour les objets.

> Donc j'aurais une méthode, **la méthode say**, **une chaine de caractère qui va dire à l'utilisateur**.
>
> **Ensuite j'affiche dans la console le nom de l'utilisateur **this.name** que je vais concaténer avec la chaine de caractère les deux points => : , ainsi que la chaine de caractère données en arguments**.
> 
> Cela va **m'afficher dans la console => Tom : et la chaine de caractère que je lui aurais fait dire**.
````javascript
let user = new User(name:"Tom", age:27, presence:true);        //()<= ici je luis indique son nom      // Tom 27 ans 
let user2 = new User(name:"sam", age:34); // instance   // objet  // Sam 34 ans

console.log(user);
console.log(user2);

user.say(str:"Bonjour" + user2.name);
user2.say(str:"Bonjour" + user.name);
````
- **J'AFFICHE DANS LA CONSOLE CE QUE JE VAIS LUI DIRE**.

- Pour l'utilisateur numéro 1 je vais lui faire dire **Bonjour concaténé avec le nom de l'utilisateur 2 => Sam**.

- Le deuxième utilisateur v'a également parler je vais lui faire dire **Bonjour concaténé avec le nom de l'utilisateur 1 => Jean**.

> **Je ne peux pas déclarer deux méthodes qui on les mêmes prénoms**, **nommée de la même manière ou nommée par constructor**.
> 
> **La méthode constructor est réservé au niveaux des classes par la syntax de Javascript**.
>
> LIEN DE LA VIDÉO ==> [https://www.youtube.com/watch?v=9RiPoY_9sPo&feature=youtu.be]

-----

# HÉRITAGE ENTRE LES CLASSES

> **On n'a vu comment on pouvait déclarer une classe, comment on pouvait créer une instance à partir de cette classe là et dans cette vidéo on va voir comment avoir un système d'héritage.**
````javascript
//Modèle
class Vehicule {
        constructor(name, wheels) {
            this.name = name;
            this.wheels = wheels;
        }
        
        accelerate()   // faccélérer
        
        brake()       // freiner
        
        turn()        // tourner
..}

class Car {}
                           // 2 véhicules (sous véhicule)
class MotorBike {}
````
- Ici j'ai une première class véhicule qui possède ainsi des méthodes, il ya la propriété .name et également la propriété .wheels <== qui est le nombre de roues qui l'a.**

- **Pour caractériser un véhicule je vais avoir certaines propriétés ,certaine information et certaines méthodes**.

- **Un véhicule est capable d'accélérer, freiner et également tourné.** Cela est valables pour tout les véhicule en tout cas dans cette exemple là.

- **Je vais avoir des sous véhicules, qui serons la voiture et une moto dans cet exemple**.

> CES DEUX VÉHICULES DOIVENT AVOIR LES MÉTHODES QUI SONT DÉCLARÉ DANS LA CLASSE VÉHICULES AINSI QUE DES PROPRIÉTÉS.

> Afin d'éviter de copier cette logique et de la coller dans la classe car on peut dire que la classe car hérite des propriétés des méthodes de la classe véhicule.

````javascript
//Modèle
class Vehicle {
        constructor(name, wheels) {
            this.name = name;
            this.wheels = wheels;
        }
        
        accelerate()   // faccélérer
        
        brake()       // freiner
        
        turn()        // tourner
..}

class Car extends vehicle{   // On dit que la class car étant vehicle
                           // 2 véhicules
    constructor(name, wheels) {
        super(name, wheels);    // mot clé super
    }
}
    class MotorBike extends vehicle {}
````
- **On rajoute le mot clé extends et indiquer de quelle classe il va étendre**.

- **Toutes les nouvelles voitures (instance) de la class car, vont être créé à partir de la class car, vont égalemetn hétité des propriétés et des méthodes qui sont déclarés dans la class vehicle**.

- On peut dire que **la class MotorBike va étendre la class vehicle, tous les objets motos qui sont créé à partir de la class MotoBike vont également avoir accès aux propriétés et au méthodes de Vehicle**.

- **! Ensuite on appelle absolument le constructeur parent de la classe véhicules**

- **La class Car est la class enfants**.

> **Lorsque j'instancie une nouvelle voiture il faut que je récupère le nom et le nombre de roues et que j'appel le nom du constructeur de la class parent**.
> 
>  Pour appeler les constructeurs de la classe parent j'utilise le **mot clé super**.
>
> **Entre parenthèse je lui donne les arguments que le constructeur a besoin.**

- **Le super va appeler la méthode constructor**

- Je peux demander à la class Car un nombre de roues par défaut.

````javascript
//Modèle
class Vehicle {
        constructor(name, wheels) {
            this.name = name;
            this.wheels = wheels;
        }
        
        accelerate()   // faccélérer
        
        brake()       // freiner
        
        turn()        // tourner
..}

class Car extends vehicle{   // On dit que la class car étant vehicle
                           // 2 véhicules
    constructor(name) {
        super(name, wheels:4);
    }
}
    class MotorBike extends vehicle {
    
        constructor(model) {
            super(model, wheels:2);
        }
    }
````
- **Je donne un nombre de 4 roues par défaut au constructor parent**

- **On fait la même chose pour MotorBike car lorsqu'on étant une class il faut appeler le constructor de la classe parent**.

- On peut également donner le nom et le model **car dans la class Vehicle on n'a besoin du nom du premier argument ainsi que du nombre de roues**

- On va dire que toutes les motos on 2 roues.

- **Ensuite je vais créer une nouvelle instance de voiture pour ses deux classes**.

````javascript
let car = new Car(name:"AUDI");     //instance
````
- **Entre parenthèses je vais lui passer seulement le nom qui sera => AUDI**.

- **Je ne lui passe pas le nombre de roues car dans la classe Car je demande que le nom dans le constructor**.

- **Je vais créer une instance d'une nouvelle moto et lui passer le model => Mitsu**(Une moto peut très bien avoir d'autre propriétés) exemple=> hasPassenger.
````javascript
class MotorBike extends vehicle {
    
constructor(model, hasPassenger:boolean = false) {
    super(model, wheels:2);
    this.hasPassenger = hasPassenger;
}
}
let car = new Car(name:"AUDI");                      // L'instance d'une voiture
let moto = new MotorBike("Mitsu", hasPassenger:true); // L'instance d'une moto`
````
- Par défaut cela sera false, ensuite on peut enregistrer cette propriété pour toutes les motos, ensuite **les motos auront la propriété hasPassenger**qui sera la propriété qui sera donnée en **deuxième argument**.

> **Lorsqu'on rajoute une deuxième propriété il faut absolument la mettre après le constructor parent**.

- Je vais indiquer que **la moto que j'ai créé a un autre passager**.

> Comme d'habitude j'affiche dans ma console mes deux instances (voiture et moto)
`````javascript
class MotorBike extends vehicle {
    
constructor(model, hasPassenger:boolean = false) {
    super(model, wheels:2);
    this.hasPassenger = hasPassenger;
}
}
let car = new Car(name:"AUDI");                      // L'instance d'une voiture
let moto = new MotorBike("Mitsu", hasPassenger:true); // L'instance d'une moto`

console.log(car);
console.log(moto);

car.accelerate();           // méthodes que j'ai hérité de la class vehicle
moto.accelerate();          // méthodes que j'ai hérité de la class moto
car.brake();
moto.turn();
`````
> **DANS LE PROTOTYPE DE MA CONSOLE ON N'A NOS MÉTHODES***.

- Je peux demander a ma voiture et ma moto d'accélérer

> Ce sont des méthodes que j'ai hérité des classes vehicle et moto, je n'ai pas défini ces méthodes.

- **Je peux demander à ma voiture de freiner**.

- **Je peux demander à ma moto de tourner**.

> **Je n'ai pas défini ces méthodes là mais pourtant j'y ai accès à l'intérieur de mes instances de voitures.**

- Je peux connaitre la dimension du coffre et stocker les informations dans l'instance.
````javascript
class MotorBike extends vehicle {

    constructor(model, coffre) {
        super(model, wheels:2);
        this.coffre = coffre;
    }
}
}
let car = new Car(name:"AUDI", coffre: 500);                      // L'instance d'une voiture
let moto = new MotorBike("Mitsu", hasPassenger:true); // L'instance d'une moto`

console.log(car);
console.log(moto);
````
> **Lorsque je crée une nouvelle instance de voiture il faut que je lui donne les dimensions du coffre(Par exemlpe:500 pour 500 litres)**.
>
> **Sa reste des classes juste que ses CLASSES hérite d'autres propriétés méthode**
>
> Puisque ce sont des classes sa reste un type de fonction qui nous permet de créer des objets.

- Si je veux créer une nouvelle voiture => **Je crée une Mercedes avec des dimensions de coffre qui sont différentes que je vais stocker dans la variable car2**
`````javascript
}
let car = new Car(name:"AUDI", coffre: 500);// L'instance d'une voiture
ler car1 = new Car(name:"Mercedes", coffre:200);
let moto = new MotorBike(model:"Mitsu", hasPassenger:true); // L'instance d'une moto`

console.log(car);
console.log(car2);
console.log(moto);
`````
- Je vais avoir 2 propiétés différentes qui on **les memes propriétés, avec des valeurs différentes**.
> **Les deux objets hérite de propriétés des méthodes d'un véhicule**.

- **Je peux créer une instance d'un véhicule directement**.
````javascript
console.log(car);
console.log(car2);
console.log(moto);

car.accelerate();           // méthodes que j'ai hérité de la class vehicle
moto.accelerate();          // méthodes que j'ai hérité de la class moto
car.brake();
moto.turn();

let bike = new Vehicle(name"Vélo", wheels:2);
````
>-> Je peux créer un nouveau véhicule en lui donnant le nom d'un vélo, un nombre de roues

- **Je vais stocker dans bike** et afficher mon vélo dans la console.

- On voit dans la console le nom et le nombre de roues, j'ai **vélo avec 2 roues**.

> Javascript est un language de prototypage qui va partager des propriétés et des méthodes dans le prototype, dans chaque objet il va partager des informations.
> 
> Lorsque l'on crée un nouveau tableau par exemple le tableau hérite de toutes les méthodes et des propriétés de l'objet global, toutes ses informations sont accessibles comme un objet.
>
> / Rappel \ **Un objet hérite des propriétés d'un objet.**
> 
> **Les classes c'est un système qui cache les partages de propriété ou de méthode dans le prototype cela rend la chose plus simple.**
> 
> LIEN DE LA VIDÉO ==> [https://www.youtube.com/watch?v=1b8YR1auFcw&feature=youtu.be]
-------
# Propriétés et méthodes static

> Les propriétés et les méthodes statiques ce sont des propriétés et des méthodes qui ne sont pas accésibles au niveau des instances.
> 
> Depuis le départ on n'a déclaré des propriétés, on n'a récupéré ses propriétés là en y accédant au niveau de l'instance et on n'a déclaré des méthodes qui sont accessibles au niveau des instances.
`````javascript
//Modèle
class Vehicle{
    
    constructor(name, wheels){
        this.name = name;
        this.wheels = wheels;
    }
    
    accelerate() {...}
                                //les propriétés déclaré
    brake() {...}
    
    turn() {...}
}

class Car extends Vehicle {
    
    constructor(name, coffre) {...}
    
    klaxon() {          //<== c'est une méthode accessible au niveau de l'instance
        console.log(this.name + "klaxonne");
    }
}

let car = new Car(name:"AUDI", coffre:500);   // instance créé

console.log(car);

car.klaxon()
`````
- **Klaxon c'est une méthode qui est accessible au niveau de l'instance**.

- Si **je crée mon instance que je stock dans la variable car** ensuite **je peux accéder à cette méthode**.

- **Les méthodes statiques ainsi que les propriétés statiques** ne sont **pas accessible au niveau des instances**.

> Je ne pourrais pas faire .test technique sur une instance, si je déclare une méthode de test technique qui est statique.
> 
> **Comme sont nom l'indique toutes les propriétés et les méthodes statiques elle sont statique à la classe, ne sont pas partagés au niveau des instances.**

- On va d'abord déclarer une propriété static pour cela on utilise **le mot clé static.**

````javascript
//Modèle
class Vehicle {
    
    static GRAVITY = 10;
    
    constructor(name, wheels) {
        this.name = name;
        this.wheels = wheels;
    }
    
    accelerate() {...}
    
    brake() {...}
    
    turn() {...}
    
    static testTechnique(vehicle) {    // méthode static
        return vehicle.wheels > 0;
    }
}
````
- **Lorsque l'on déclare des propriétés dite static ce sont des constantes**

- **Je nomme ma constante en majuscule** car en général on les nommera comme cela pour repérer les constances dès le premier coup d'oeil.

- **Gravity sera une constante qui sera appliqué à tous les véhicules**.

- On nomme **GRAVITY de manière statique sur la classe véhicule**

- On accède à **la constance gravity** **on va devoir y accéder directement via la class**

- **On déclare une méthode dite static et ensuite on nomme la méthode comme on pourrais le faire pour une méthode normale**.

- **Je vais appeler la méthode testTechnique**

- **La méthode testTechnique prend en argument un Vehicle et elle retournera un test** qui sera es-ce que le véhicule à plus de 0 =>> - La méthode testTechnique prend en argument un Vehicle et elle retournera un test qui sera es-ce que le véhicule à plus de 0 =>> **return vehicle.wheels > 0;**

> **Si le véhicule à zero euros ou moins c'est que le test Technique ne passera pas**.

- **Les méthodes qui sont dites statique** ce sont des méthodes qui sont généralement utilitaire, puisque ses méthodes ne sont pas accessibles au niveau des instances, cela sera des méthodes qui vont nous permettre de réaliser **des tâches utiles au niveau des véhicules.**

> On pourrait très bien déclarer le testTechnique au niveau de l'instance.
> 
> Imaginons que la méthode testTechnique ainsi que la propriété gravity ne seront pas accessible au niveau de linstance **car**, je ne pourrais pas afficher dans la console**car.gravity**, car elle n'est pass accessible elle est statique à la classe donc je ne peux pas appeler test techniques.
````javascript
class Car extends Vehicle {
    
    constructor(name, coffre) {...}
    
    klaxon() {          //<== c'est une méthode accessible au niveau de l'instance
        console.log(this.name + "klaxonne");
    }
}

let car = new Car(name:"AUDI", coffre:500);   // instance créé

console.log(car);

car.klaxon();

console.log(car.GRAVITY, car.testTechnique(car));    //je ne peux pas accéder à la propriété qui est statique
````
- **Pour accéder à ses propriétés et ses méthodes static il faut que j'y accède via la class directement**

- **Donc je nomme la class et j'accède à la propriété qui est static directement, on fait de même pour la méthode**.

- La méthode prend un véhicule en argument donc je lui passe ma voiture .

- Je sait que ma voiture a la propriété wheels donc je lui passe la propriété wheels, la propriété qui est téstée.

> Je récupère la propriété **GRAVITY ansi que testTechnique**, on voit que sa nous retourne 10 dans la console donc la valeur de **la propriété statique et sa me retourne true** pour mon **véhicule qui a 4 roues**.
> 
> Par défaut dans mon constructor lorque je consuit une voiture je donne le nom au constructor parent =>constructor(name, coffre) {...} et je donne **4 pour le nombre de roues.**

> **Si je donne 0 à chaque fois que je vais créer une instance de la class voiture toutes les voitures auront 0 roue**. Donc au niveau du testTechnique on ne **passera pas le testTechnique et sa nous retourna false**.

> **Donc on n'a vu comment déclarer une propriété static ainsi que des méthodes staticc**.
````javascript
//Modèle
class Vehicle {
    
    static GRAVITY = 10;
    static GRAVITY2 = 10;
    
    constructor(name, wheels) {
        this.name = name;
        this.wheels = wheels;
    }
````
- **On peut avoir plusieurs propriétés ainsi que plusieurs méthodes**

- On n'a vu également comment accéder à ses propriétés directement via **la class => console.log(car.GRAVITY, car.testTechnique(car));**

> Pareille pour **ma class car** je peux avoir une propriété static qui sera par exemple =>**static Coffre = 11;** et la valeur est 11.
````javascript
lass Car extends Vehicle {
    
    static Coffre = 11;
    
    constructor(name, coffre) {...}
    
    klaxon() {          //<== c'est une méthode accessible au niveau de l'instance
        console.log(this.name + "klaxonne");
    }
}
````
- **Je ne peux pas accéder à ses propriétés en faisant car.Coffre** dans ma console **car les propriétés static ne sont pas accessible au niveau des instances**,parcequ'elle sont static à la class, donc **il faut que j'appelle le nom de la class**, et on voit que s'affiche bien 11.
````javascript
console.log(car.Coffre, car.testTechnique(car));
````
> LIEN DE LA VIDÉO => [https://www.youtube.com/watch?v=9sjSMVCAspY&feature=youtu.be]
> ----