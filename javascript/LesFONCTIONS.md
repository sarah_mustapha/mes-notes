# Les Fonctions

- Sommaire:

[Les fonctions anonymes](#les-fonctions-anonymes)

[Les fonctions callback](#les-fonctions-callback)

[L'objet argument](#lobjet-arguments)

``Une fonction est un ensemble d'instruction effectuant une tache ou calculant une valeur, elle peut retourner des données ou non ``

- Pour **utiliser une fonction** il est nécessaire de l'avoir **définis**, **déclarer** avant de l'utiliser.

- Pour **utiliser une fonction**, on utilise le mot clé **function**, ensuite **on nomme la fonction**.
---

```javascript
ex:

function multiplyBy(nb, nb2) {
// instructions
console.log('nb :', nb);
console.log('nb2 :', nb2);
return nb * nb2;
}
multiplyBy(2, 4); // Puis on lui rajoute une variable comme si dessou
let produit = multiplyBy( nb4, nb2 );
console.Log(produit);
```
- On peut avoir **une fonction qui n'a aucun argument**.

- Après avoir **nommé la function** on va donner dans les parenthèses **les arguments de la fonction**, **les arguments de la fonction** ce sont **toutes les données qu'a besoin la fonction pour fonctionner correctement**.

``Les variables qui se trouve à l'intérieur de la fonction ne sont pas accessible à l'extérieur de la fonction.`
``
- Une fois avoir **declarer les arguments espacer dans la parenthèse**, on **ouvre des accolades**, qui correspond **au corp de la fonction**, ou il y aura **toutes les instructions de ma fonction**.

`Nous ne somme pas limité au nombre d'argument que peux avoir une fonction`

- **Une fonction n'est pas obliger de retourner une donnée**, mais le cas on **l'on veut retourner une donnée** on va utiliser le mot clé **return** dans la fonction.

- On **peut retourner tout type de donnée** 

- **Pour invoquer une fonction** on **la nomme par son nom** et **on ouvre des parenthèses**.

- **Dans les parentheses on doit donner les paramètres de la fonction**.

**/ rappel \ Les arguments c'est la ou l'on définit la fonction et les paramètres c'est la ou on invoque la fonction**.

- On va pouvoir **stocker dans une variable** le **retour de notre fonction**.

- On peut **appeler les fonctions** **autant de fois** que l'on souhaite.

-  Une fonction peut faire **les tache que l'on souhaite**, **Une fonction peut vérifier une donnée**.

````javascript
function checkValue(value) {
    console.log(value === 30);
}
checkValue(30);
````
- **On peut avoir d'autre argument qui on des valeurs par default**.

- On peut avoir une **fonction qui va vérifier deux arguments**.

````javascript
let arr = [2, 3, 4, 5,];

function afficheCHAQUEvALUER(arr) {
    console.log('Tableau:', arr);
    for (let i = 0; i < arr.length; i++) {
        console.log(arr[i]);
    }
}

afficheCHAQUEvALUER(arr);
````
- On peut avoir **une fonction ou chaque valeur est multiplié par 2**.

liens de la video > https://www.youtube.com/watch?v=wMt_WWSTjC0&feature=youtu.be

---

# Les fonctions anonymes

`Les fonctions anonymes c'est une fonction qui ne porte pas de nom.`

```javascript
let fn = function () {
    return 1;
}

fn();
console.log(fn(), fn());
```
- Lorsque **l'on crée une fonction anonyme** on va **stocker la référence de la fonction dans une constante ou une variable**.

- **On invoque la fonction en ouvrant des parenthèses**.

Liens de la vidéo => https://www.youtube.com/watch?v=K7S8vHg8i5g&feature=youtu.be

---

# Les fonctions callback

`Une fonction callback c'est une fonction qui va etre passée en arguments et qui va etre invoquée par une autre fonction `

```javascript
function maCallback(nb) {
    return nb * 3;
}
function maFonction(nb, callback) {
    let var1 = nb * 16;
    return callback(var1);
}
let res = maFonction(6, maCallback);
console.logres,(maFonction(5,maCallback))

```
- **la fonction callback peut prendre un argument un nombre**, en **deuxième argument une fonction**.

- **Stocker le résultat** de ma **fonction dans une variable nommé res**.

- Si on **n'invoque pas la fonction callback** on passe seulement **la référence**.

- Ma fonction **stock l'argument qu'on lui a donné** ensuite elle **l'exécute**.

Lien de la vidéo => https://www.youtube.com/watch?v=00OsnExIE4U&feature=youtu.be

---

# L'objet arguments

- La **fonction fn** peut prendre **plusieurs arguments**.

```javascript
function maFn(nb, nb2, nb3, nb4, nb5) {
    console.log(arguments);
} 
```
- ON peut **retirer la definition d'argument**.
```javascript
function maFn() {
    console.log(arguments);
}
maFn(1, 2, 3, 4, 5, 6, 7);
```
``
- On peut voir dans la console tout les elements grace a l'objet argument.

- On peut afficher dans la console l'argument 0, l'argument 5.

- On peut utiliser une boucle dans les arguments.

>/ rappel \ À chaque fois que j'invoque ma fonction c'est tout les elements qui vont être éxé exécuté.

>Lien de la vidéo => [https://www.youtube.com/watch?v=J42UvLufEts&feature=youtu.be]