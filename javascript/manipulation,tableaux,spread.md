> Sommaire:
> - [Spread operator](#spread-operator)
> - [Affecter par décomposition](#affecter-par-dcomposition)
> - [Manipuler les tableaux](#manipuler-les-tableaux)
> - [La méthode .forEach](#la-mthode-foreach)
> - [La méthode .find](#la-mthode-find)
> - [La méthode .findIndex](#la-mthode-findindex)
> - [La méthode .map](#methode-map)
> - [La méthode .reduce](#la-mthode-reduce)
> - [La méthode .some](#la-mthode-some)
> - [La méthode .every](#la-mthode-every)
----
# Spread operator

``Le Spread operator ce sont les ...(3 petits points) qui veux dire étaler.``
```javascript
let arr1 = ["Jean", "Morad", "Tom" ];
let arr2 = ["Pauline", "Nadia", "Sarah"];

let arr3 = [];

for(let i = 0; i < arr1.length; i++); {
    arr3.push(arr1[i]);
}
for(let i = 0; i < arr1.length; i{
    arr3.push(arr2[i]);
}
 console.log(arr3);

```
- **À la place de tapper tout sa** on peut créer un nouveau tableau et **étaler les valeurs de [arr1]et [arr2]** ainsi que le reste des valeurs.

```javascript
let arr3 = [...arr1,...arr2];
console.log(arr3,...arr2).
```
----
- Le **spread operator** nous évite de faire une boucle.

- Le **spread** peut aussi être **étalée dans des objets**.
````javascript
let studentDefault = {
    name: null, // indéfinis
    class: null,
};
let student = {
    ...studentDefault,
    name: "sam"; // Le spread permet de créer un nouvelle objet
};
console.log(student.name);
````

- Le **spread operator** peut également être utilisé **dans des objects**.
  
- Ce qui est bien c'est qu'on peut récrire des Propriétés.
  
- Sa **PERMET DE CRÉER** un **nouvelle objet** et de **modifier une valeur**.

> I ya une règle au niveau des objets des fonction et des tableaux c'est qu'il ne faut jamais les copiers , car on va copier la référence et pas la valeur.
> 
> À la place de copier un tableau, on crée un nouvel objet avec le spread operator

- Si on modifie la donnée se sera toute

/ Rappel \ Chaque valeur est associé a une référence, à un label, **à chaque fois que l'on veut copier un tableau il faut recréer la donnée**.

````javascript
// exemple tableaux
let arr1 = [1, 3];
let arr2 = [...arr1]; // ref=> donnée

arr2.push(5);

console.log(arr1, arr2);
````
- **arr1** ne sera pas affecter , grace au **Spread operator**.
  
- On utilise très souvent le **spread operator**.
````javascript
let arr = [1];
let arr1 = [2];
let arr2 = [3];
let arr3 = [4];

let arr4 = [...arr1, ...arr2, ...arr3];
console.log(arr4);
````
> On peut faire la même chose pour un objet.
````javascript
let obj = {a: 1};
let obj1 = {...obj, ...obj2, };
console.log(obj1);
````
----
> Lien de la video => [https://www.youtube.com/watch?v=w-vmVE5rzJc&feature=youtu.be].

-----
# Affecter par décomposition

```javascript
let a, b, rest;

let arr = [1, "J", true, "Bonjour"]; // 1 est la valeur a et J est la valeur b.

[a, b, ...rest] = arr;

console.log(a);
console.log(b);
console.log(c);
console.log(rest);

```
1. Je peux décomposer le **tableau arr** et **l'inséré directement** dans ma variable **a et b**.

- **On n'a affecté le tableau a et b en décomposant array**.(c'est une décomposition)

- On va **pouvoir récupérer true et bonjour** en les insérant dans une variable **...rest**. (bien sure il faut déclarer rest au-dessus dans la variable).

- Je viens **de décomposer mon tableau sans déclaration**.
---
> On peut aussi directement déclarer une constante ou une variable en décomposant un tableau.

````javascript

const [a, b, ...rest] = [1, 2, 3, 4, 5];

console.log(a);
console.log(b);
console.log(rest);
````
2. le mot **clé ...rest** nous **évite de tapper le reste du tableau (les autres valeurs)**.

- Nous ne sommes pas obliger de rajouter le mot cle **rest**

---
3. Parti Objet.

- **J'affecte par décomposition la variable Key**, mais pour que **cela fonctionne** il faut **afficher le tout** par **des parenthèses**.
````javascript
let key;
({key} = {key: "Jean"});
console.log(key);

````
- Ce qu'il se passe ici c'est que **je décompose**. 

- Il faut **toujours déclarer** la variable **avant de l'afficher**.

- Il n'y a pas de **question d'ordre**, tendit que lorsqu'on décompose un **tableau c'est different**.

- On **peut déclarer** soit **des constantes** soit **des variables** en **même temps** que **la décomposition**, en retirant les parenthèses.

- On peut modifier nos variables cela ne va pas changer nos objets.

- On peut renommer les propriétés l'or de l'affectation par décomposition, déclaratif.

- Il est possible de rajouter des valeurs par default

```javascript
let student = {name: "jean", class: "B", age: 20 };
//  Cet syntaxe pas decomposition nous évites de déclarer une variable et de l'assigner a une nouvelle variable du même nom.
//let name = student.name,
//     age = student.age;

let {name, age} = student; // On viens de simplifier cla déclaration du dessus.
```
- C'est plus agréable de décomposer que de déclarer chaque valeur ou de nouvelle variable.

> LIEN DE LA VIDEO =>[https://www.youtube.com/watch?v=3rpp3wzJ4Tg&feature=youtu.be]
----
# Manipuler les tableaux

> Il existe des méthodes qu'il faut **absolument connaitre** pour manipuler des tableaux, et **surtout les maitrisés**.

1. // .forEach()
2. // .find() / .findIndex()
3. // .map()
4. // .reduce()
5. // .filter()
6. // .som() .every()

> 1. La **méthode .forEach** => **nous permet d'effectuer une fonction pour chaque élément d'un tableau**.
> 2. La **méthode .find** => Nous permet de **trouver un élément** dans un tableau en **fonction d'une condition**.
> 2. La **méthode .finIndex** => Nous permet de **retourner l'Index** de l'élément qui **renvoie true**. 
> 3. La **fonction .map** permet de **modifier** et de **créer un nouveau tableau à partir d'un tableau**.
> 4. La **Fonction .reduce** nous permet de **réduire** un tableau à une certaine valeur, ou sous une autre forme.
> 5. La **Fonction .filter** nous permet de **filtrer** les elements d'un tableau en fonction d'une condition.
> 6. La **Fonction .som** permet de **retourner true** si une des valeurs **correspond à une condition** (si il n'y a aucune valeur qui correspond au condition elle **retournera false**)
> 6. La **Fonction .every** permet de **retourner vraie** si toutes les valeurs **respecte une condition** (si une valeur ne respecte pas une certaine condition, cette methode **retournera false**).

> Lien de la video => [https://www.youtube.com/watch?v=bw5S9DJTM9k&feature=youtu.be]
----
# La méthode forEach

```javascript
let arr = [1 , 4, 6, 8]; // J'ai un tableau avec des valeurs initiales.

for (let i = 0; i < arr.length, i++) {
   arr[i] ;
   // cela est une méthode pour pouvoir accéder à chaque valeur du tableau
}
```
> La méthode forEach nous permet de traverser le tableau de la même manière.

````javascript
let arr = [1, 4, 6, 8];

arr.forEach(function (value: number, index: number ) { 
    
}) // On utilise la méthode forEacbh sur le tableau.
````
- Entre parenthèse on doit lui donnée une fonction tout simplement.
- on donne en argument la valeur, en deuxième argument elle va récupérer l'index de la valeur.
> La méthode **forEach** va invoquer la fonction que l'on passe en argument pour chaque valeur du tableau.
- Donc pour la première valeur du tableau elle va invoquer la fonction.
- **Value** sera **égale à 1** et **index** sera égale **à zéro**.
> Puis elle va invoquer la prochaine valeur et ainsi de suite.
- Donc **value** sera **égale à 4** et **index** sera **égale à 1**.
> On peut le vérifier en affichant dans la console la valeur.

`````javascript
arr.forEach(function (value: number, index: number ) {
    
}
console.log(index, value);  // d'abord l'index et ensuite la valeur.
})
`````
> **La Fonction .forEach** va être **exécuté** pour **chaque valeur** du tableau, **comme le nom l'indique**.
> 
>**/ Rappel \ Nous ne somme pas obliger de préciser l'index, par contre il faut exécuter la valeur sinon sa ne sert à rien**. 
> 
> Avec le **méthode .forEach** c'est possible **d'invoquer une fonction** pour **chaque valeur**, et **sans la récupérer**, On peut **récupérer** également le tableau .

```javascript
arr.forEach(function (value: number, index: number, arrey:number[]) {
    
}
console.log(index, value, array);  // On va récupérer le tableau à chaque fonction .
return 'ok';
})
```
- **La fonction callback** ne **retourne aucune donnée** mais on peut **ajouter un return** si on le souhaite, mais sa ne sert à rien car la méthode **.forEach** ne retourne aucune donnée.

- **L'idée** de l'utilisation de **.forEach** c'est **simplement** d'exécuter **une fonction** pour **chaque élément de notre tableau**.

> Lien de la vidéo =>[https://www.youtube.com/watch?v=D6UW9BwDIoI&feature=youtu.be]
----
# La méthode .find

> **La méthode .find** nous permet de **récupérer** une valeur en **fonction d'une condition**.(Elle nous permet de **trouver une valeur**.)
> Pour utiliser cette méthode j'utilise un tableau.
```javascript
let arr = ["Jean", "Sam", "Steve", "Tom"]; // tableau

let callback = function (element, index) {  // soit je la stock dans une variable
   
    console.log(index, element);
    return false;
};
arr.find(function (value)  // je lui donne une fonction callback
// ou
arr.find(callback) ;  // soit je la donne dans un argument
```
- **Entre parenthèse** je dois lui donnée une **fonction callback** c'est une fonction qui va être **invoquée par la méthode** et qui est **donnée en argument**.

- Soit je la **définis** dans **l'argument (entre parenthèse)** ou soit je la **stock** dans **une variable**.

- **J'invoque la callback** dans **la parenthèse** et **non la fonction**.

- La méthode.find c'est le retour de la fonction, **.find** à besoin **d'une fonction** pour **fonctionner**.

> Lorsque **la fonction callback** va **invoquer la première valeur** ,**Jean sera stocké dans élément** et **index vaudra 0**, si la fonction **retourne false** il va **continuer à invoquer** la fonction pour **la seconde valeur**, donc **sam sera stocké dans l'élément** et **l'index** vaudra **1** et si la fonction **retourne false** sa **continue** pour le reste du tableau, et si la **fonction retourne true**il va retourner la valeur ex: **Tom à cette endroit**.
`````javascript
let arr = ["Jean", "Sam", "Steve", "Tom"]; // tableau

let callback = function (element, index) {
    console.log(index, element);
return false;
};
let res = arr.find(callback) ; // on va pouvoir récupérer la valeur

`````
- On va pouvoir **récupérer** la valeur dont la fonction **retourne vraie**.

- **J'affiche dans la console** **l'élément et l'index** pour être **sur** de **récupérer l'élément et l'index**.

> Pour savoir si une chaine de caractère contient un **o => 'TOM'**, je peux utiliser **la méthode=> .includes** et entre parenthèse je lui donne l'élément que je recherche ('o').
````javascript
let arr = ["Jean", "Sam", "Steve", "Tom"]; // tableau

let callback = function (name, index) {
    
if (name.includes(searchElement:'o')) {
    return true;
} else {
    return false;
}
};
let res = arr.find(callback);
console.log('res:', res);
````
- Si l'élément **inclus un o** c'est que la condition est **vraie** donc l'élément **sera exécuté**, je pourrais **retourner true**, mais si l'élément ne **contient pas de true** alors c'est le **sinon** qui sera **exécuté**, je retourne **false** pour **continuer la méthode .find**.

- Ensuite **le résultat** je vais le **stocké** dans la variable **let** puis **l'afficher dans la console**, si l'interpréteur **trouve** un élément **true** il va le **stocker** dans la variable **res**.

```javascript
let arr = ["Jean", "Sam", "Steve", "Tom"]; // tableau

let callback = function (name) {
    return name.includes(searchElement:'o') { // on n'a pas besoin de rajouté la condition if comme sur le dessus.
};
let res = arr.find(callback);
console.log('res:', res);
```
> Il est possible de faire une seule ligne, ici on n'a pas besoin de la condition if et de l'index, name me permet de récupérer ma fonction qui contient un o.
> 
> Lien de la video => [https://www.youtube.com/watch?v=2UWs5heVtow&feature=youtu.be]
----

# La méthode findIndex

> La méthode **findIndex** permet de **retourner l'index** qui **respecte une condition**, c'est le **même comportement que find** sauf que cette fois si **sa ne va pas nous retourner** la valeur mais **son index**.
>
> Entre la méthode **find** et **findIndex** la plus utilisé est **findIndex**.
```javascript
let arr = ["Jean", "Sam", "Steve", "Tom"];// index Tom

arr.findIndex(predicate: function ( {
    
    return true
});
```
- Pour utiliser la méthode **.findIndex** j'utilise sur **un tableau**.

> En **argument** je dois lui passer une **fonction callback** qui va être **invoqué** pour **chaque valeur** jusqu'à ce qu'on **trouve une valeur** qui **correspond à notre condition**, donc pour cela c'est jusqu'à que la **fonction callback retourne true, pour que l'on nous retourne l'index de l'élément qui retourne true**.

```javascript
let arr = ["Jean", "Sam", "Steve", "Tom"];// index Tom

let callback = function (element, index, array) {    // decclarer la fonction callback pour que ça soit plus lisible 
                                                    // cette fonction prend en argument le premier élément
    console.log(element, index)        // on vérifie avec lesquelle on travaille
});   
arr.findIndex(callback);    // findIndex va passer en premier argument l'élement, l'index en deuxième argument, ainsi que le tableau en troisième argument
    
    return true
});
```
- Je déclare une variable callback qui va contenir ma fonction callback pour que ça soit plus lisible .

- Ma fonction callback prend en première argument l'élément, donc quand elle va être invoquée par la méthode fine index.

- La méthode findIndex va passer en argument le premier élément 

- findIndex va passer en premier argument l'élément, l'index en deuxième argument, ainsi que le tableau en troisième argument si on le souhaite et qu'on en n'a besoin sinon non.

> **Première chose à faire** toujours **vérifier** avec lesquelles on travaille avec **la console .log**.
````javascript
let arr = ["Jean", "Sam", "Steve", "Tom"];// index Tom

let callback = function (element, index, array) {    // decclarer la fonction callback pour que ça soit plus lisible 
    // cette fonction prend en argument le premier élément
    console.log(element, index)        // on vérifie avec lesquelle on travaille                       
    return element.length === 5;
};
let index = arr.findIndex(callback); 
````
- Ici on doit avoir une condition la condition sera la même que précédemment, on peut la modifier également mais cette fois demander un élément ou la longueur est supérieur où est égal à 5.

- Ici il y a qu'un seul prénom qui a une longueur de 5 caractères c'est **steve**, donc l'index qui sera retourné c'est l'index numéro 2 qui correspond à l'index de la valeur steve..
```javascript
   return element.length === 5;
};
let index = arr.findIndex(callback);
console.log('Index:', index, arr[index]);`;
```
- **À partir du moment ou cette condition est vrai la méthode finIndex va s'arrêter et elle v'a me retourner un index, donc cet index je vais pouvoir le stocker dans une variable que je vais nommer index**.

- **On peut accéder à la valeur en utilisant l'index qui est stocké**.
```javascript
let index = arr.findIndex(callback);
```
> **C'est pour cela que ça sera plus utiliser que fin car ça sera plus facile a manipulé**
> - Au lieu de récupéré la donnée on récupère l'index , donc ensuite on va pouvoir récupérer la donnée grâce à l'index et on peut également manipuler d'autre tableaux grâce à la position .

```javascript
let index = arr.findIndex(callback);
console.log('Index:', index, arr[index]);
```
> **Donc là on récupère l'index, on affiche l'index et on récupère la valeur qui se trouve à la position stocker dans la variable index.**
> 
> / IMPORTANT \ **Il faut bien comprendre qu'à partir du moment que la fonction callback retourne vraie la méthode .findIndex s'arrête et va me retourner l'index.**
> Lien de la video => [https://www.youtube.com/watch?v=f8zRojo4T5s&feature=youtu.be]
----
# METHODE MAP

> La méthode .map nous permet de **créer un tableau à partir d'un autre tableau**, c'est souvent **utilisé pour modifier un tableau ou pour recréer un nouveau tableau à partir d'un autre tableau**.

```javascript
let arr = [1, 2, 4, 56, 78, 99, 123, 456];

arr.map(function (value) { 
    
});
```
> **Ce que je souhaite c'est créer un nouveau tableau qui contient toutes les valeurs de arr1 multiplier par2**.
>
> Pour cela on **utilise une boucle for pour boucler à l'intérieur du tableau** et ensuite on multiplie chaque élément et on les pousse dans un nouveau tableau.
>
> **Avec la methode .map on peut le faire très facilement**.

- Pour utiliser la méthode .map on l'utilise dans un tableau **arr.map**

- Entre parenthèse je dois lui passer une **fonction callback** **(c'est une fonction qui va être invoqué par la méthode .map)** qui sera **invoqué pour chaque élément du tableau**.

- La méthode map va retourner un nouveau tableau, que l'on va pouvoir stocké dans le nouveau tablea **arr2.**

`````javascript
let arr = [1, 2, 4, 56, 78, 99, 123, 456];

let callback = function (value, index, array) {  // la fonction prend pour argument la valeur courante.
 console.log(value, index, array)   // afficher la console pour savoir ce que l'on fait.

    return value * 2;
};
    

ler arr2 = arr.map(callback); // je place la réference callback , qui contient la référence de la fonction.
console.log(arr2);
`````
- Je vais ensuite afficher **arr2 dans la console**.

- **Je déclare la fonction dans une variable callback**.

- **À chaque fois que j'invoque ma fonction callback** je retourne toujours **ma valeur multiplier par 2**

> On peut **voir le résultat de notre valeur arr2**qui contient chaque valeur du tableau **arr * 2**.
> 
> Dans le cas ou ils sont pair ou impair on va retourner 

```javascript
let arr = [1, 2, 4, 56, 78, 99, 123, 456];

let callback = function (value, index, array) {  // la fonction prend pour argument la valeur courante.
 console.log(value, index, array)   // afficher la console pour savoir ce que l'on fait.

    return value % 2 ? "Inpair" : "Pair";
};
```
- Dans ce cas je vais avoir un tableau avec des chaines de caractère qui résume le résultat.

- **On peut aussi concaténé avec la valeur** 

```javascript
let arr = [1, 2, 4, 56, 78, 99, 123, 456];

let callback = function (value, index, array) {  // la fonction prend pour argument la valeur courante.
 console.log(value, index, array)   // afficher la console pour savoir ce que l'on fait.

    return value % 2 ? "Inpair : "+ value : "Pair :" + value;
};
```
- Dans ce cas la **j'utilise le ternaire** car en une seule ligne je peux créer **la même chose que la condition if else**.

- **Je peux également modifier un tableau qui contient des objets**.

```javascript
let arr = [
    {age: 22},
    {age: 17},     // ici on n'a 3 objets qui correspond à un utilisateur
    {age: 56},
];

let callback = function (value) {  // la fonction prend pour argument la valeur courante.
 console.log(value) ;  // afficher la console pour savoir ce que l'on fait.

    return value.age;
};
let arr2 = arr.map(callback);
```
- Ici on voit que **value correspond au premier objet** ensuit correspond au **deuxième et ainsi de suite**.

- Ce que l'on peut faire c'est **créer un tableau cette fois d'age**, **en retournant la propriété. age**. 

> **.map permet de créer un nouveau tableau à partir d'un autre**.

- On va **changer la valeur et ce retour de la fonction sera stocké dans la variable arr2** on peut faire ce que l'on veut en **fonction de la donnée que l'on n'a de base et fonction que l'on n'a à la fin**.

> LIEN DE LA VIDEO => [https://www.youtube.com/watch?v=TAHJwuwRp7U&feature=youtu.be]

----
# LA MÉTHODE REDUCE

> **La méthode .reduce permet de réduire un tableau sous une donnée, ou sur un autre type de donnée**.

```javascript
let arr = [3, 4, 49, 230, 48, 123]; // 3922 

arr.reduce(function (acc:number, value:number, index:number) {  // fonction callback
    
}, 0);
```
- Je souhaite **réduire mon tableau de nombre en un nombre(3922), qui sera le cumule de l'ensemble des nombres en tableau**.

- Pour cela **ce qu'on peut faire c'est de créer une boucle for**, traverser dans notre tableau array et additionner dans une variable somme.

>**Pour utiliser la méthode .reduce ()**, on indique **dans les parenthèses une fonction callback**.
>
> - **On va lui donner un second paramètre qui va commencer à 0**, car **je veux accumuler tous les nombres du tableau arr**.
>
> 1. **En premier argument je vais récupérer l'accumulateur**, **l'accumulateur c'est une donnée qui va être passé entre chaque fonction**, que je raccourcis par **acc**

> 2.  **En deuxième argument je vais récupérer la valeur à laquelle la fonction est exécutée**.

> 3.  **En troisième argument je vais récupérer son index**.

> 4.  **En quatrième argument je vais récupérer le tableau**.
> 
> - Je vais **vérifier dans ma console ce que contient l'accumulateur**, la valeur et ce que contient **l'index**.

`````javascript
let arr = [3, 4, 49, 230, 48, 123]; // 3922 

arr.reduce(function (acc:number, value:number, index:number) {  // fonction callback
    console.log(acc, value, index);  
    return "ok"; // la première fois l'acc vaut 0 et ensuite il vaudra "ok".
}, 0);

`````
- **La première fois que l'accumulateur est exécuté il vaut le nombre initial** et ensuite pour les autre fois il **vaudra la valeur qui sera exécuté par la fonction précédente**.
  
- **À chaque fois que ma fonction est exécuté ma valeur est enregistré dans la value**.

- Donc l'accumulateur on v'a **le retourné à chaque fois** et on v'a le modifier.

````javascript
let arr = [3, 4, 49, 230, 48, 123]; // 3922 

arr.reduce(function (acc:number, value:number, index:number) {  // fonction callback
    console.log(acc, value, index);

    acc = acc + value;

    return acc;
}, 0);
````
> La première fois que la fonction est exécuté acc vaut 0 la valeur vaut 3 donc on fait 0 + 3 donc 3 et on retourne 3.
> 
> La deuxième fois que la fonction est exécuté acc vaut 3 , la valeur vaut 4 donc on fait 3 + 4 donc 7 et on retourne 7.
> 
> La troisième fois que la fonction est exécuté acc vaut 7 , la valeur vaut 49 donc on fait 7 + 49 donc 56 et on retourne 56.

- À la fin ce qui me sera retourné c'est l'accumulation, on peut récupérer l'accumulation et la stocké dans la variable accumulation puis l'affiché dans la console avec mon tableau.

```javascript
let arr = [3, 4, 49, 230, 48, 123]; // 3922 

arr.reduce(function (acc:number, value:number, index:number) {  // fonction callback
    console.log(acc, value, index);

    acc = acc + value;

    return acc;
}, 0);

console.log(accumulation, array);
```
> **Grâce à la méthode reduce j'ai réduit ce tableau à une donnée**.

- Ce que l'on peut faire aussi c'est de **créer un nouveau tableau**, à la place de rechercher l'accumulation **on va push la valeur dans le tableau et retourné** ensuite le tableau.

````javascript
let arr = [3, 4, 49, 230, 48, 123]; // 3922 

let accumulation = arr.reduce(function (acc:number, value:number, index:number) {  // fonction callback
    console.log(acc, value, index);

    acc.push(value); // on va push la valeur.

    return acc;
}, []);       // on peut recréer un objet ou une chaine de caractère.

console.log(accumulation, array);
````
- À chaque fois il va push la valeur.

- **Si on recrée un tableau avec la méthode reduce sa ne sert à rien vaux mieux utilisé la méthode map.**

> / Rappel \ Il faut savoir utiliser la meilleure méthode en fonction de notre besoin.

````javascript
acc = acc + value;
````
- On remet le code ici pour avoir **l'accumulation**.

- **On accumule et on sauvegarde l'accumulation puis on retourne l'accumulation**.

- Donc à la fin on **récupère l'accumulation qu'on va stocké dans l'accumulation**.
>
> Lien de la video => [https://www.youtube.com/watch?v=sNSV0NBShPc&feature=youtu.be]
----
# LA METHODE FILTER

> **La méthode filter permet de filtrer les éléments d'un tableau en fonction d'une condition**.
>
> **Pour utiliser cette methode je l'utilise sur un tableau.**

````javascript
let arr = [3, 4, 49, 230, 7, 48, 123];

arr.filter(function (element,index, array [] ) {  // callback
    console.log(element, index);
});
````
- **Je passe en paramètre une fonction callback**.

- **Je récupère en premier argument l'élément**.

- **En deuxième argument l'index.**

- **En troisième argument le tableau**.

- **Je vérifie dans la console avec quoi je travaille**.

- **La fonction callback v'a être invoqué** pour **chaque valeur** dans mon tableau.

- Cette fonction doit retourner un **boolean soit true soit false**.

````javascript
let arr = [3, 4, 49, 230, 7, 48, 123];

arr.filter(function (element,index, array [] ) {  // callback
    console.log(element, index);
    
    return true;
});
````
- **Si la fonction retourne true, la première valeur v'a être gardé**.

- **Si la fonction retourne false la première valeur ne sera pas gardé**.

- Cette **méthode filter retournera un tableau qui constituera tous les éléments qui auront été filtrés**.

- Donc je vais **stocker ce tableau dans une variable que je vais nommer Filtered**.

````javascript
let arr = [3, 4, 49, 230, 7, 48, 123];

let arrFiltered = arr.filter(function (element,index, array [] ) {  // callback
    console.log(element, index);
    return true;
});
console.log(arrFiltered);
````
- J'affiche ce tableau dans la console.

- COMME JE GARDE TOUS MES ÉLÉMENTS, DONC DANS LA CONSOLE J'AURAIS TOUS MES ÉLÉMENTS.

- **Si je retourne false je ne garde rien du tout**, donc mon tableau **sera vide**.

- EN FONCTION DE LA CONDITION JE VAIS POUVOIR GARDER OU RETIRER DES ÉLÉMENTS DONC LES FILTRER.

````javascript
let arr = [3, 4, 49, 230, 7, 48, 123];

let arrFiltered = arr.filter(function (nb,index, array [] ) {  // callback
    console.log(nb, index);
   
    if (nb % 2) {
        return true;
    }else {
        return false;
    }
});
console.log(arrFiltered);

````
- Ici ma condition est **si un élément est impair**, l'élément sera la valeur **donc je la renomme**, car **c'est un tableau de nombre**.

- **Je vais retourner true pour garder seulement les nombres impairs**.

- **SINON je retournerais false pour les nombre qui sont pair**.

- **Donc à la fin je vais avoir un tableau de nombre impair**.

````javascript

    if (nb % 2) {
        return false;
    }else {
        return true;
    }
});
````
- La condition si je la change ou si je change la valeur de retour en fonction de la condition, donc si je dis que les nombres impairs je les enlèves, donc je retourne false et que je garde les nombre pair dans ce cas la je vais avoir un tableau de nombre pair.

- Je peux le simplifier et seulement avoir un retour du % de 2, donc du nombre % de 2.

````javascript
let arr = [3, 4, 49, 230, 7, 48, 123];

let arrFiltered = arr.filter(function (nb,index, array [] ) {  // callback
    console.log(nb, index);

    return nb % 2; //  => true
});
console.log(arrFiltered);
````
- **Ici le nombre est impair il va me rester 1 qui est l'équivalent de true**.

- **SI le nombre est pair il va retourner 0 qui est l'équivalent de false**.

- En laissant les conditions comme tel **je vais garder seulement les nombre impairs**. 

- **Dans mon tableau je vais garder seulement les nombres impairs**.

- **Pour garder les nombre pairs** il faut que **j'inverse la condition en l'entourant par des parenthèses** et je rajoute le **non logique ! donc l'inverse**.

````javascript
   return !(nb % 2); 
````
- **Donc les nombres impairs vont me retourner true sauf que l'inverse de true sera false**.

- Pour les nombre impairs je vais les enlever lorsque le nombre est pair ici ca va me retourner 0 , **il me restera 0 et l'inverse de 0 c'est true** donc je vais **garder les nombres pairs**.

- La conditipn peut être tout autre chose comme vérifier si le nombre est supérieur par exemple 40.

````javascript
return nb > 40 && nb < 200;
````
- **Si le nombre est supérieur à 40 je le garde** sinon je l'enlève, ici **j'ai gardé seulement les nombres supérieur à 40**.

- Et inférieur à 200.

- **Ici je vais garder seulement les nombres qui sont entre 40 et 200**.

- Donc la **c'est en fonction de ce que l'on veut filtrer et comment on veut le filtrer**.

> **Cela est possible grace à la méthode .filter**.
> 
> LIEN DE LA VIDÉO => [https://www.youtube.com/watch?v=qyG33rLcAhY&feature=youtu.be]

----

# La méthode some

> **La méthode some est utilisé pour savoir si au moins une valeur du tableau valide une condition**.
> 
> **La méthode some va nous retourner true dans le cas ou une des valeurs valide la condition**.

> **La méthode some va nous retourner false dans le cas ou aucune des valeurs ne valide la condition**.

````javascript
let arr = [3, 4, 49, 230? 7, 48, 123];

arr.some(function (value, index) { // fonction callback
    console.log(value, index);
})
````
- **Pour utiliser la méthode some je l'appelle sur un tableau**.

- Je lui donne **en premier argument une fonction callback**, qui sera invoquée pour **chaque valeur de mon tableau arr**.

- **La première fois que la fonction sera invoquée value sera égale à 3 et l'index à 0.**

- **La deuxième fois value doit être égale à 4 et l'index doit être égale à 1**.

- **La troisième fois value doit être égale à 49 et l'index à 2**.

- Ensuite on peut recharger dans la console pour **voir le résultat**.

> Ce que je veux c'est savoir **si au moins un des nombres est inférieur à < 10**.

````javascript
let arr = [3, 4, 49, 230? 7, 48, 123];

arr.some(function (value, index) { // fonction callback
    console.log(value, index);
    
    if (value < 10){
        return true;
    } else {
        return false;
    }
})
````
- **Pour cela je dois tester chaque nombre**. 

- Je veux savoir si **ma value est inférieure à 10** si cette condition est vrai je lui **retourne true**.(Dans le cas j'ai au moins une valeur qui est inférieure à 10).

- **Sinon avec le (else) je retournerais false**.

- On comprend que c'est une répétition inutile, **on peut directement retourner l'expression elle même**.

````javascript
let arr = [3, 4, 49, 230? 7, 48, 123];

arr.some(function (value, index) { // fonction callback
    console.log(value, index);

    return value < 10;
});
````
- On comprend bien que la première fois que value est exécuté pour la valeur 3 du tableau arr, **3 est bien inférieur à 10**.

- **La méthode some va s'arrêter dès la première valeur**.

- En vérifiant on voit que **la fonction callback est appelé qu'une seule fois**, car dès la première valeur on **retourne true**.

- Donc **le retour de la méthode some je peux le stocker dans une variable** pour savoir si ma **condition est vraie**.

- **Je stock dans la variable cond** et je vais afficher ensuite **la condition pour savoir si dans mon tableau j'ai au moins au nombre inférieur à 10**.
````javascript
let arr = [3, 4, 49, 230? 7, 48, 123];

let cond = arr.some(function (value, index) { // fonction callback
    console.log(value, index);

    return value < 10;
});
````
- Et la ca me **retourne true** dans la console car **effectivement 3 est inférieur à 10**.

- **On peut aussi changer la condition** pour savoir **si value est supérieur à 1000 par exemple et la ca va me retourner false**, car aucun nombre dans le tableau est **supérieur à 1000**.

> Lorsque la fonction retourne **true cela arrête la méthode some**, lorsque la fonction **retourne false sa continue de tester chaque valeur du tableau**.

> **Dès que le .some rencontre une valeur ca s'arrêtera**.
> 
>**/ RETENIR \ Le some s'est pour savoir si au moins une des valeurs est vraie et qui correspond à une condition.**
> 
> LIEN DE LA VIDEO => [https://www.youtube.com/watch?v=RHBRHj5qKnc&feature=youtu.be]
----
# LA MÉTHODE EVERY

> **La méthode .every permet de tester un tableau et de vérifier que toutes les valeurs du tableau valide une condition**.

````javascript
let arr = [3, 4, 49, 230, 7, 48, 123];

arr.every(function (value, index, array[]) {   // callback
 console.log(value, index);
 
 return false;
});
````
> - Pour l'utiliser **je l'utilise avec le tableau arr, et j'utilise la méthode every** sur ce tableau.

> - Je vais lui passer **la fonction callback** qui sera exécuté **pour chaque valeurs du tableau arr**.

> - 1. Je récupère en première argument la valeur.

> - 2.  En deuxième argument l'index.

> - 3.  En troisième argument le tableau.
 
- Je ne vais pas utiliser de tableau, donc je peux le retirer.
 
- On **vérifie avec quelle argument on travaille en affichant dans la console**.

- La console m'affiche seulement **une des valeurs puisque il faut que je retourne une donnée**.

- **Il faut que toutes les valeurs de mon tableau valide une condition**.

- **Si il y a une valeur qui retourne false à cause d'une condition**, sa **retournera false**.

````javascript
let arr = [3, 4, 49, 230, 7, 48, 123];

arr.every(function (value, index, array[]) {   // callback
 console.log(value, index)  
 
    return false;   
});
````
- Dans le cas ou une valeur **retourne false c'est qu'il n'y a aucune condition**.

- **Donc si il y'a une condition vraie cela va me retourner true**.

`````javascript
let arr = [3, 4, 49, 230, 7, 48, 123];

arr.every(function (value, index, array[]) {   // callback
 console.log(value, index)  
 
    return true;   
});
`````
> **Every en francais c'est chaque donc il faut que toutes les valeurs valide une condition**.

- Ici la condition sera **est-ce que la valeur est bien égal à chaque valeur dans mon tableau**.

````javascript
let arr = [3, 4, 49, 230, 7, 48, 123];

arr.every(function (value, index, array[]) {   // callback
 console.log(value, index)  
 
    return value > 0;   
});
````
- **Est-ce que la valeur est supérieur à 0**.

- Dans mon cas **toutes les valeurs sont supérieur à 0**, donc **la condition est valide pour chaque valeur de mon tableau**.

- Ce qui sera **sauvegarder dans la variable cond** sa **sera le boolean true**.
`````javascript
let arr = [3, 4, 49, 230, 7, 48, 123];

let cond = arr.every(function (value, index) {   // callback
 console.log(value, index)  
 
    return value > 0;   
});
`````
- Dans le cas ou il y a **une valeur qui ne valide pas la condition** même **si c'est la dernière cela me retournera false**.

> / rappel \ c'est le même concept que pour les méthodes qu'on n'a vu, la méthode e.every est la moins utiliser mais qui peut être utilisé.
>
> **Des méthodes il en existe énormément**, on n'a juste à **tapper méthode array sur mozilla** et on peut accéder à **la documentation des tableaux**.
> 
> **/ ! \ Il faut absolument connaitre ses méthodes sur le bout des doigts**.
> 
> LIEN DE LA VIDEO => [https://www.youtube.com/watch?v=3lccOfejkA4&feature=youtu.be].