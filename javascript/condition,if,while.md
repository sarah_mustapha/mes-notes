# Declarer des conditions

````
Les conditons sont très utiliser lorsque l'on developpe des scripts / sites web / application`
````
- On utilise mot clé **if** pour créer une condition.

```javascript
cons age = 19;

if (age >= 18 ) { 
console.log('majeur')
} else if (age >= 13 && age <18) { 

}
```
`Si cette condition est vrais , tout ce qui sera écris après les accolades {Le bloque d'instruction} sera éxécuter`

- On écris ce que l'on veut dans les accolades.

- On utilise le mot clé **else** pour **alors**.

`Si le code est faut sa sera **else if** qui sera éxécuter, et si le code else if est faux sa sera **else** qui sera éxécuter.`

`A partir du moment ou une condition est éxécuter les autre ne serons pas éxécuter.`

---
# Le ternaire

`Est un operateur conditionel qui à 3 opérands`

- **?** > si l'expression est vraie.

- **:** > si l'expression est fausse.

- Cela nous permet d'écrire une seule condition (une seule phrase).

# L'opérateur switch

`Nous permet de réaliser des condition en fonction de l'expression`

- Si ma variable fonctionne avec un **case** > (dans le cas), je casse le **switch**.

- Pour rajouter un autre cas on rajoute **case**.

- On peut rajouter un **break** pour casser l'expression.

/Rappel\ Lorsque l'on est développeur on évite de se répéter.

- On peut avoir **2 cas possible** qui vont exécuter **les mêmes instructions**.

- Ne pas oublier d'ajouter **break** sinon notre **switch** ne va pas s'arrêter, jusqu'à que l'interpréteur rencontre **un break** ou la fin de notre **accolade }**.

- Le mot clé **default** est optionnel (pas obliger).

``LA LECTURE SWITCH EST BEAUCOUP PLUS SIMPLE L'ORQUE L'ON N'A UNE VARIABLE ET L'ON VEUT CONNAITRE SI ELLE EST ÉGALE À CERTAINE VALEURS(UNE CORRESPONDANCE) IL EST PRÉFÉRABLE D'UTILISER LE SWITH CASE, QUE LES IF ETC..ON UTILISE LES IF ETC.. POUR DES CONDITIONS PLUS COMPLEXES``

- On peut avoir **des variables** entre les case, ce qui est interested.

- Un **block d'instruction** est **définie par des accolades** ouvrante et fermente .

``` Lorsqu'une variable est déclaré dans le block d'instruction elle n'est pas accessible à l'extérieur.` 

---
# LES BOUCLES

---

## La boucle for

``Les boucles en javascript nous permet de rajouter des actions X fois``

- La syntaxe de la boucle for est le mot clé **for**.

- À partir du moment ou la boucle est **vrai** la boucle va se **répéter** à l'infini.

// for ( [ expressionInitial]); [condition]; [expressionIncremente]) {
// instructions
// }

- L'expression Initial est > quand es que la boucle commence.

- Condition > quand la boucle va continuer de tourner, t'en que cette condition est vraie la boucle va continuer à se répéter.

- Incremente > va être l'expression qui sera réalisé a la fin de chaque boucle et qui va rendre l'expression fausse a un moment.
````javascript
for (let i = 0; i < 10; i++);
````
`A CHAQUE FIN DE BOUCLE ON VA INCREMENTER i DE + 1`

- On affiche **i** dans la console.`

`
/Attention\ si notre condition est toujours vrai on va faire cracher notre navigateur, a aucun moment notre boucle s'arrêtera. Dans ce cas l'a on doit ouvrir notre moniteur pour stopper l'activité de google chrome, la condition doit toujours être finis.
`

/ ! \ Il faut vraiment manipuler les boucles en faisant attention.

- On peut casser la **boucle** en utilisant le mot clé **break**.

- Si j'ajoute un **-1** je rajoute un **=égale** >= -1 .

/ rappel \ Pour accéder à une valeur dans un tableau on utilise **des crochets []** .

---

# L'instruction do ...while (tandis que)

``C'est litéralement en francais **faire t'en que**``

- On utilise le mot clé **do**.

- On ouvre les accolades et dans **les accolades** il **y aura **LES FONCTIONS** qui seront répété** .

- Après les accolades il y aura écrit **while**.

- T'en que **l'instruction est vraie** tous ce qui se trouve **après le do sera répété**.

````javascript 

          do {
              instructions
          } while (condition)
````

- Pour L'incrémentation on incrémente avec **i++** avant de l'afficher dans la console.

- On peut également rajouter le mot clé **break** pour stopper la condition.

----

# L'instruction while

`Permet de créé une boucle t'an que l'instruction est vérifié.`

- On utilise le mot clé **while**.`

````````javascript
ex:
       while (condition)  {
           instruction
     }
``````
@   