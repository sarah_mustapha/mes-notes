# Prise de note Javascript

[Note javascript 2](#note-javascript-2)
---
## JAVASCRIPT

- Est un **language de programmation de script**, une page web interactive.

- Apparition en **1995 au début de l'internet**.

- `OBJECTIF

- Dynamiser les sites webs grace à des intéractions et Animations.

- Le HTML / CSS et le JAVASCRIPT sont les Technologies les plus utilisé sur le web.

- Nom abrégé de JAVASCRIPT : *JS*

- Live SCRIPT à étais renommé en JAVASCRIPT.

- JAVASCRIPT est employé **coté client** sur le navigateur pour les mobiles, les montres connecter, la télé connecter de l'utilisateur. Dans ce cas il sera utilisé pour développer une interface utilisateur réactive, ou pour un site web dynamique.

- JAVASCRIPT est également utiliser coté serveur pour développer tout type d'application.

---

##ECMASCRIPT

- JAVASCRIPT respecte les standards établie pas ECMA INTERNATIONAL (on dit que le langage est standardisé).

- JAVASCRIPT est un **ensemble de norme concernant les languages de screamting (programmation)**, est standardisé par ECMA INTERNATIONAL.(ECMA262)

- Il s'agit donc d'un standard dont les spécifications sont mise en oeuvre dans différents langage de SCRIPT comme JAVASCRIPT ou ACTIONSCRIPT

- Les standards ECMASCRIPT sont édités annuellement.

- Les navigateurs les plus récents supporte tousse US2015 pour ECMASCRIPT 2015.

### ATTENTION / ! \

Les navigateurs ne supporte pas toutes les dernières versions de JAVASCRIPT
notamment les plus *anciens*`

- Il est possible d'adapter votre script au navigateur ciblé ou d'utilisé un **POLYFILL**

---

## POLYFILL

- Est un bout de code utilisé pour fournir des ** fonctionnalités récente sur des anciens navigateur** qui ne les supporte pas.
---

## TRANSPILEUR

- Est un **type de compilateur qui prend le code source d'un langage de programmation et le compile dans un autre language de programmation** , il traduit votre code vers un code cible

- Permet de **transformer du code en code dans un même language**.

exemple: JAVASCRIPT ES6 en JAVASCRIPT ES5 ou du JAVASCRIPT non optimisé en JAVASCRIPT optimisé.

- La transpilation va surtout nous permettre de transformer automatiquement du JAVASCRIPT dernière generation (ES6,ES6,ES8...) en JAVASCRIPT lisible par les navigateurs les plus anciens.

- Grace au transpileur pas besoin de choisir, vous pouvez utiliser les fonctionnalités les plus modernes et produire des applications universellement accessibles.
--- 

## ANGUILAR / REACT

- Font parties des *frameworks* JAVASCRIPT les plus utilisé.

- Ils **permettent la création de l'application web complexe**, l'ajout de partie dynamique sur un site web déjà existant.
---
- NODEJS permet **d'exécuter du code JAVASCRIPT coter serveur**.
---
- THREEJS est une **LIBRAIRIE 3D qui permet d'ajouter des animations à effet de lumière ou jeux 3D à votre site**.


## Les boîtes ou projet qui utilise JAVASCRIPT

- Toutes les entreprises ayant une application ou un site internet on probablement utiliser du **JAVASCRIPT** COMME FACEBOOK / INSTAGRAM / LE BON COIN....

- Nous pouvons travailler dans tout type de société lorsque nous somme **compétant**, la demande est très présente.

# ÉCRIRE DU CODE JAVASCRIPT

- Ouvrir un fichier JAVASCRIPT

- click droit sur mon dossier / new / file / insérer le nom de mon fichier (index.JS) appuyer sur entrer.

- Ensuite nous pouvons écrire notre code JAVASCRIPT

- Nous pouvons également créé un FICHIER **HTML** en click droit sur notre dossier (.html).

- Mettre un ! (point d'exclamation) puis faire une tabulation ou sinon tout taper a la main sur notre page.

- Dans notre balise **BODY** ajouté une balise **SCRIPT** puis ajouté le type de text qui sera du JAVASCRIPT. A l'intérieur de notre balise, nous pouvons écrire du code JAVASCRIPT également (dans notre** FICHIER index.HTML**)

- click droit sur INDEX.HTML / OPEN IN BROWSER / CLICK SUR CHROME

- Nous pouvons inspecter pour ouvrir la console

- Ouvrir une balise **SCRIPT** écrire **src** et indiquer le chemin d'accès vers notre fichier **JAVASCRIPT** pour lier mon fichier JAVASCRIPT au HTML (INDEX.JS).

## MOTEUR JAVASCRIPT

`Est un programme logiciel qui interprète et exécute du code en langage JAVASCRIPT` .

- Sont généralement intégré au navigateur web.

- **Spider monkey est le premier moteur JAVASCRIPT**.

- Une ligne de code est une instruction et le point virgule **(;)** permet de préciser que l'instruction est terminé.

- Un bloke d'instruction est délimiter pas une accolade ouvrante et fermente.

## LES VARIABLES ET CONSTANTES

- Est une case mémoire qui contient ou non une valeur (elle peut changer au cours du script)

- À l'inverse les constantes ne peuvent pas changer de valeur

- Pour utiliser une variable il faut d'abord la déclarer / utiliser le mot clé let / ensuite nommé la variable / insérer une variable initiale ex: (jean)

- Nous pouvons déclarer une autre variable qui ne contient aucune valeur (indéfinis)

- On peut assigné une valeur avec le égale (=)

- Nous pouvons déclarer une constante en utilisant le mot clé (const) / nommé ma constante et lui donné une valeur initiale ex: 12

- On peut multiplier et changer d'opérateur

- Il faut comprendre que dans une variable ou dans une constante on stock une donnée et cette donnée peux être réutilisé plus tard dans le code.

### ATTENTION

- On ne peut pas déclarer deux variables posse≠dent le même nom

- La constante / la valeur ne change pas

## NOMMER NOS VARIABLE

Nous pouvons nommez nos variables comme nous le souhaitons`

- Ìl est Préférable de **nommer les variables en anglais**.

- Un nom de variable peut être composé de chiffre allant de 0 à 9 de lettre allant de A à Z en minuscule et majuscule.

- Il est préférable de ne pas utiliser de chiffre pour commencer un nombre variable car cela va faire une erreur.

` Éviter d'avoir des noms de variables trop long

- **NB** = un nombre

- **str** = une stream

- **classeName** = le nom de la classe

### Le premier mot en minuscule et les autres mots en majuscule

- Tous les mots réservé à la syntaxe de JAVASCRIPT ne peuvent être nommé pour une variable.

`Il faut que le nom de la variable soit explicite`

## LES TYPES ET LES TYPAGES DYNAMIQUE

- Nous ne sommes pas obliger de préciser quel est le type de donnée que doit contenir une variable, le typage dynamique s'en chargera pour nous.

- Il est important de maitriser les donnés que l'on stock dans notre variable.

### LES DIFFERENT TYPES DE VARIABLES

Javascript est un langage **légèrement typer**.`

- Les Number

- **Les String** => chaine de caractères => **""** (une phrase / un mot)

- **Les Boolean** => permet de gérer les états (initialisé avec **true** puis changer la valeur avec **false**)

- **Les objects**

- **Array (les tableaux)**

- Déclarer la variable avec le mot **let** ex: let **age** = valeur 12

- Pour connaitre le type de votre variable il faut utiliser le mot clé : **typeof** pour indiquer la variable que l'on veut tester ou avoir.

- Nous avons un onglet console ou nous pouvons voir tout ce qui est sortie de notre SCRIPT.

- Séparer par une virgule dans la **console**

Faire attention au type de donné`

- Faire un **command R** pour recharger la page

## OPÉRATEURS ARITHMÉTIQUES

- Utilisé le signe `+` pour additionner deux nombres entre eux, stocker le résultat de cette opération dans une nouvelle variable ou constante.

- Assigné une nouvelle variable en utilisant l'opérateur =`

- Affiché la somme dans la console (la console permet d'afficher les erreurs).

- Faire un click droit sur **index.html** / open in browser.

- Pour soustraire deux nombres entre eux nous utilisons le signe `-

- Pour multiplier deux nombres entre eux on utilise le signe => `*` stocker le résultat de cette opération dans une variable que l'on nomme **produit**

- Pour la division on utilise signe `/ on nomme la variable *quotient*

_ Pour calculer une puissance il faut utiliser deux étoiles => `**`, stocker le résultat dans une variable appeler **result**.

- `Pour incrémenter` on utilise les signes (+1) `++` on utilise la variable **inc**(il ya une différence entre le *++* avant et après)

- Pour décrémenter on utilise les signes (-1) *--* on utilise la variable *dec*

## OPÉRATEURS DE COMPARAISON

- Les opérateurs de comparaison permettent de comparer deux éléments entre eux

### égalité simple

- Permet de tester la valeur et non le type

- On utilise 2 *==*

### inégalité simple

- Permet de testé la valeur

- On utilise un point d'éxlamation et un égale*!=*

- Le point d'exclamation permet de faire l'inverse de l'égalité simple

### égalité strict

- Permet de testé en type / genre et en nombre

- On utilise 3 *===*

### inégalité strict

- Permet de vérifié la correspondance en valeur et en type de deux variable

- On utilise un point d'exclamation et 2 égale *!==*

### strictement supérieur

- On utilise le signe supérieur *>*

### strictement inférieur

- On utilise le signe inférieur *<*

## OPÉRATEURS LOGIQUES

- IL Y'EN N'A 3 : -  ET LOGIQUE / OU LOGIQUE / NON LOGIQUE

### ET Logique => && (on peut ajouté plusieurs expression)

- Le *ET LOGIQUE* va nous permettre de créé des conditions

- Pour ajouté une deuxième expression on ajoute 2 =>*&&*

- Il faut que toutes les expressions soit égale a *true*

### OU Logique => 2pipe => *||* (en faisant option+maj+l sur mac)

### NON Logique => *!* = *true*

- Elle va nous donner *l'inverse* d'une valeur

- l'état ouvert c'est *true et 1* et l'état fermé c'est *false et 0*

- Pour avoir l'inverse de l'inverse on utilise 2 point d'exclamations *!!* = *false* cela permet de savoir si elle est défini

- Les deux ponts d'exclamations permet de tester 3 états d'une variable

-On utilise des parenthèses pour séparer l'expression

## OPÉRATION D'AFFECTATION

- Assigne une valeur a une variable

### afffectation simple

- ex: *nb=30;*

### affectation après addition

- ex: *nb += 3;* = nb = nb + 3

### affectation après soustraction

- ex: *nb -= 3;* = nb = nb - 3

### affectation après multiplication

- ex: nb*= 2;= nb = nb*2

### affectation après divison

- ex: *nb/=3;* = nb = nb / 3

### affectation du reste (% modulo)

- *nb%= 4;* = nb = nb % 4

# Note javascript 2

- On utllise la fonction *prompt* pour demander a l'utilisateur son âge.

- On écrit *prompt*(on entre du texte)

- Une fenêtre va être ouverte avec le message qu'on n'a indiqué et un champ ou l'on rentre la réponse du texte.

- Ensuite il faut récupérer la valeur que l'on n'a entré en créant une *constante*

- On peut utiliser la *constante (ex:age)* en l'affichant dans la *console*

- On recharge la page internet / inspecter /console / ajouté l'age / ok 

## On peut stocker le résultat soit dans une constante soit dans une variable

- On peut utiliser la fonction *alerte* qui contient l'*age*

- On click sur *ok* pour fermer l'alerte

- On affiche le type en utilisant *typeof*

- On peut convertir grace au *+* la *constante* 

## Il faut faire attention au valeur qui sont entré par l'utilisateur

- Il faut ajouter des vérifications avant toute conversion,

- On vérifie si la modification est un nombre

## Si dans la console s'affiche **NaN** c'est que ce n'est pas un nombre
