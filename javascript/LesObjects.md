> Sommaire :
> - [Déclarer un objet](#dclarer-un-objet)
> - [Accéder a l'objet courant dans une méthode](#accder-a-lobjet-courant-dans-une-mthode)
# Les Objets

## Déclarer un objet

> Un objet c'est la représentation de données sous forme de clé valeur, chaque caractéristique liée à un objet on n'appelle cela des propriétés et chaque actions liée à un objet on n'appelle cela des méthodes(des actions).

- Les objets javascript peuvent être mis en parallèle avec l'objet du téléphone.

- Une methode en javascript c'est l'équivalent d'une action dans la vie reel.

- On sépare les propriétés entre elle avec des virgules.

```javascript
// { key (propriété): value, key: value } ex:
let phone = {
    // propriétés
    name: 'Iphone',
    marque: 'Apple',
    largeur: 5,
    longueur: 5,
    
    //Méthodes (les actions)
    call: function (to) {
        console.log('call Sam');
    },
    function(to {
        console.log('send sms to' + to);
    }
        
};

console.log(phone);
console.log('marque : ', phone.marque);

phone.call();
phone.sendMesg('sam');
phone.sendMesg('Tom');
phone.sendMesg('jo');

let phone2 = {
    marque: 'samsung',
};

console.log(phone2.largeur);

phone2.largeur = 6;

console.log(phone2.largeur,phone2.marque);
```
 
- Pour créer un objet on utilise des accolades.

- Pour accéder a mon objet, je nomme mon objet puis je rajoute un point.

- Pour définir une méthode donc les actions qui serons défini.

- Le nom de la fonction sera le nom de la clé.

- **Une Fonction c'est également un objet**, ou il va y avoir **des propriétés**.

- On peut mettre le nom que l'on souhaite rajouter dans notre objet.

- **Il existe un autre moyen pour accéder au propriété** en utilisant la **syntax avec des [crochets]**, comme pour les tableaux mais cette fois donner **la clé**, ou **une variable** à laquelle je veux accéder.


> lien de la vidéo => https://www.youtube.com/watch?v=0tEc8hIxbr0&feature=youtu.be

----
# Accéder a l'objet courant dans une méthode
```javascript
// { key: value, key: value}
let student = {
    name: 'sam',
    class: 'Terminal',
    say: function (say){
        //"NAME : BONJOUR"
        console.log(this.name +':' + say);
    }
};

student.say('bonjour');
```
#### Example:
> Un étudient a des caracteristique il a : 
> 1. Un Nom
> 2. Une classe
> 3. Il peut parler (dire quelque chose)
> 4. La Fonction va afficher dans la console ce que le mot clé (this.name) contient, **this** fait occurrence à l'objet courant.

=> On accède à la propriété en utilisant le point (.) 

/ ! \ Bien donner le nom de la clé.
`````javascript
let student2 = {
    name: 'jean',
    class: 'seconde',
    say: function (say) {
        //"NAME : BONJOUR"
        console.log(this.name +':'+ say);
    },
    
};
student2.say(say:'bonjour'+ studentname)
student.say(say:'bonjour');
`````
- La fonction **say** va afficher dans la console : ce que l'étudient dit.

- Le **this** fait référence à l'objet courant, à l'objet dans laquelle la **fonction est appelé**.

/ ! \ Attention il faut accéder a des propriétés qui sont définis dans les objets.

- Il y a une méthode **saveClass :** qui prend en paramètre une classe et qui enregistre la classe dans mon **objet**.
  
- On peut afficher également **la class après le this**.

- Pour accéder a la propriété on utilise (this.name).
----
> Lien de la vidéo => [https://www.youtube.com/watch?v=D-NISDcBpv4&feature=youtu.be]
