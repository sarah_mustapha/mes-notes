> - Sommaire:
> - [Présentation nodejs](#presentation-nodejs)
> - [Présentation npm](#presentation-npm)
> - [Présentation typescrypt](#prsentation-typescript)
> - [Configurer un package nodejs](#configurer-un-package-nodejs)
> - [Basic types](#basic-types)
> - [Typer une fonction](#typer-un-faction)
> - [Enums](#enums)
> - [Tuples](#tuples)
> - [Literal types](#literal-types)
> - [Interface](#interface)
> - [Les class](#les-classes)
> - [Les paramètres des propriétés](#les-paramtres-des-proprits)
> - [Import export modules](#import-export-modules)
> - [Ts config](#ts-config)
----
# Presentation Nodejs

> **Nodejs c'est un moteur d'exécution du language javascript**.
> 
> Habituellement on **développe dans des fichiers javascript.**
> 
> Ses fichiers **javascript là et Html** sont chargé par **le navigateur**.
>
> **C'est le navigateur qui va exécuter le javascript sur le navigateur**.

- Grâce à **nodejs qui est un moteur d'exécution de javascript** on va pouvoir exécuter **le Javascript** avec **nodejs**.

- Ce qui nous donne maintenant accès au développement d'application **d'un pays coté serveur**.

> Grâce à **Nodejs** on peut exécuter du **javascript**.

- Nodejs est **basé sur le moteur V8 de google chrome**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-26%20à%2022.09.01.png)
  
- On se trouve sur le site, **lorsqu'on installe node.js**, nodejs va nous **installer notre package management Npm**. Donc c'est son gestionnaire de paquets.

- On tape sur notre **Terminal** => **node --version** entrer ensuite **npm --version** entrer.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-26%20à%2022.09.23.png)

- Ensuite on va avoir la version de **node.js** qui sera installé.

- On n'a **une documentation** ou on choisit la version dans laquelle on n'est. Et on n'a accès à une **api** coté serveur (Donc la c'est tous ce **qu'on peut faire avec node.js**).

> LIEN DE LA DOCUMENTATION => [https://nodejs.org/docs/latest-v13.x/api/]

- On peut **créer un serveur http ou HTTPS** avec **nodejs**, et tout **un tas de choses**

- On peut créer la **Crypto** etc..

> Cela c'est tous ce qui est possible de faire coter **navigateur**.
> 
> **Attention !** le code qui est exécuté par le serveur est exécutée que par le serveur **pas par le naviqateur**.
> 
> **Le serveur et le navigateur** du client de l'utilisateur, c'est **deux environnements d'exécution Javascript** qui sont différents.

> LIEN DE LA VIDÉO => [https://www.youtube.com/watch?v=8Ak786GELow&feature=youtu.be]

----

# Presentation npm

> **Npm c'est l'acronyme de notre package manager**, dans **l'environnement de node.js**, **npm** est **le gestionnaire de paquet**.

- **Un packet** c'est un programme qui est développé en **javascript** et qui est mis en avant sur **la plateforme mpm**, pour la communauté **javascript**.

> Les **npm** on peut installer des **packets** des **programmes** des **librairies** ou des **freamworks** qui sont mis sur le **site npm**, on peut tous **les installer, on peut les gérer**, on peut **créer nous même des packets** et ensuite **on peut publier nos propres paquet**.

- Actuellement sur la plateforme **npm** on peut **trouver plus d'un million de paquets**, parmi ces paquets, on connait **react*, **angular**, **vujs**, et d'autre plus petit paquet qui **permette de simplifier le développement, la manipulation de petits objets**, comme exemple => **les dates**.

- Il ya d'autre packets qui permettent de faire **de la 3d**.

> Donc **npm c'est simplement un outil de distribution du code**, c'est très utilisé par la **communauté de l'opérateur javascript**, car on **peut récupérer le code, le travaille d'autre personnes**, qui nous simplifient nous notre travail.

- Par la suite on va très souvent utiliser **npm**
> Lien du site => [https://www.npmjs.com]
> Lien de la vidéo => [https://www.youtube.com/watch?v=N-RpiBQF4Zg&feature=youtu.be]
----
# Présentation typescript

> TypeScript c'est **un langage de programmation** qui est développé par **microsoft**, qui est gratuit sur **open source**, qui va **rajouter un ensemble de fonctionnalités à javascript**.

> Donc des **fonctionnalités sur le typage**, par rapport au class etc..

- L'objectif de **typeScript** c'est d'améliorer le **code**, **la sécurité du code** et d'avoir **un code qui est plus lisible et plus propre**.

> Voici la documentation de typeScript => [https://www.typescriptlang.org/docs]

> On n'a la documentation pour l'installer => [https://www.typescriptlang.org]

- On n'a la documentation pour l'installer
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-26%20à%2022.20.52.png)

> On l'installe de manière globale pour y avoir accès, parmi tous nos fichier et projets. (On pourrait très bien l'installer sans le **-g**), pour l'installer dans **un seul projet**.

- Donc ce qu'on n'a installé c'est **le transpileur**, car lorsqu'on développe en **typeScript**, le navigateur ou le serveur ne peux **pas lire du typescript**.

- Le typescript c'est basé sur **javascript** et pour lire un **code** qui est développé en **typescript** il faut le **transpiler en javascript**.

> **Le transpileur c'est un logiciel qui va permettre de traduire un code vers un code civil**.

- Dans notre cas **on développe en typescript** et on va traduire le code **typescript** en **javascript** et pour cela **on doit installer le **transpileur**.

- Pour utiliser ce **Transpileur** on peut directement utiliser le **tsc**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-26%20à%2022.35.11.png)
  
- Donc la je suis dans mon projet **typescript**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-26%20à%2022.36.48.png)
  
- J'ai créé **un fichier typescript** qui finis pas **.ts**.

- Ce fichier **typescript** s'appelle **index.ts**.

- Je vais pouvoir développer dans mon fichier **index.ts** en **javascript**.

````typescript
let var1: boolean = "ok";
````
- Parmi l'une des fonctionalité qui est ajouté **avec typscript** c'est le fait de **typer** des **variables**.

- Dans mon cas là j'ai **typer typescript**.

- J'ai dit que **la variable est un boolean** donc je vais lui donner **un boolean**
````typescript
let var1: boolean = true;
````
- Ensuite ja vais utiliser **le transpileur** pour le traduire en **javascript**, pour cela **j'utilise la commande **tsc** et je lui indique qu'elle est le fichier que **je veux transpiler** (traduire).

- Dans mon cas je veux le traduire en **ts**, en **index.ts**
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-26%20à%2022.49.49.png)
  
- Donc créer la version **javascipt** de mon code écrit en **typescript**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-26%20à%2022.52.40.png)
  
- On constate qu'il m'a bien créé mon **Var1** et bien même la valeur **dont j'ai souhaité**.

> À retenir que **typescript** est un language de programmation mais qui va être ensuite treaduir en **javascript**.

- Car **typescript**, nous permet de **racheter** certaines **fonctionnalités** qui sont très importante, qui manque dans le **javascript**.

> On développe en *typescript** mais il faut **transpiler** en **javascript**.

- L'intérêt du **transpilateur** c'est qu'on peut **cibler des versions**.

- lorsqu'on n'a parlé de **ecmascript** qui permet de **définir certaines règles**, la programmation **en javascript**.

- le fait qu'on va utiliser **let**, ces notions là **évoluent** dans le temps.

- Lorsqu'on va **Transpiler notre code** on peut cibler quelle version on veut avoir **à la fin**en javascript.

- L'avantage c'est **le code qui va être créé apres la traduction en **fonction de la cible que l'on souhaite** on peut être **compatible avec certains navigateurs trop anciens*** externe.

> Donc là on **développe avec un code récent** et lorsqu'on va le traduire on peut le traduire dans un code qui va être **lisible** même par des navigateurs qui ne sont **pas à jour**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-27%20à%2012.00.33.png)

> LIEN DE LA VIDÉO => [https://www.youtube.com/watch?v=a114c2kEgLU&feature=youtu.be]
----
# Configurer un package nodejs

> On va créer notre premier paquet et par la même occasion comprendre comment utiliser le **gestionnaire de paquet du nodejs**

- J'ai créé un dossier qui comprend rien pour l'instant.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-27%20à%2012.06.54.png)
  
- Je vais lui insérer un fichier **index.html**.

> Pour l'instant cela est **projet dans mon dossier script**, je vais avoir une **page html**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-27%20à%2012.09.46.png)

- Pour **initialiser un paquet**, On rentre dans notre terminal et on va utiliser la commande **npm init**.

- Il va nous demander **les informations sur notre paquet**, donc le nom ensuite une version qui est l'auteur etc..
  ![imh.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-27%20à%2012.14.28.png)
  
- Il m'a rajouté un **package.json** qui contient toutes **les informations relative à mon paquet**
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-27%20à%2012.17.29.png)
  
- Donc **il y a le nom de mon paquet, la version de mon paquet etc..**

> Grâce à **npm** je vais pouvoir mettre à **jour mon paquet**, je vais pouvoir le publier également.

- Si je veux je publie mon paquet et à l'intérieur il y aura juste **index.html** et **package.json** et rien d'autre.

- Dans le cas ou **je développe un site, car actuellement je développe un site**, j'ai besoin d'un site pour **développer le site d'une librairie**.

- Avant j'aimerais aller chercher une **librairie** qui me permet de **créer des serveurs**.

- Peux importe la librairie, je tape **Express** sur la barre de recherche dans **le site npm**.

- Donc ici j'ai la commande pour **installer l'express npm**.

- Donc la je suis en train de récupérer le code de la **librairie express**.

![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-27%20à%2012.25.38.png)
  
- **J'ouvre mon terminal** dans mon **dossier** du projet **Script**, je vais exécuter la commande.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-27%20à%2012.29.08.png)
  
> Donc la je suis en train de **récupérer le code de la librairie express**.

- Maintenant ce code là est **accessible dans mon projet**
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-27%20à%2012.32.00.png)
  
- On voit qu'il m'a ajouté **deux éléments à mon dossier**.

- On voit que j'ai **dependencies**, **dependencies** va **lister toutes les dépendances** de mon paquet. Donc si **je suis en train de développer un serveur** (libraire etc..) que je veux publier en **open source** pour tout le monde, il va **lister toutes les dépendances de mon serveur**.

> Donc mon projet **dépend** d'autre projet. **Il dépend du projet express pour bien fonctionner.** Donc ici il le **liste**.

- Mon **package.lock.json** va **lister toutes les dépendances**de **dépendances**.

> Donc là j'ai installé **express**, mais express **c'est un paquet** qui a **lui-même des dépendances** lister à l'intérieur.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-27%20à%2020.00.11.png)

> Donc quand j'installe **express**, j'installe en faite toutes **les dépendances du paquet**.

- Lorsque je vais publier mon paquet à moi, les gens qui vont installer **mon paquet** ils vont installer mon paquet et **les dépendances qui sont indiqué ici dans cet objet**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-27%20à%2020.04.40.png)
  
> C'est important de bien **mettre à jour la clé de dependencies**, car si notre **projet dépend d'un autre projet** il faut qu'on installe d'autre projets aussi.

- Donc là c'est ce qu'on retrouve ici.

> On n'a installé le **package express qui lui-même a des dépendances**, donc on n'a installé toutes les dépendances.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-27%20à%2020.08.24.png)

> Ces deux fichiers sont **important lorsque vous partagez du code, vous partagez votre projet avec d'autre collègues**.
> 
> Parceque lorsqu'on va installer les dépendances de votre projet **il faut toutes les dépendances**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-27%20à%2020.12.17.png)

-**Toutes les dépendances sont listées dans ces deux fichers**
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-27%20à%2020.04.40.png)
---
- Dans le dossier **node-modules** on retrouve toutes **les dépendances**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-27%20à%2020.19.34.png)
  
> Donc nous on n'a installé le projet **express**, mais on voit que ce projet qui est **lui-même un paquet** il a des dépendances.

- Si je vais sur la **clé dependencies** on voit qu'il a des dépendances et il dépend de tous ces projets si desous.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-27%20à%2020.23.42.png)
  
> Donc lorsqu'on installe **exprès on va également installer toutes ses dépendances**, c'est ce qu'on voit si **dessus**

> Cela nous permet de toujours avoir **un projet à jour et un projet qui fonctionne**.

- Car ici on n'a **listé nos propres dépendance**
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-27%20à%2020.04.40.png)
  
- ET ici on n'a **toutes les informations sur nos dépendances de nos dépendances**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-27%20à%2020.08.24.png)
  
> Il faut se rappeler que le dossier **node_modules** on ne le partage jamais car on le supprime toujours, ce qu'on partage ce son ces deux fichiers **package.json et package-lock.json** car pon n'a toutes les informations sur notre projet.

- Lorsque je vais partager ces trois fichiers donc **mon code, les dépendances et les informations du paquet**, les développeurs qui vont récupérer ce code là ils auront juste à tapper sur leurs terminal **npm install**, grâce à cela il va installer toutes les dépendances du projet qui vont lui créer un dossier **node_modules** c'est pour cela qu'on ne le partage pas car lorsqu'on installe les dépendances ils se font automatiquement.

- Si on partage toutes les dépendances, tous les fichiers ça serai beaucoup trop volumineux.

> On partage seulement les informations qu'il va récupérer sur **le site de npm** directement **le code**.

- Ici on n'a **une clé script** qui nous permet d'écrire ici **des commandes**, par exemple si on veut **build**de notre projet, on va voir un script **built** qui va exécuter par exemple **npm build index.html**.

- Lorsqu'on va exécuter notre commande **build** cela va **exécuter** cette commande après.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-01-27%20à%2020.46.18.png)
  
- Pour exécuter la commande build que j'ai créé, je tpe dans mon terminal **npm run(pour un script) build**.

> Il faut ce souvenir des clés **scripts et dependencies**, ainsi que des fichiers **package.json et package-lock.json** et du dossier **nodes-modules** qui contient toutes les dépendances du code dans lequel on dépend.

> LIEN DE LA VIDÉO =>[https://www.youtube.com/watch?v=s3WTu1jJgIc&feature=youtu.be]
----
# Basic types

> Lorsque l'on déclare une variable avec une valeur une valeur initiale, l'interpréteur va assigner **le type de la valeur à la variable**, c'est ce qu'on appelle un **typage dynamique**.
````typescript
let  a = 3; // Dynamic type
````
- Donc sa c'est en **javascript**, en javascript on n'a un typage dynamique.

- Lorsqu'on insère une **valeur à un variable**, la variable va prendre **le type de sa valeur**.

- Donc si je demande quelle est **le type de la variable a cela m'indique 3**.
````typescript
let  a = 3; // Dynamic type
````
- Lorsque qu je demande le type de la variable a il va me dire que cela est une chaine de caractère.
````typescript
a = "2";   // la variable a est une chaine de caractère
````
> Car **l'interpréteur dynamiquement va changer le type de la variable à chaque fois qu'on va lui donner une valeur**. Cela peut poser problème, dans la précédente video de cohérence au niveau des valeurs insérés dans les variables.

- POUR REMÉDIER À SA ON PEUT DÉCLARER UNE VARIABLE DE LA MÊME MANIÈRE QUE JAVASCRIPT AVEC :

- Je vais dire à cette variable qu'elle peut contenir que des nombres, **que des valeurs qui sont de type nombre** donc j'ai typé mon **a**.
````typescript
let a: number = 3;
````
- Si j'essaie de lui **insérer une chaine de caractère** je vais avoir une **erreur**
````typescript
let a:  number = 3;

a = '4';  //je vais avoir une erreur
````
- Pour remédier à ça il faut que je lui insère un nombre car la variable **a** est maintenant typer par **number**.
````typescript
let a:  number = 3;

a = 4; 
````
> Cela est le premier type basique

- JE PEUX ÉGALEMENT TYPER DES CHAINES DE CARACTÈRES AVEC LE MOT CLÉ **STRING**.

-Donc la je ne lui ai pas inséré de valeur.
````typescript
let str: string;
````
- Ici il faut absolument que je lui insère une **chaine de caractère**.
````typescript
let str: string;
str = "ok";   // il faut absolument une chaine de caractère
````
- Le troisième basic sera le **boolean**.
```typescript
let bool: boolean = true;
```
- Si je veux **assigner une nouvelle valeur** il faut absolument que ce **soit un boolean**.
````typescript
let bool: booleab = true;
bool = false;
````
> Ce que je vais pouvoir faire à la place des nombres c'est d'accepter que ma **variable nb** est en valeur soit un nombre ou **une chaine de caractère** et pour cela je rajoute **un pipe**.

````typescript
let nb: number | string = 3;
````
- Cela me dit que **le type de ma variable** est soit **un nombre soit une sting**.

> Une variable peut accepter plusieurs types, Ca peut être soit un nombre, un tableau soit un boolean, 

- Ici **je peux enchainer les conditions et une variable peut accepter plusieurs types**.
```typescript
let nb: [] | string | Boolean = 3;
```
- Donc maintenant si je change le type de ma variable je peux insérer **un tableau**, je peux également insérer **une chaine de caractère**.
````typescript
let nb: [] | string | Boolean = 3;
nb = []; // ou une chaîne de caractère
````
> **Par contre je ne peux pas insérer un nombre**, car il ne fait pas partie des types accepté => **let nb: [] | string | Boolean = 3;**
> 
> Donc ça c'est le moyen de rajouter plusieurs types à une variable.
---
- **Array** c'est une variable qui va **contenir un tableau**.

- Je peux ajouter également un type de **Tableau** par exemple.
```typescript
let arr: [];
arr = [];
```
- Ici **je peux typer** le contenu du tableau, pour cela **j'indique le type** et dire que ce type là sera un tableau.
````typescript
let arr: number[];   //dabord le type (number) et ensuite les crochets pour indiquer que c'est un tableau.
arr = [];
````
- **Si je push une chaîne de caractère dans mon tableau je vais avoir une erreur**.
````typescript
let arr: number[];   //dabord le type (number) et ensuite les crochets pour indiquer que c'est un tableau.
arr = [];
arr.push("ok");   // erreur si je push une chaîne de caractère.
````
- Car mon tableau est un **tableau de nombre**, ici il faut que **je push en nombre**.
````typescript
let arr: number[];   //dabord le type (number) et ensuite les crochets pour indiquer que c'est un tableau.
arr = [];
arr.push("3"); //=> ici il faut que je push un nombre.
````

> Je peux **additionner plusieurs tableaux de chaîne de caractères** dans mon **type de variable**.

- Ici je vais devoir **push un tableau de chaine de caractère**.

> Je pourrais mettre **plusieurs tableaux à l'infini**
````typescript
let arr: string[][]; // je lui dit que c'est un tableau de chaine de caractère/ ou de nombre etc..
arr = [];
arr.push(["ok"]);
````
> On peut faire **la même chose avec un tableau de boolean**

- Ici c'est soit **true ou false**
````typescript
let arr: boolean[][]; // je lui dit que c'est un tableau de boolean.
arr = [];
arr.push(true);
````
----
> **Je peux typer un objet**.

- Pour **typer** un **objet** => **Let: objet {};**

- J'indique à ma variable que cela sera **un objet**.

> **Donc objet ne peut pas être un nombre**.

```typescript
let obj: {};    // je lui indique que cela sera un objet.
```
> Généralement on ne dit **jamais que c'est un objet vide**. => {}

- On va dire que c'est un objet qui va devoir avoir ces propriétés, **les propriétés de cet objet là on peut les typer également**

- On peut dire que mon objet **aura une propriété name** et qui va être de **type string**.

- mon objet ne peut pas **être vide** il lui faut **la valeur name**, il faut également que la propriété soit **une chaine caractère**.
````typescript
let obj: {ok: string};
obj = {name};   // ici sa ne peut pas être un objet vide
````
> Je peux ajouter **d'autre propriétés et dire que cette propriété sera un nombre**.
````typescript
let obj: {name: string, age: number};
obj = {name: "sarah", age: 27};
````
> On peut également dire que notre propriété est **optionnelle**, **mon objet n'est pas obligé d'avoir la propriété age**

- Donc si je retire la **propriété age** cela **fonctionne toujours**.
````typescript
let obj: {
    name: string,
  , age: number
};
obj = {name: "sarah"};
````
- Je peux dire que **age** est soit un nombre, soit une **chaine de caractère**.

- Si je remet **la propriété age** dans **mon objet**, mais que cette fois ce n'est plus **un nombre mais une chaîne de caractère** cela fonctionnera toujours.
````typescript
let obj: {
    name: string,
    age?: number | string
};
obj = {name: "sarah"}, age: "12";
````
- Le typage on l'a vu récemment, c'est un **typage basic**.

> Lien de la vidéo => [https://www.youtube.com/watch?v=WlyIWGLzHo4&feature=youtu.be]
---
# Typer une fonction

> Pour typer une fonction **il faut d'abord déclarer une fonction nommée**.

- Je vais appeler la fonction **multipy**, qui va prendre en paramètre un **nombre1** et un **nombre 2**.

````typescript
function multipy(nb1, nb2) {
    
}
````
- Je peux avoir **la même fonction anonyme**, cette fois.

- **Une fonction anonyme ne prend pas de nom**, AVEC UN **NOMBRE 1** en premier paramètre et **un nombre 2** en deuxième paramètre.
````typescript
function multiply(nb1, nb2) {
    
};

let multiply = function (nb1, nb2) {
    
};
````
- Donc la je peux très bien appeler **multiply** avec des paramètres, et **multiply 2 avec des paramètres**.

> Donc cela ce sont **deux moyens de typer des fonctions**.
```typescript
function multiply(nb1, nb2) {    // 2 moyens de typer des fonctions
    
};

let multiply = function (nb1, nb2) {
    
};

multiply(nb1, nb2);
multiply2(nb1, nb2);
```
> Ce qu'il va nous intéressé ici c'est **qu'on peut typer ces deux fonctions là**=>**function multiply(nb1: number, nb2: number)**, c'est **l'avantage de type script**.

- **Ici on peut également passer les arguments à la fonction.**
````typescript
function multiply(nb1: number, nb2: number) {    // 2 moyens de typer des fonctions
    
};

let multiply2 = function (nb1, nb2) {
    
};

multiply(nb1, nb2);
multiply2(nb1, nb2);
````
- Pareille pour les **fonctions anonymes**.
```typescript
function multiply(nb1: number, nb2: number) {    // 2 moyens de typer des fonctions
    
};

let multiply2= function (nb1: number, nb2:number) {  // fonction anonymes
    
};

multiply(nb1, nb2);
multiply2(nb1, nb2);
```
- La raison pour laquelle je vous ai mis **deux manières d'écrire une fonction** c'est qu'on **déclare une variable** mais on peut aussi **typer une variable**.

- EX: On veut que **la variable ne prenne qu'une fonction**.

- Donc là on lui dit que **la fonction va prendre un premier argument qui sera un nombre** et aussi en **deuxième argument**.
```typescript
let multiply2: (nb1: number, nb2: number) => void = function (nb1: number, nb2:number) {
```
- Donc là je pourrais laisser cela comme sa => **let multiply: (nb1: number, nb2: number) => {} = function (nb1: number, nb2:number) {**, ou l'enlever => = **function (nb1, nb2)** .

- Car le compilateur de **type script** va automatiquement déterminer **que c'est bien deux nombres**.

> Donc ce que je lui dit c'est que les paramètres, **let multiply: (nb1: number, nb2: number)** définit **le type** des paramètres.

- La fonction prend **deux paramètres** et **ces deux paramètres sont des nombres**. Ensuite je lui indique qu'est-ce que ma **fonction retourne** grace a (=>).

- Dans mon cas ma fonction **ne retourne rien** car je n'ai **aucune donnée**.
```typescript
function multiply(nb1: number, nb2: number) {    // 2 moyens de typer des fonctions
    
};

let multiply2:(nb1: number, nb2; number) => void = function (nb1, nb2) {
};

multiply(nb1, nb2);
multiply2(nb1, nb2);
```
- Dans le cas ou je veux retourner **un nomnbre**, exemple **3*4**

- Ici je ne retourne plus **void** qui veut dire **rien**, je retourne **un nombre** qui sera acceptée dans **cette variable** => **multiply** doit être **une fonction** qui prend **deux paramètres** qui sont **Des nombres** qui **retourne** **un nombre**(=> number).

- Pour typer le retour de ma fonction, il faut que j'indique **apres les parenthèses** **: number** car je veux que cette fonction me retourne un nombre./ ou une **String, un boolean, un tableau** en fonction de **la logique** donc la c'est à nous de décider.
````typescript
function multiply(nb1: number, nb2: number): number {    // Ici je dit que ces deux paramètres sont des nombres et qu'il me retourne un nombre
    return 3 * 4;                                  // pour typer le retour
};

let multiply2:(nb1: number, nb2; number) => number = function (nb1, nb2) {
    return 3 * 4;
};

multiply(nb1, nb2);
multiply2(nb1, nb2);
````
- Donc la fonction **basique** c'est cela .
````typescript
````typescript
function multiply(nb1: number, nb2: number): number {    // Ici je dit que ces deux paramètres sont des nombres et qu'il me retourne un nombre
    return nb1 * nb2;                                  // pour typer le retour
};

let multiply2:(nb1: number, nb2; number) => number = function (nb1, nb2) {
    return nb1 * nb2;
};

multiply(nb1, nb2);
multiply2(nb1, nb2);
````
> ON voit qu'on peut typer une variable avec **une signature de fonction**.

- Donc le je vais supprimer la valeur,**let multiply2:(nb1: number, nb2; number) => number;** cette ligne ne fait **que typer**, elle déclare une variable **multiply2**, elle dit que cette variable elle doit retourner une **fonction qui retourne un nombre**

- Donc là je sais que cette **variable à 2 nombres en paramètres, et elle retourne un nombre**.
````typescript
let multiply2:(nb1: number, nb2; number) => number;
};               // signature de la fonction

multiply(nb1, nb2);
multiply2(nb1, nb2);`
`````
- Je peux directement savoir qu'est-ce que cette fonction va faire car lorsque je vais l'appeler elle ne contient aucune valeur donc **la variable sera indefined**.

- Mais lorsque j'utiliserais cette fonction sachant que **j'ai la signature exacte de cette fonction** et j'ai exactement ce qu'elle retourne, je sais à quoi sert cette fonction.

> Je ne peux pas insérer a **multiply 2** un nombre, cela doit être **une fonction**

- Ici il faut que je lui rajoute une **fonction**, **nb1** et **nb2** qui sont **des nombres**.

- Il faut que je retourne ici **un nombre** et pas une chaîne de caractère ou un boolean, et dans ce cas là on n'est bon.
````typescript
````typescript
function multiply(nb1: number, nb2: number): number {    // Ici je dit que ces deux paramètres sont des nombres et qu'il me retourne un nombre
    return nb1 * nb2;                                  // pour typer le retour
};

let multiply2:(nb1: number, nb2; number) => number;
    
    multiply2 = function (nb1, nb2) {
        return nb1 * nb2;
    };

multiply(nb1, nb2);
multiply2(nb1, nb2);
````
> Étant donné que je peux typer les arguments, je peux également dire que **l'argument est optionnel** en utilisant **?**, qui veut dire que **je ne suis pas obligé d'insérer un deuxième argument**.
````typescript
````typescript
function multiply(nb1: number, nb2?: number): number {    // je dit que l'argument est optionnelle
    return nb1 * nb2;                                 
};
````
- Deuxième éxemple :

> On déclare une fonction **createUser** qui prend en paramètre **son nom, age et adresse**.

1. je veux que son nom et son adresse **soit une chaine de caractère**
2. je veux que son âge **soit un nombre**
3. je veux que son adresse **soit une chaine de caractère**
````typescript
function createUser(name: string, age: number, adress: string);
````
- Je veux dire que son adresse **soit optionnelle**, je ne suis pas obligé d'insérer l'adresse **lorsque je vais créer un utilisateur**.

- Cette fonction va me retourner un nouvel utilisateur qui **Est un objet** qui aura:

1.la propriété **name** => argument name / name=> argument age
1.la propriété **age** => argument name
1.la propriété **adresse** => argument age

````typescript
function createUser(name: string, age: number, adress?: string); {    // ? => optionnelle

    return {
        name: name,
        age: age,
        adress
      
    };
};
````
- Je rappelle ici que les variables âge et name on les memes nom que les propriétés dans la fonction, donc on peut **réduire cela comme si dessous**.
````typescript
function createUser(name: string, age: number, adress?: string); {    // ? => optionnelle

    return {
        name,
        age,
        adress
      
    };
};
````
> Si j'invoque ma fonction **createUser**, il faut que je donne **une chaîne de caractère en premier** argument, un **nombre en second** et une **chaine de caractère en troisième (optionnelle)**.
````typescript
function createUser(name: string, age: number, adress?: string); {    // ? => optionnelle

    return {
        name,
        age,
        adress
      
    };
};
    
createUser(name:"sarah", age:24, adress:"rue d'alsace");
````
> **Je peux également typer** ma fonction.
- Je veux typer **un objet** qui à les **mêmes types** que ce qui est passé en **Argument**.
````typescript
function createUser(name: string, age: number, adress?: string):
        {name: string, age: number, adress: string} {    // : pour typer

    return {
        name,
        age,
        adress
      
    };
};
    
createUser(name:"sarah", age:24, adress:"rue d'alsace");
````
- Je peux ajouter **let fn: () =>** qui dit ce qu'elle retourne en **ouvrant les parenthèses**.

- Ici je peux **typer les arguments qu'elle va recevoir**

````typescript
function createUser(name: string, age: number, adress?: string):
        {name: string, age: number, adress: string} {    // : pour typer

    return {
        name,
        age,
        adress
      
    };
};
let fn : (nb: string) => boolean | number;    // je type les arguments qu'elle va recevoir

createUser(name:"sarah", age:24, adress:"rue d'alsace");

````
> Lien de la vidéo =>[https://www.youtube.com/watch?v=0iGLn7h73Hs&feature=youtu.be]
----
# Enums

> Type script rajoute cette fonctionnalité qui n'est pas une fonctionnalité de **javascript** On retrouve des **enumérateurs** en **my sql** en **php** en *c++** et pleins d'autre languages, mais pas du tout en **javascript**.

- Pour créer un **enumérateur** on utilise le mot clé **enum**.

- Ensuite on donne le **nom** de notre enumérateur. Qui sera **Direction**.

- On va **enumérer** une liste **de constantes**.

- On va avoir une constante **TOP** qui aura la **touche** de la valeur du clavier et 4 autres valeurs.
````typescript
enum Direction {
    TOP = "123",
    BOTTOM = "321",
    RIGHT = "234",
    LEFT = "543",
}
````
- Donc là on n'a créé un type qu'on n'a appelé **Direction** qui contient plusieurs **constante de valeur** et grâce à ce **Type** là on peut **Typer** une **valeur**.

- On va créer une valeur **userDirection**. (Pour **Typer c'est :**), c'est la même syntaxe que si nous avions utilisé **number** à la place de **direction**.
````typescript
enum Direction {
    TOP = "123",
    BOTTOM = "321",
    RIGHT = "234",
    LEFT = "543",
}
let userDirection: Direction;   // pour typer on utilise :
````
> POUR typer une **Variable** on utilise **:**.
>
> On vient de créer un **type** (Direction) et on n'a dit que c'est une liste **De constante**(enum) donc on n'a **énuméré de constantes** qui on **Des valeurs**, et on dit que cette **Variable** (userDirection) ne peut avoir en **Valeur**qu'une des valeurs de notre **énumérateur** puisqu'on le type avec notre **énumérateur**.

- Donc là si j'essaie de lui passer 12 j'aurais une erreur ici. Je peux l'essayer pour cela je vais ouvrir le **Terminal**.

- Je vais lancer le **Transpilateur** pour cela **C'est tsc** et j'indique le fichier que je **Veux transpiler** en **javascript** qui est **index.ts**.

- LE **Terminal** m'indique bien que le nombre 12 ne peux pas être **insérée dans ma variable**, car ma **Variable** attend une valeur de **Type Direction**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-02-12%20à%2009.39.06.png)
````typescript
enum Direction {
    TOP = "123",
    BOTTOM = "321",
    RIGHT = "234",
    LEFT = "543",
}
let userDirection: Direction = 12;   //erreur
````
- POUR LUI DONNER CE QU'IL ATTEND. Je ferme mon **Terminal** à la place de lui donner **12** je vais lui donner une **des valeurs de mon énumérateur**(Direction.TOP), pour accéder avec une **valeur** je vais l'ajouter avec **le point(.)**
````typescript
enum Direction {
    TOP = "123",
    BOTTOM = "321",
    RIGHT = "234",
    LEFT = "543",
}
let userDirection: Direction = Direction.TOP; 
````
- Donc là tous fonctionne.

- Je vais ouvrir le **terminal** pour téster et je relance la commande **Tsc index.ts**, je n'ai aucune erreur c'est que tous fonctionne.

- JE teste en affichant dans la console **userDirection**.
````typescript
enum Direction {
    TOP = "123",
    BOTTOM = "321",
    RIGHT = "234",
    LEFT = "543",
}
let userDirection: Direction = Direction.TOP;

console.log(userDirection);
````
- Pour exécuter le **Fichier index.js** ce,que je peux faire c'est de modifier **mon contenu** dans mon **terminal** et je relance **la transpilation**.

- Je peux ouvrir **l'index.js** on voit bien que le code à été transformé en **javascript natif** avec le **console.log** rajouté.

- Je peux exécuter **Mon fichier index.js** pour cela je vais utiliser **node**. Je rappelle que **node** est un **moteur d'exécution de javascript**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-02-12%20à%2009.52.11.png)
 
- Donc la je lui dis **Node exécute le fichier index.js**, il l'exécute et m'affiche dans ma console **userDirection**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-02-12%20à%2009.54.08.png)
  
- Le **Terminal** m'affiche **123** car la **Constante TOP**(Direction.TOP) vaut la **chaîne de caractère 123**.

- JE veux que cette **variable**(userDirection) ai comme valeur seulement **une des valeurs** que j'ai **enuméré** dans **enum Direction**.

- Donc à la place de mettre 123 ici et de faire des changements un peux partout dans notre code on n'a qu'un seul endroit ou on maitrise la valeur et on utilise le **Mot clé**
````typescript
enum Direction {
    TOP = "123",
    BOTTOM = "321",
    RIGHT = "234",
    LEFT = "543",
}
let userDirection: Direction = Direction.TOP;

userDirection = "123";  // donc à la place de mettre 123 ici

console.log(userDirection);
````
- Donc même si c'est la même valeur **userDirection = "123"** ce n'est pas accepté car il attend **une des constantes**. IL faut utiliser **Direction.TOP** ou **.right etc..**
````typescript
enum Direction {
    TOP = "123",
    BOTTOM = "321",
    RIGHT = "234",
    LEFT = "543",
}
let userDirection: Direction = Direction.TOP;

userDirection = Direction.TOP; 

console.log(userDirection);
````
- Donc en faisant cela on **peut garder Direction.TOP** à 100 endroits dans notre code et on peut garder **la valeur**.

- Si on veut **Changer la touche** qui nous permet de **monter vers le haut **on n'a juste à changer le code de la touche** exemple: **TOP = "245"**.

> DONC ON PEUT AJOUTER UNE CONDITION, es ce que **userDirection** est **Strictement égale** à **Direction.TOP** si c'est le cas alors j'affiche dans la console**tu montes**
 ````typescript
enum Direction {
TOP = "123",
BOTTOM = "321",
RIGHT = "234",
LEFT = "543",
}
let userDirection: Direction = Direction.TOP;
 // on supprime une des deux lignes puisqu'on insère la même valeur
userDirection = Direction.TOP;

if (userDirection === Direction.TOP) {
    console.log('tu montes');
}

console.log(userDirection);
````
- Donc la je teste **les correspondences de valeur**. Je relance le **Transpileur** **tsc index.ts** et je vais **exécuter** le **fichier index.js**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-02-12%20à%2010.17.11.png)
````typescript
enum Direction {
TOP = "123",
BOTTOM = "321",
RIGHT = "234",
LEFT = "543",
}
let userDirection: Direction = Direction.TOP;

if (userDirection === Direction.TOP) {   // cette conditioon est juste
    console.log('tu montes');
}

console.log(userDirection);
````
- Mon **index.js** est bien à jour, maintenant il **m'affiche bien tu montes**. Car **user Direction** est bien égale à la valeur de la **constant(enumérateur  Direction.TOP**.

> Je ne suis pas obligé de mettre des valeurs dans **mon enumérateur**. Par défaut **la première propriété aura la valeur 0**
````typescript
enum Direction {
TOP,
BOTTOM, 
RIGHT, 
LEFT, 
}
let userDirection: Direction = Direction.TOP;

console.log(userDirection, Direction£.TOP);
````
- Donc il faut que je **retranspille** mon **fichier index.ts** et que je **R'exécute** mon fichier **index.js** générés dans la **transpilation** . On vois bien dans mon **Terminal** que **Direction.TOP** est bien égale à 0.

- ENSUITE TOUTE LES VALEURS DE MON ENUMÉRATEUR VONT ÊTRE **INCRÉMENTÉ** DONC.
````typescript
enum Direction {
TOP,   //0
BOTTOM,  //1
RIGHT, //2
LEFT, //3
}
let userDirection: Direction = Direction.TOP;

console.log(userDirection, Direction£.TOP);
````
- Ce que je peux faire c'est de donner **une valeur par défaut** et si je n'est pas de **Valeur pour les autres elles seront incrémenté de 1**
````typescript
enum Direction {
TOP = 100   //0
BOTTOM,  //101
RIGHT, //102
LEFT, //103
}
let userDirection: Direction = Direction.TOP;

console.log(userDirection, Direction£.TOP);
````
- Donc si je **Transpille mon index.ts** dans mon **terminal** puis j'exécute mon **index.ts** généré. Et si je change la **valeur** de **userDirection** et que je lui indique que la direction **sera LEFT**.

> RAPPEL IL FAUT TOUJOURS **TRANSPILLER**
````typescript
enum Direction {
TOP = 100   //0
BOTTOM,  //101
RIGHT, //102
LEFT, //103
}
let userDirection: Direction = Direction.LEFT;

console.log(userDirection, Direction.LEFT);
````
- ON VOIT DANS **le terminal** que **userDirection** la valeur est **103**.

> C'est assez **utile** si on veut gérer à **un seul endroit** les données et s'assurer que **une variable** ne possède que **une des données présentes dans notre enumérateur**.

> Lien de la video =>[https://www.youtube.com/watch?v=o__NKdLOq8g&feature=youtu.be].
----
# Tuples
> Les **tuples** permettent de **Typer une variable** avec un tableau **D'élément finis et commis**.

- Ex: un **tableau avec deux éléments** 

.1 Le **premier élément va être un nombre**
.2 Le **second élément va être une chaîne de caractère**
````typescript
let arr [number, strng];
````
- Donc dans **la valeur** il faut que je lui **indique** que le **premier élément va être un nombre** et le **Second élément** une **Chaîne de caractère**.
````typescript
let arr [number, strng] = [12, "ok"];
````
> Il ne peut pas avoir d'autre éléments, sinon j'aurais une **Erreur**.

- Pour plus d'information **Consulter Typescriptlang.org**.

> Lien de la vidéo =>[https://www.youtube.com/watch?v=_p4Jn5tDNSU&feature=youtu.be]

- On peut **Définir ce que l'on veut**, il peut avoir plus de **tableau** ou moins.
````typescript
let arr: [number[], string, number[]] = [[12], "ok", [4]];
````
----
#Literal types

- Pour cela on va déclarer **une variable** qu'on va appeler **Transition**.

- Puis on va lui **donner un nouveau type** qui sera **une chaîne de caractère** qui sera déja définis.
````typescript
let transition: 'ease-in' | 'ease-out';
````
- Donc on lui dit à cette **Variable** ton **type** sera soit **la chaîne de caractère** **ease-in** soit **la chaîne de caractère** **ease-out**.
````typescript
let transition: 'ease-in' | 'ease-out';
transition = 'ease';
````
- Donc lorsque je vais devoir **assigner une valeur** il faut que je lui **insère** soit **ease-in** soit **ease-out**, car si je lui **insère** la **transition ease** il m'indique que c'est une **Erreur**.

- Pareille pour une **fonction** car si j'ai une fonction **TransformX** qui prend en premier **Argument** la **transition** qui **retourne** rien(void)
````typescript
let transition: 'ease-in' | 'ease-out';
transition = 'ease';

function transformX(transition): void {
    
}
````
- Au moment où je vais **Appeler** **TransformX**, je n'ai pas **Typer**, je pourrais mêtte tous et n'importe quoi.
````typescript
let transition: 'ease-in' | 'ease-out';
transition = 'ease';

function transformX(transition): void {
    
}

transformX(transition:2);
````
- Si je met en **paramètre** dans ma **fonction** une **chaîne de caractère**, je sais qu'il faut que je lui **insère** une **chaîne de caractère**.
````typescript
let transition: 'ease-in' | 'ease-out';
transition = 'ease';

function transformX(transition: string): void {
    
}

transformX(transition:"");
````
> Rappel => IL y a très peu de **Type de transition**. Ce sont des **Chaîne de caractère** qui sont bien **définis et spécifique**.

- Donc moi je ne peux pas juste mettre une **Chaîne de caractère**. Car pour **actionner** la **Transition ease-in** il faut que j'indique **ease-in**.

- Donc je vais le **typer** avec la **Transition literal** **Ease-in** ou **Ease-out** ou touteds les **Chaîne de caractères** que l'on souhaite.
```typescript
let transition: 'ease-in' | 'ease-out';
transition = 'ease';

function transformX(transition: 'ease-in | 'ease-out |''): void {
    
}

transformX(transition:"ease-in");
```
- Dans le cas on va rester **Simple** et on va garder **deux types literal**.

- Donc maintenant je **Définis éxactement** quelle sont les **chaînes de caractères** dont j'ai **besoin** (ease-in | ease-out).
````typescript
let transition: 'ease-in' | 'ease-out';
transition = 'ease';

function transformX(transition: 'ease-in' | 'ease-out'): void {
    
}

transformX(transition:'ease-in');
````
> Donc cela c'est l'expression **literal** on peut voir la même chose mais avec **les nombres**.
````typescript
let val: 1 | 2 | 3;
val = 4;
````
- Donc là on lui dit que **la variable val** va contenir **le nombre 1, 2, 3** mais pas autre chose.

- Donc je dois absolument lui mêttre une des **Valeurs** que j'exprime **literalement**.

> Lien de la video => [https://www.youtube.com/watch?v=8pexkpGCe5A&feature=youtu.be]
----
#Types assertions

> Il y arrive des cas ou on n'a **typer** **toute notre application**, donc on se retrouve avec une **Variable1** qui attend **une chaîne de caractère**.

- Par exemple cette **Variable1** on va lui donner **une valeur**, qui est la **Valeur qu'on n'a inséré** dans un champ dans notre **page internet**. A chaque modification on va **insérer la valeur** dans notre **variable1** qui est une **chaîne de caractère**.
````typescript
let var1: string;
````
- On ne connait pas forcément **le type de la valeur de champ**.

- La valeur qui est dans le **champ** est **inconnu**. Donc cela pourrait très bien être **43 ou 24**.
````typescript
let input: unknown = "24";
let var1: string;
````
> Ici si j'essaie **D'assigner var1 à input** j'ai une **Erreur**.
````typescript
let input: unknown = "23";
let var1: string;

var1 = input;    
````
> Car on me dit que le **type unknown** n'est pas du même **type que string**. Pour cela il faut que je lui **modifie son type**.

- Pour modifier **son type** j'utilise le **as** de syntaxe.

- Je dis que **input** sera **du type suivent** pour cela j'utilise le **as string**.
```typescript
let input: unknown = "23";
let var1: string;

var1 = input as string;    // ici sa pourrait être une classe, une interface/ anonyme
```
- Je vais **Caster** le type de la variable et dire que la variable suivante sera de **type string**, ou **number** ou **Number array**.
```typescript
let input: unknown = "23";
let var1: string;

var1 = <string>input;
```
> Donc la je dis à **typescript** fait moi confiance je sais que **input** est **une chaîne de caractère**, ou que **input** est **un nombre**. Donc cela c'est le moyen **De contrecarrer** des situations ou on n'en connait plus sur **la donnée** que **Typescript**.

> Parfois **typescript** ne sait pas ce que sait, cela vien de la **page internet**.
````typescript
let input: unknown = "23";
let var1: string;

var1 = input as string;
````
> Donc **Typescript** ne sait pas quelle sera **la donné** nous on le sait et on va lui indiquer **input as string** (cette variable sera une chaîne de caractère ou autre chose si var1 est un **nombre** on doit dire que c'est un **nombre**).

- Parfois on va dire que la donnée qui **se trouve dans cette variable** considère la comme n'importe quelle **Donnée**, pour changer le **Typage**.
````typescript
let input: unknown = "23";
let var1: string;

var1 = (input as any);
````
- Exemple si ici j'ai une **String** je vais changer **le type** pour **Typescript** et lui dire **le type de cette variable **est nimporte qu'elle donnée**. Donc cela peut très bien être un **nombre**, donc il va accepter.
```typescript
let input: string = "23";
let var1: number;

var1 = (input as any);
```
> Lorsque l'on utilise **une insertion de type**, le **compilateur** nous fait confiance, il ne va pas y **Avoir de double vérification**, car il considère que l'on n'a fait **tous les tests**. Donc il nous autorise cette **modification du type**.

> Lien de la vidéo => [https://www.youtube.com/watch?v=Osl7mcX0jbs&feature=youtu.be]
--- 
#Interface

> Les interfaces c'est un contrat de **Structure**, en déclarent une interface ce que l'on dit c'est que la prochaine **Structure** qui doit **implémenter** cette **interface** doit avoir toutes les composantes de **cette interfaces** complète.

- Pour déclarer une **interface** on utilise le mot clé **Interface**. Par exemple cela peut être **Interface IVehicule**
```typescript
interface IVehicule {
    model: string;  // cractéristique / propriété
    km: number;
    move(): string;  //fonction qui retourne une chaîne de caractère / ou roien (void)
    marque?: string; // qui est une propriété optionnelle
}
```
- Maintenant on va déclarer **une variable** qui doit **implémenter** notre **interface**.

- **CAR** doit **implémenter** **l'interface** **IVehicule**, je vais lui donner **un objet**, cette objet la doit avoir **toutes les propriétés**, qui sont défini **au dessus**.
1. Il doit avoir le **model** qui est une **chaîne de caractère**.
1. Il doit avoir le **kilométrage** qui est un **nombre**.
1. Il doit avoir la **fonction / une méthode** qui retourne une **chaîne de caracatère** ou **rien(void)**.
````typescript
interface IVehicule {
    model: string;
    km: number;
    move(): string;
    marque?: string;
}

let car: IVehicule;   // notre objet implémente l'interface
car = {
    model: 'BMW',
    km: 12,
    move: function () {
        return '';
    },
    marque: "ok"; // vu que marque est obtionnelle je ne suis pas onbliger de le mettre.
};
````
> Vu que **marque** est **optionnelle** cela ne peut être **qu'une chaîne de caractère** et pas autre chose.

- Donc on n'a définis une **interface** et on n'a dit que notre **objet**, notre **variable** doit **implémenter l'interface**.

- Chaque élément qui utilise **Ce contrat** doit respecter ce **contrat**.

> On n'a dit que notre **variable** doit **implémenter** notre **interface**.

> C'est un contrat et chaque élément qui utilise ce contrat doit respecter ce **contrat**.

> Cela nous permet de **Typer** des objets, des **classes** ou des **Tableaux**.
----

- Donc on va voir comment **Typer** une fonction, avec une **interface**.

- J'utilise le mot clé **interface**, que je vais appeler **Myfn**. Une **fonction** qui ne prend **Rien en paramètre** et qui retourne **un boolean**.

````typescript
interface MyFn {
  (): boolean; // fonction qui ne prend rien en paramètre et qui retourne un boolean
}
````
- Donc la je crée une **fonction** qui doit **implémenter** mon contrat qui s'appelle **MyFn**. Donc en valuer je dois avoir **une fonction** qui ne prend rien en paramètre et qui retourne soit un **boolean**, soit **rien**.

- Donc si je ne respecte pas mon contrat cela m'indique **une erreur** en argument car je dis bien que dans mon contrat ma fonction **n'a aucun argument**.
````typescript
interface MyFn {
  (): boolean;   // ma fonction n'a aucun argument ()
}

let myFn: MyFn = function () { // je type directement ma variable et ce que j'insère à l'interieur c'est un objet pas une fonction
    return true;
};
````
> Je peux donner un nom à ma méthode on constate que j'ai une erreur.

- Car tous ce que je dit maintenant c'est que toutes les variables qui **implémente** mon **contrat MYFn** doivent être des **objets** qui contiennent **des propriétés** qui s'appelle **tick**, qui ai une fonction.

- Donc pour que cela **fonctionne**, il faudrait que :

1. Je crée un objet
2. Que la propriété qui s'appelle **tick** soit une fonction, qui ne prend **rien en paramètre**.
3. Qui retourne un **boolean**.

`````typescript
interface MyFn {
  tick(): boolean;
}

let MyFn: MyFn = {
    tick: function () {
        return true;
  }
};
`````
- Donc on voit que cela fonctionne.

> Attention si je ne met rien j'attens d'avoir une **fonction directement**.
````typescript
interface MyFn {
  (): boolean;
}

let MyFn: MyFn = function () {
        return true;
};
````
> De la même manière, je supprime et je veux **typer un tableau**.

- Je crée une **interface** que j'appelle **RandomArray**.

- Pour **typer un tableau** j'ouvre les crochets.
  
- Et je lui indique que je vais **typer l'index**, la **position** qui doit être un **nombre**.

- Et la valeur qui **doit être une chaîne de caractère**.
````typescript
interface RandonArray {
  [index: number]: string;
  
}
````
- Maintenant je déclare une **variable Array** qui doit **implémenter** mon contrat **RandomArray**.

- Ici en **index** je dois avoir **un nombre**, donc ici cela sera ma **Valeur** et la **position de ma valeur**, donc **Array** et **0** car il n'y a qu'un seul élément et ma **valeur doit être une chaîne de caractère**.
````typescript
interface RandonArray {
  [index: number]: string;
  
}

let arr: RandonArray = [
    "Ma valeur"
];
console.log(arr[0]);
````
- On voit que je **n'ai pas d'erreur**.

> Si je met **une string** dans mon **objet**, maintenant j'ai une erreur, car il faut que je lui **indique la chaîne de caractère** et non **la valeur**. Donc maintenant cela se transforme **en objet**.

- Sa pourrais être en **valeur un nombre**, donc en **Valeur** je vais avoir un **nombre**.
````typescript
interface RandonArray {
  [index: string]: string;
  
}

let arr: RandonArray = {
  'ok': 3,
};
console.log(arr[0]);
````
> Donc voilà ce que c'est une **interface**.

-----
- Pour les héritages..
  
- Je peux avoir **une inteface **IVehicule**, qui a **le model** qui est **une chaîne de caractère**.

- On peut avoir une Autre **interface** **ICar**, qui a la propriété **model**.

> Ce que l'on dit c'est que **ICar** **etand IVehicule**

````typescript
interface IVehicule {
    model: string;
}

interface ICar extends IVehicule {
    model: string;
}
````
- Maintenant lorsque l'on **Va typer** notre voiture avec le contrat **ICar**, le **I** majuscule veut dire **interface**. Car si on ne met pas de **I** on pourrait confondre avec **une classe**.

> Dans mon interface **IVehicule**, à la place de **dupliquer model**, on pourrais créer **un nombre de roues**.
````typescript
interface IVehicule {
    model: string;
}

interface ICar extends IVehicule { // je dis que l'interface icar doit implémenter l'interface IVehicule
    wheels: number;
};
````
- Je déclare une **variable car** donc un **objet** qui possède la **propriété** wheels et également **un model, une chaîne de caractère**.
````typescript
interface IVehicule {
    model: string;
}

interface ICar extends IVehicule { // je dis que l'interface icar doit implémenter l'interface IVehicule
    wheels: number;
}

let car: ICar = {
    wheels: 4,
    model: "ok";
};
````
> Maintenant je vais **étendre** une interface **bike**, qui a la meme **propriété** que la voiture.

- On dit que le **IVehiciule** va avoir une **fonction** qui s'appelle **move** et qui ne retourne **rien**.

- Donc maintenant la variable **car**, doit **implémenter** une fonction qui me **Permet** de me **déplacer**.

- On fait pareille pour **Bike** qui doit avoir les mêmes **propriété** qui **ICar**.
````typescript
interface IVehicule {
    model: string;
    move(): void,
}

interface ICar extends IVehicule { // je dis que l'interface icar doit implémenter l'interface IVehicule
    wheels: number;
}

interface IBike extends IVehicule {
    wheels: number;
}

let car: ICar = {
    wheels: 4,
    model: "ok",
    move: function () {
        
    }
};

let bike: IBike = {
    wheels: 4,
    model: 'ok',
    move: function () {
        
    };
````
> On voit que j'ai **deux interfaces** qui on hérité du contrat **IVehicule**.

> Les **interfaces** peuvent être **utilisé partout**.
> 
> là on les a utilisé **pour déclarer une variable**, mais on peut également **typer une fonction**, qui prend par exemple un **Vehicule** en paramètre et ce **Vehicule** doit être du type **IVehicule** et il va retourner un **vehicule**.
````typescript
interface IVehicule {
    model: string;
}

let bike: IVehicule;  // declarer une variable

function (vehicul: IVehicule): IVehicule {   // typer une fonction
    
}  
````
- Si je déclare **une voiture**, lorsque je vais appeler **ma fonction** garage,

- Ce que je dit c'est que je doit retourner un **vehicule** également.
````typescript
interface IVehicule {
    model: string;
}
let car: IVehicule;
let bike: IVehicule;  // declarer une variable

function garage(vehicule: IVehicule): IVehicule {   // typer une fonction qui retourne un IVehicule
    let car1: IVehicule;
    return car1;
}  

garage(bike); // ici je doit l'appeler soit car soit bike
````
- Donc l'intérêt d'utiliser les **interfaces** c'est qu'on va s'assurer qu'il existe une structure dans l'objet qu'on reçoit car on définit la **Structure**. Par exemple on va utiliser la fonction **move** qui renvoie **une chaîne de caractère**.

- Si je veux appeler **move** il faut que je sois sur que **l'objet vehicule** est la méthode **move**. Comment je suis sur car j'ai rajouté un **Type** a mon objet **vehicule**.

- Ici je ne peux passer que des objets qui **implémente** mon contrat, je vais donner a ma fonction **garage car** .

- Je vais implémenter ma **fonction** qui doit **r'envoyer this.model**.

- IVehicule attend d'avoir une propriété **model et une méthode move**
````typescript
interface IVehicule {
    model: string;                   // structure // si on na pas cette fonction move on ne peut pas avoir cette logique en dessous
    move(): string;
}
let car: IVehicule;  // declarer une variable
car = {
    model: 'BMW',   // ici je doit avoir un model
    move: function() {
        return this.model;
    }
}
function garage(vehicule: IVehicule): IVehicule {    //l'objet vehicule
    
    vehicule.move();
    vehicule.model;
    return vehicule;
}  

garage(car); // je ne passe que des objets qui iplémente mon contrat
````
> La logique **function garage** ne peux fonctionner que si j'implémente **l'interface IVehicule**. Car dans ma logique je dois utiliser **move** par exemple.
> 
> C'est pour cela que c'est utile de savoir utiliser des **interfaces**. Car on va pouvoir **Typer** une structure de **Donnée** , et on va s'assuer **que notre objet implémente cette structure de donnée**.
> 
> Lien de la vidéo => [https://www.youtube.com/watch?v=fJQZGKtMGTw&feature=youtu.be].
-----
# Les Classes

> Pour cela on va d'abord créer une classe en **javascript natif** .

- On va créer une classe qui s'appelle **car** avec le mot clé **class**.

- On va déclarer un **constructeur**, lorsque l'on veut construire **une nouvelle instance** on va devoir récupérer **le model**.

- Ensuite on va enregistrer le **model** dans l'instance **this.la propriété dans l'instance (model) et on va insérer le model entré dans le constructeur**.

````typescript
class Car {

  constructor(model) {
      this.model = model;   //on enregistre le model dans l'instance
  }
}
````
- Donc là je vais déclarer une **Variable car** et créer **une nouvelle instance**, donc **new Car**.

- Ici dans le **constructeur** j'attends un **model** donc on l'indique dans **l'instance**, il **attend 1 argument et en reçoit 0**.
````typescript
class Car {

  constructor(model) {
    this.model = model;   //on enregistre le model dans l'instance
  }
}
let car = new Car(model: 'RS3');  // je viens de créer une instance de ma classe Car
````
- Je viens de créer une **instance de ma classe Car**.

> Sauf que **Typescript** attend de **Typer**, l'aventage de **Typescript** c'est le fait de **typer les eléments**.

> Et **this** ne connait pas **model**, car **model** n'existe pas dans la **Class Car**.

- Il faut définir **model** pour pouvoir **insérer model** et lui dire que car sera une **String**.

- Ici je pourrais également **typer** l'argument passé au **constructor**.
````typescript
class Car {
    
    model: string;   // il faut déclarer la propriété avant de l'enregistrer

  constructor(model: string) {
    this.model = model;   //on enregistre le model dans l'instance
  }
}
let car = new Car(model: 'RS3');
````
> Rappel : Il faut déclarer la propriété avant de l'enregistrer

- Maintenant je peux accéder à **car.model** et l'afficher dans **ma console**.
````typescript
class Car {
    
    model: string;   // il faut déclarer la propriété avant de l'enregistrer

  constructor(model: string) {
    this.model = model;   //on enregistre le model dans l'instance
  }
}
let car = new Car(model: 'RS3');

console.log(car.model);
````
- Cela est bien avec **typescript** c'est qu'on peut ajouter des mots clé pour **l'héritage**.

- Le premier mot clé sera **public**, je peux définir que **model** est une **propriété public**, par défaut **tout est public**, donc je peux accéder à la propriété **model** partout.
````typescript
class Car {
    
    public model: string;   // il faut déclarer la propriété avant de l'enregistrer

  constructor(model: string) {
    this.model = model;   //on enregistre le model dans l'instance
  }
}
let car = new Car(model: 'RS3');
console.log(car.model);
````
- Par contre si je dis que la propriété **model** est privé je ne pourrais pas **y accéder**.

- Je pourrais y accéder que dans mon **objet** si par exemple j'ai une méthode **move** et qui affiche dans la console **this.model + moves**.

- J'exécute ma **méthode move** qui affiche dans ma console le nom de ma **voiture concaténer avec moves**.
````typescript
class Car {
    
    private model: string;   // il faut déclarer la propriété avant de l'enregistrer

  constructor(model: string) {
    this.model = model;   //on enregistre le model dans l'instance
  }
   move() {
      console.log(this.model + 'moves');  // instance
   }
}
let car = new Car(model: 'RS3');
car.move();
````
> Donc là je ne peux pas accéder à la propriété **model** mais dans mon instance je peux **y accéder**.
----
- Je peux aussi avoir un **nom** qui lui est **public** et un **kilométrage** qui lui est **protégé**.

- Donc là on n'a 3 niveaux **d'accessibilité** à des **méthodes**.
````typescript
class Car {
    public name: string;      // 3 niveau d'accessibilité
    protected km: string;
    private model: string;   // il faut déclarer la propriété avant de l'enregistrer

  constructor(model: string) {
    this.model = model;   //on enregistre le model dans l'instance
  }
  
   protected move() {   // je peux également dire que cette méthode est protégé ou privé ou public
      console.log(this.model + 'moves');  // instance
   }
}
let car = new Car(model: 'RS3');
car.move();
````
- La **propriété protected** c'est dans le cas ou on n'a un **héritage**.

- Donc ici on n'a une **seconde classe** exemple: un **BMW** qui **etand de la classe Car**. Donc la on dit que **BMW** c'est un **enfant de la classe Car**.

- Maintenant si je déclare **BMW** je vais avoir les memes fonction de **Car** qui sont accessible.

> Mais attention ici vu que notre **classe parent a un constructeur il faut appeler ce constructeur là**, on l'appelle **super**.

- Le mot clé **Super**Va exécuter le **constructeur**, on voit que le constructeur de la classe Car attend un **model** donc il faut lui donner **un model** qui est une **Chaîne de caractère**.
````typescript
class Car {
  public name: string;      // 3 niveau d'accessibilité
  protected km: string;
  private model: string;   // il faut déclarer la propriété avant de l'enregistrer

  constructor(model: string) {
    this.model = model;   //on enregistre le model dans l'instance
  }

  public move() {   // je peux également dire que cette méthode est protégé ou privé ou public
    console.log(this.model + 'moves');  // instance
  }

  class BMW extends Car {   // BMW est un enfant de la callse Car
  
      constructor(model: string) {
        super(model);
    }
  
  }

let car = new BMW(model:'RS3');

car.move();
````
> Donc on a juste mis en place un **héritage**.

- Le mot clé **protected** définit que la propriété **km** est accessible que pour les **enfants**. Donc on ne peux pas utiliser **this.km**.

- Si c'est privé on ne peut pas utiliser **this.model**.

- Et si c'est **public** tout le monde peut y **accéder**.

- Je rappelle que je ne peux pas accéder à **car.model ou car.km**.
````typescript
class Car {
  public name: string;      // 3 niveau d'accessibilité
  protected km: string;
  private model: string;   // il faut déclarer la propriété avant de l'enregistrer

  constructor(model: string) {
    this.model = model;   //on enregistre le model dans l'instance
  }

  public move() {   // je peux également dire que cette méthode est protégé ou privé ou public
    console.log(this.model + 'moves');  // instance
  }

  class BMW extends Car {   // BMW est un enfant de la callse Car
  
      constructor(model: string) {
        super(model);
        console.log(this.km); //on ne peux pas utiliser la méthode this pour km car cest protected
    }
  
  }

let car = new BMW(model:'RS3');

car.move();
car.model();  //je ne peux pas y accéder 
car.km();  //je ne peux pas y accéder   car elle son protégé ou privé
````
- Si je veux accéder à **model** il faut que dans mes parents j'ai une méthode **getModel** qui me retourne **this.model** et dans ce cas la **model sera public**.

> Maintenant j'utilise **this.getModel** dans mon constructor pour avec le **model** dans **BMW**.
````typescript
class Car {
  public name: string;      // 3 niveau d'accessibilité
  protected km: string;
  private model: string;   // il faut déclarer la propriété avant de l'enregistrer

  constructor(model: string) {
    this.model = model;   //on enregistre le model dans l'instance
  }

  public move() {   // je peux également dire que cette méthode est protégé ou privé ou public
    console.log(this.model + 'moves');  // instance
  
  }
  
  public getModel(): string {  //maintenant le model sera public
      return this.model  
  }
}

  class BMW extends Car {   // BMW est un enfant de la callse Car
  
      constructor(model: string) {
        super(model);
        console.log(this.getModel()); // jutilise la méthode pour avoir getModel
    }
  
  }

let car = new BMW(model:'RS3');

car.move();
````
- Donc cela est pour le coter **héritage**.
----
- On peut déclarer une **interface**.

- Pour déclarer une **interface** j'utilise **ICar** et je crée une **Class Car**.

- l'interface **ICar** va avoir un **nom et une chaîne de caractère**, un **medel qui sera une chaîne de caractère** et  une **méthode qui va retourner rien/ void**.
````typescript
interface ICAr {
    name: string;
    model: string;
    move(): void;
}

class Car {
    
}
````
- Ma **class Car** doit **implémenter** pour cela j'utilise le mot **impléments** le contrat définit dans **l'interface ICar**.
````typescript
interface ICAr {
    name: string;
    model: string;
    move(): void;
}

class Car implements ICAr {
    name: string;
    model: string;
    move(): void {
      
    }
}
````
- Maintenant si je crée une **instance car** qui va être du **Type ICar**.

- Pour créer une **instance c'est le mot clé new, je crée une instance de la class Car**, je peux accéder au **nom** en faisant **Car.name**
````typescript
interface ICAr {
    name: string;
    model: string;
    move(): void;
}

class Car implements ICAr {
    name: string;
    model: string;
    
    move(): void {
      
    }
}

let car: ICAr = new Car();
car.name;  // je peux accéder au name
````
> L'avantage, cela serais ex: **l'interface IVehicule**, onn pourrait avoir une **autre class qu'on appellerait Bike** qui **implémente également le contrat de IVehicule avec les memes propriétés**.

- Car un vélo **c'est également un véhicule** il a aussi les memes propriétés qu'un **Véhicule**.

> Donc là je peux créer pleins de classes qui **implémente mon contrat**.

- Maintenant je ne suis plus obligé de **typer par la class CAr ou Bike** mais par **l'interface**.

- Ex: je veux que ce soit un véhicule qui puissent bouger j'utilise **vehicule.move()** et je **type le vehicule avec l'interface**.

- Je peux également créer une **instance Bike** cela fonctionne encore.
````typescript
interface IVehicule {
    name: string;
    model: string;
    move(): void;
}

class Car implements IVehicule {
    name: string;
    model: string;
    
    move(): void {
      
    }
}
class Bike implements IVehicule {
    name: string;
    model: string;
    move(): void {

  }
}

let vehicule: IVehicule = new Bike ();
vehicule.move();
````
> Les interfaces et les classes sont très utilisé.
---

> Les classes peuvent être utilisé pour **Typer**.

- Je peux dire ici que j'ai **une chaîne de caractère**, et je crée une **Variable coord**. Je l'utilise pour **typer** je dis que ma variable doit implémenter **x et y**.
````typescript
class Cord {
    x: string;
    y: string;
  
}

let cord: Cord;

cord = {
    x:'ok',
    y:'ok',
};
````
- Il y a une différence c'est qu'on utilise **jamais les classes pour typer**, on peut mais on va utiliser **les interfaces**.
---
- Maintenant je vais **compiler le code** dans mon **terminal** j'utilise la commande **tsc** avec le fichier que je vais **transpiler en javascript** donc **tsc index.ts**.

- Je vais regarder mon fichier qui a étais généré, je vois qu'il m'a créé le meme **Code type script en javascript**
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-02-20%20à%2011.11.44.png)
  
> Donc ce qu'il a fait c'est qu'il a **créé une variable cord** car j'ai déclaré une classe et ensuite il m'a créé mon objet**.

> Les **interfaces** étend pas natif à **javascript**, ce sont des **éléments uniquement ajouté** par **typescript** et lorsqu'il va **compiler en javascript il va supprimer toutes les interfaces**.
> 
> Les interfaces **servent juste pour le développement** pour être sur de ce que **l'on utilise** .

- Si j'utilise les **interface dans mon index.js** et que je **Retranscris le code dans mon tableau** on voit que dans mon index.js il a supprimer une partie, donc **cela sera un gain de temps, nos projet seron moins volumineux**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-02-20%20à%2011.28.08.png)
  
- Toutes les interfaces vont être supprimer donc il va rester que le **Vrais code**, pour les applications si vous utiliser **Des classes pour typer** au lieux des **interfaces** cela va nous genérer des **codes inutiles** dans notre fichier final et ce n'est pas ce que l'on souhaite.

> Lien de la vidéo => [https://www.youtube.com/watch?v=u3FkhyrHJCs&feature=youtu.be]
-----
# Les paramètres des propriétés

> Lorsque l'on déclare une classe avec le mot clé **class** qui va avoir un **constructeur** qui va récupérer le ***nom** par exemple.

- On va vouloir insérer le **nom** dans l'instance, pour cela on doit également dire qu'on n'a une propriété **name** public qui est une **String**.
````typescript
class Person {
    public name: string;
    constructor(name) {
        this.name = name;  // nom dans l'instance
    }
};
````
- On n'a vu que lorsque l'on veut insérer une nouvelle **donnée ex la propriété name** il faut d'abord **la déclarer**.

> Maintenant ce que l'on peut faire grâce à **typescript** c'est de déclarer les propriétés et les **initialiser** au **même endroit**, en utilisant le mot clé **public, private, protected** directement dans le paramètre de **constructor**.

````javascript
class Person {
  public name: string;

  constructor(public name) {
      this.name = name;
  }
}
````
- Donc maintenant on n'est plus obliger de définir la propriété **name**, on n'est plus obligé de **l'insérer avec un this.name**. Automatiquement il va déclarer **une propriété name**, on peut également **Typer**.

````javascript
class Person {

  constructor(public name: string) { // en une seule ligne
  }
}
````
> Donc en une seule ligne on a fait les 3 lignes du **Dessus**, on a supprimé **3 lignes**.
----
- Ce qu'on peut faire c'est de créer une **instance** de **personne**.

- Je crée une **instance** avec le mot clé **new** et je vais lui insérer dans **le constructeur** son **nom**.

> On voit que **la variable person** qui contient **l'instance** je l'ai **Typer** avec **la class**.
>
> Lorsque l'on crée **une onstance** l'instance est du même **type que la class**. Don effectivement je peux **Typé avec le nom de la class**.
````javascript
class Person {

  constructor(public name: string) { // en une seule ligne
  }
}

let person: Person = new Person(name:'Sarah');

````
- Je vais maintenant afficher **le type de la class/ de l'instance** en faisant **console.log(typeof person)**. Ce que je vais faire en plus c'est d'afficher **son nom**.
````javascript
class Person {

  constructor(public name: string) { // en une seule ligne
  }
}

let person: Person = new Person(name:'Sarah');

console.log(typeof person, person.name);
````
- Donc je vais compiler mon **code typescript** en **javascript**, pour cela j'utilise **la commande **Tsc et j'indique le fichier que je veux transpiller** qui est **index.ts**, ensuite je vérifie dans mon fichier **javascript**.

- Lorsque je suis sur mon **fichier index.js** je peux exécuter mon **fichier index.js** dans mon tableau en faisant **node index.js**. J'appuie sur **Entrer** et il m'affiche **mon type qui est du type objet** car on n'est dans **du javascript**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-02-21%20à%2012.12.06.png)
  
> En **Typescript** l'instance d'une classe, son type **c'est la class**.

- Dans mon **index.js** je ne peux pas **Typer avec une classe**, une instance **De personne ne peut pas être une voiture**.

> LIEN DE LA VIDÉO =>[https://www.youtube.com/watch?v=iwC6cAd2y4U&feature=youtu.be]
 ----
#Import export modules

> On va parler des **modules** de **l'exportation de l'importation des objets en typescript**.

> En javascript depuis **ECMA 2015** on peut **Devolloper des modules**.

> Les modules ce sont des block **d'environnement**, dans lequel on va avoir des **Variables, des fonctions** et qui ne seront pas utilisables ailleurs.

- On va créer un **Fichier que l'on va appeler car en ts**, ensuite on va déclarer une **interface que l'on va appeler ICar**.

- On lui dit que l'on veut avoir **une propriété model** qui soit **une chaîne de caractères**.

- On lui dit que l'on veut avoir **une propriété km** qui soit **un nombre**.

- On déclare **une class Car** qui implémente **l'interface que l'on vient de créer**. Donc on veut **une class qui est les mêmes propriétés que l'on vient de créer**.

- On utilise les propriétés **pour déclarer une variable et assigner la valeur en même temps**.

````typescript
interface ICar {
  model: string;
  km: number;
}

class Car implements ICar {

  constructor(public model: string, public km: number) {
  }
}
````
- Pour créer une **instance** j'utilise le mot clé **new** et je crée **l'instance d'une voiture**.
````typescript
interface ICar {
  model: string;
  km: number;
}

class Car implements ICar {

  constructor(public model: string, public km: number) {
  }
  let car = new Car(model:'BMW', km:13234);
}
````
> Récap: j'ai créé un fichier **car.ts**, donc on peut considérer que ce **fichier c'est un module**, qui se **préoccupent que des voitures**.

> Si je veux utiliser **Des modules de voitures qui me permet de créer des instances de voitures**, il faut que **j'importe les éléments qui se trouve dans ce fichier**.

- Pour importer **les éléments qui se trouve dans ce fichier car.ts** il faut que je les marques avec **Export** comme **exportable**.

- Je dis que je peux **exporter de ce fichier l'interface ICar**.

- Je peux également **exporter de ce fichier la class Car**.
```typescript
export interface ICar {
  model: string;
  km: number;
}

export class Car implements ICar {

  constructor(public model: string, public km: number) {
  }
  let car = new Car(model:'BMW', km:13234);
}
```
- Je peux le faire de cette manière ou je peux **Exporter Car directement par défaut**.
````typescript
interface ICar {
  model: string;
  km: number;
}

class Car implements ICar {

  constructor(public model: string, public km: number) {
  }
export default Car;
}
````
- Si je veux pouvoir **Exporter plusieurs éléments** j'utilise des **Accolades**. Dans ses **Accolades** je donne **les arguments que je veux exporter**.

- Ce que je peux faire c'est d'avoir **plusieurs arguments que je veux exporter** et avoir **un argument que je veux exporter par defauts**.
````typescript
interface ICar {
  model: string;
  km: number;
}

class Car implements ICar {

  constructor(public model: string, public km: number) {
  }
export{Car, ICar};     // la on n'a marqué les éléments qui sont exportable
export default Car;
}
````
> Donc là on a marqué les **éléments que l'on veut exporter, mais on veut les importer** maintenant.

- Exemple: je veux **importer ma classe Car**, mais d'ou es ce que je vais **l'importer** pour cela j'utilise le mot clé **from** puis je lui donne **le chemin d'accès**donc le **point** pour le **fichier courant** et ensuite le **slash/** dans le fichier courant ou je vais recupérer le fichier **car**.

````typescript
//import Export
import Car from'./car';
````
- Maintenant je peux créer une **nouvelle instance de ma class car**, je peux lui donner **un model et le kilométrage**.
````typescript
//import Export
import Car from'./car';

ler car = new Car (model: 'RS3', km: 1313);
````
- Ici c'est la class qui est **exporter car on l'a défini comme exportable par défaut**. Si je ne le définis pas **je ne pourrais l'importer comme au dessus**.
```typescript
interface ICar {
  model: string;
  km: number;
}

class Car implements ICar {

  constructor(public model: string, public km: number) {
  }
export{Car, ICar};     // la on n'a marqué les éléments qui sont exportable
}
```
- Pour récupérer la class par **Défaut** soit je lui récupère tous (*), **as** qui nous permet **D'assigner un nom** je lui donne un **nom** exemple: **fromCAr** avec **la syntaxe as** qui nous permet **d'assigner **un nom**, donc maintenant je peux récupérer **Via mon objet fromCar** l'élément Car**.
```typescript
//import Export
import * as fromCar from'./car';

ler car = new fromCAr.Car (model: 'RS3', km: 1313);
```
- Donc cela c'est un des moyens pour récupérer tous les **éléments exporté d'un fichier**, cela fonctionne dans le cas où j'ai un **export** à la fin **qui exporte un objet**.

- Si j'ai plusieurs **exports au moment de la déclaration**, donc là je déclare une **interface** qui va être **exportable**, je déclare également **une voiture qui va être exportable**
````typescript
export interface ICar {
  model: string;
  km: number;
}

export class Car implements ICar {

  constructor(public model: string, public km: number) {
  }
//export{Car, ICar};
//export default Car;
}
````
> Donc là j'ai plusieurs éléments **qui vont être exportable**. Si je ne m'et pas de **exporte elle pourra pas exporter ICar**, cela est dans le cas ou j'utilise le mot clé **export**.

> Dans mon **Index.ts** cela revient **au même**, tous ce qui est **exportable je l'importe et je l'insère dans fromCar**.
````typescript
import * as FromCar'./car';  //je peux utiliser cette syntaxe là pour tous récuperer et donner un nom

ler car = new FromCar(model: 'RS3', km: 1313);
````
- Je vais remèttre dans mon **car.ts** mon **exporte à la fin** qui va exporter un **objet contenant la class et l'interface**.
````typescript
interface ICar {
  model: string;
  km: number;
}

class Car implements ICar {

  constructor(public model: string, public km: number) {
  }
export{Car, ICar};
````
-----
- Ici je peux utiliser cette syntaxe pour **tout récupérer et donner un nom**.
````typescript
import * as FromCar'./car';  //je peux utiliser cette syntaxe là pour tous récuperer et donner un nom

ler car = new FromCar(model: 'RS3', km: 1313);
````
- Ce que je peux faire aussi c'est **j'ouvre l'objet et dire dans mon objet je vais récupérer que la class Car**.
`````typescript
import {Car}From'./car';  // je récupère seulement la class Car

ler car = new Car(model: 'RS3', km: 1313);
`````
> Donc là je peux **Récupérer plusieurs éléments dans mon objet que je sépare par une virgule**.

- Donc je peux dire que **car est typé par l'interface ICar**.
````typescript
import {Car, ICar}From'./car';  

ler let car: ICar = new Car(model: 'RS3', km: 1313);
````
> Comme ma class **implémente mon interface le typage fonctionne bien**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-02-22%20à%2015.29.50.png)

> C'est utile car imaginons que l'on n'a un **fichier qui exporte 20 éléments**

- On n'a :
1. **Une classe**
2. **Une interface**
3. Des **fonctions exporté**

- Une fonction exporté que l'on nomme **carFactory** (usine à voiture).

- Une usine à voiture va me retourner un **Tableau de plusieurs instance de ma voiture**, je vais pouvoir typer et dire que la **fonction carFactory me retourne un tableau de l'interface ICar**.
````typescript
export interface IBike {
  model: string;
  km: number;
}
export interface ICar {
  model: string;
  km: number;
}

export class Car implements ICar {

  constructor(public model: string, public km: number) {
  }
  
  export function carFactory(): ICar {  // je type et je dit quelle me retourne un tableau de l'interface ICar
        return [new Car(model: 'BMW', km: 12)];
}  //fonction exporté
export{Car};
//export default Car;
````
> Donc si on n'a 20 éléments on ne va pas **tous les récupérer**.

- Je veux seulement récupérer les **modules qui m'intéresse dans mon fichier car.ts**.

> Donc cela peut être une fonction qui peut être **importé**.
-----
- Dans mon **index.ts** je veux avoir un **Tableau de voiture**, je vais importer ma **classe carFactory**, pour cela je récupère **l'export et je l'utilise**.
````typescript
//import Export
import {Car, ICar, carFactory}From'./car';   //importe ma class carFactory

ler let car: ICar = new Car(model: 'RS3', km: 1313);

let cars = carFactory();
````
> Si j'affiche ce que contient ma variable **Cars** c'est mon tableau qui a été **retourné**.
````typescript
  export function carFactory(): ICar {  // je type et je dit quelle me retourne un tableau de l'interface ICar
        return [new Car(model: 'BMW', km: 12)];  //tableau qui a été retourné
````
> Cela est un moyen de partager du **code entre les fichiers**, lorsque l'on **importe un projet via MPM **, lorque l'on **installe un package**, une librairie, une bibliothèque de fonction** a chaque fois on va importer à partir de cette **librairie**.
````typescript
//import Export
import {Car, ICar, carFactory}From'./car';  //<= à partir de cette librairie
````
> Au final ce que fait cette librairie elle va **Exporter les éléments qu'elle a codé**.

> Donc elle va exporter **l'ensemble des fonctions qui sont utilisables, l'ensemble des classes etc...**. en marquant avec **un export**.
````typescript
export class Car implements ICar {  //l'ensemble des classes

  constructor(public model: string, public km: number) {
  }
  
  export function carFactory(): ICar { // exporter l'ensembles des fonctions 
        return [new Car(model: 'BMW', km: 12)];
````
> Ce qu'elle peut faire également est de modifier **l'ensemble de l'export**. En disnat que **Car** est exporté mais sous le nom de **CarClass**
````typescript
export interface IBike {
  model: string;
  km: number;
}
export interface ICar {
  model: string;
  km: number;
}

export class Car implements ICar {

    constructor(public model: string, public km: number) {
}
  
export function carFactory(): ICar {  
        return [new Car(model: 'BMW', km: 12)];
}  
export{Car as CarClass}; // car est exporté sous le nom de CarClass
````
> Donc sur notre un **index.ts** on modifie **Car** par **CarClass**.

- On peut changer le nom de **carFactory** et dire que maintenant son nom est **Factory**.
````typescript
//import Export
import {CarClass, ICar, carFactory as Factory}From'./car';   

ler let car: ICar = new CarClass(model: 'RS3', km: 1313);

let cars = Factory();
````
> Ce que je peux aussi faire est de **tous récupérer et de le nommer** mais ce n'est pas dans les **crochets mais en dehors**.

````typescript
//import Export
import * as fromCar From'./car';   

let car: fromCar.ICar = new CarClass(model: 'RS3', km: 1313);

let cars = fromCar.CarFactory();
````
> Récap: on peut **exporter tous les éléments** avec le mot clé **export** ou importer tous les éléments avec le mot clé **import** soit **Exporter ou importer certains éléments en particuliers**.

> Donc après le mot clé **from** on doit donner le **Chemin d'accès au fichier**, ensuite on peut utiliser tout les éléments **exporté**.
------
> C'est libre accès de créer des **modules**, exemple: **user.ts**.

- Je pourrais avoir un utilisateur qui contient **son nom, email**.

- Je peux avoir une classe **USer** qui **implémenterais mon interface IUser**, qui aurait un **Constructeur** qui prendrait un **nom**. Donc qui **déclarerait la propriété name et qui insérait la propriété name dans ma class User**.

````typescript
interface IUser {
  name: string;
  email: string;
}

class User implements IUser {

  constructor(public name:string) {
  }
}
````
- Pareille pour le **email**, ici il le comprend qu'on **ajoute la propriété email dans User**.
````typescript
interface IUser {
  name: string;
  email: string;
}

class User implements IUser {

  constructor(public name:string, public  email:string) {
  }
}
````
> Maintenant il n'y a plus d'erreur **USer** implémente correctement l'interface **IUser**.

> Je peux **exporter ses deux éléments**, je peux **Exporter User par défaut** et je peux **Exporter l'interface séparément**.
````typescript
export interface IUser {
  name: string;
  email: string;
}

class User implements IUser {

  constructor(public name:string, public  email:string) {
  }
}
export default User;
````
> Dans mon **index.ts** je peux maintenant **importer un autre module du fichier courant users**.
> 
> Je vais créer un **utilisateur** de **USer** en lui donnant un **nom et un email**.
````typescript
//import Export
import {CarClass, ICar, carFactory as Factory}From'./car';   
import User from'./users';
ler let car: ICar = new CarClass(model: 'RS3', km: 1313);

let cars = Factory();

let user = new User(name: 'Sarah', email:'sarah@hotmail.fr');
````
> Donc celà je peux le faire à l'infini. **Libre à nous**

> Lien de la vidéo => [https://www.youtube.com/watch?v=u6hSND2wNcw&feature=youtu.be]
--------
# Ts config

> Lorsque l'on utilise une commande que l'on ne connait pas il faut toujours regarder **le manuel**.

- Donc ici, la commande c'est **tsc** on rajoute **-h** ou *--help**.

- Donc ici cela va nous afficher toutes les **options** qui sont utilisable avec **cette commande**, ainsi **la manière de comment utiliser cette commande**.

- La syntaxe on voit c'est **Tsc -h (donc le nom de la commande)** plus **(les options) et ensuite le fichier**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-03-01%20à%2012.05.48.png)
----
> Ici on n'a des exemples :
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-03-01%20à%2012.07.13.png)
-----
> Ici on n'a la liste des options qui sont utilisable avec la **commande tsc**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-03-01%20à%2012.11.00.png)
- Toutes ses options peuvent être utilisé dans une seule commande.
----
- On va essayer l'option **-w**, donc je nettoie mon terminal avec **clear** et je vais utiliser la première commande.

- La première commande sera **ts tsc -w** pour lui montrer que je veux **watch**, écouter les modifications du fichier et le fichier que je veux écouter c'est **index.ts**, j'appuie sur **entrer**, cela indique qu'on commance le mode **watch** et qu'il n'a trouvé **aucune erreur**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-03-01%20à%2012.17.32.png)
  
- Donc je vais modifier le fichier et l'interpréteur va capter les modifications dans mon **index.ts**.
----
- Je vais déclarer une variable 1 qui sera de type **String** et je vais lui donner **un nombre à la place**.
````typescript
let var1: string = 1;
````
> On voit que l'erreur ressort
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-03-01%20à%2012.22.05.png)

> **Le type nombre n'est pas assignable à un type string**.
````typescript
let var1: string = "1";
````
- Je modifie m'a déclaration de variable en lui donnant une **chaîne de caractère**, on voit la modification et il me retourne qu'il a trouvé **Aucune erreur**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-03-01%20à%2012.25.24.png)
-----
- Pour **Stopper ce watch mode** je fais **commande c ou contrôle c**.

- Je peux relancer ce mode avec le **-w et l'option --pretty** toujours en lui indiquant **le fichier que je veux écouter donc index.ts**, donc là il ne me sort **aucune erreur** mais dans le cas ou je remets **un nombre ou un boolean** , je vais avoir un message qui est **un peux plus styliser**
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-03-01%20à%2012.33.14.png)
  
- Je vais stopper cela et on va revenir au niveau de l'aide donc **ts tsc -h** pour voir **les différentes options**.

- Ici on n'a une option **-t ou --target** qui nous permet de **signifier la cible** donc en quelle version **d'ECMA SCRIPT** je veux compiler mon code.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-03-01%20à%2012.37.06.png)
> Donc je peux signifier dans la même commande le **-t sera ma target**, moi je veux que mon fichier index.ts soit compiler en **ES5**, ou qu'il **soit compilée en ES2020**.

> Donc il est possible de rajouter le code qui va être resortit par **la compilation**.
---- 
- Donc je lance la compilation, je modifie **ma déclaration de variable 1** et on voit qu'il a **compiler notre fichier index.js en ES5**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-03-01%20à%2012.54.18.png)
  
> Donc ici je peux le rechanger et le mettre en nombre on va voir qu'il va bien **le modifier**, donc cela compile directement mon **fichier index.ts**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-03-01%20à%2012.57.12.png)
----
> Ce que je peux faire c'est de couper le serveur et revenir au niveau de l'aide, c'est de **regarder les autres options**. Au niveau des autres options je peux **définir le dossier de sortie** avec **l'option --outDic ./build index.ts**, je veux que ce qooit dans le dossier **build**. Maintenant on compile, il y a une erreur au niveau de la déclaration donc il faut bien mettre une **Chaîne de caractère** et ensuite on voit qu'il nous a bien sorti **notre compilation dans le dossier build**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-03-02%20à%2013.42.14.png)
> Cette fois il ne **compile pas dans le dossier source**, donc on n'a **défini un dossier dans lequel il va être compilée**.
-----
- Donc je peux changer mon code **en lui mettant une chaîne de caractère**.
````typescript
let var1: string = "2";
````
- On va voir que ce sera dans le dossier **build** qui va me **compiler ce fichier là**. Ce qui est intéréssent c'est que ce dossier **build** peut être **partagé sur un serveur et en exécutant le fichier index.js on exécutera toute la logique**.

> En général ce qui se passe c'est **lorsque l'on build un projet**, on va **le build dans un autre dossier** et **ce dossier là on va le partager**.
----
- Ici on n'a vu que l'on pouvait **Rajouter d'autre options et les options on peut en rajouter énormément**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-03-02%20à%2013.53.26.png)
  
> Mais si on n'ajoute toutes ses commande qui sera extrênement longue et qui sera **peu fiable**, on peut faire **une erreur de syntaxe, se tromper en oubliant de rajouter un s etc...**.

> Le mieux **lorsqu'on veut configurer une vraie compilation c'est d'avoir un fichier ts config**. Donc pour **initialiser fichier ts config on va lancer la commande avec l'option --init**. Donc ici je fais **Tsc --init**, il m'indique qu'il a correctement configuré le fichier **tsconfig.json**.

- Donc ici on le voit à la racine de notre projet **ici tsconfig.json est créé**, on peut l'ouvrir et on voit à l'intérieur c'est un contenu. Donc **json** c'est un format de données qui se **présente sous la forme d'objet**, donc on n'a des accolades, **chaque clé représente une configuration** et on n'a une **valeur** qui peut être **un tableau, un objet lui-même, une chaîne de caractère** mais **pas un nombre ni un boolean**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-03-02%20à%2014.08.20.png)
  - Donc cela est un format de données qui est couramment utilisé le **point json (.json)**
  
> Donc ici on n'a **une compilation qui est complète et extrêmement documenté**

> Ici on n'a **la target comme on n'a pu le définir, on peut avoir le es5 soit une autre taget**.

> On peut **choisir le module donc comment le code sera généré donc ici en commandeJS**, on peut choisir dans un autre module **en général on touche pas à ça**.

> Au niveau de la configuration ça va être **outDir et rootDir**, rootDir c'est ce que l'on n'avait utilisé déjà et on n'avait rajouté le **build pour outDir**, donc le dossier Build c'est à la sortie de notre compilation, ou es ce que le dossier built vont être rajouté, ils vont être rajouté dans le dossier **build**.

- On le voit c'était le dossier courant donc **la racine du projet**, maintenant opn va définir le **build**
![img.](../MESIMAGES/nodejs/Capture%20d’écran%202021-03-02%20à%2014.16.54.png)
  
- Ici **rootDir** permet de définir quelle est le chemin de notre projet, en général on le verra les projets ce trouvent souvent dans **le dossier src**, et donc on trouvent ici nos fichiers dans le dossier **Src**. Donc pour pouvoir changer la position de notre projet il faut **changer ou dire par src**.

- Dire que notre projet ne se trouve pas dans notre projet initial mais se trouve dans le projet **src**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-03-02%20à%2016.31.07.png)

> On n'a d'autre options on peut également afficher **sourceMAp** ce qui nous **permet d'avoir des informations sur notre fichier**, donc **cela est intéressant pendant le développement**, pour s'avoir ou es ce que l'erreur est affiché ou est ce que **le consolelog est affiché**. En production en général on met cela a false.

> On va avoir des options qui nous permette ici d'ajouter des **options au niveau du développement**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-03-02%20à%2016.35.59.png)

- **noImplecitAny** si on le met à **true**, on ne pourra pas typer des valeurs avec le type **Any** on ne pourra pas le faire car cela **ressortira une erreur.**
````typescript
let var1: any = "2";
````
- Donc on peut **lancer toutes ses configurations simplement en faisant un tsc**.

- **Avec la commande tsc** il va se rendre compte de **tsconfig.json** et va prendre la configuration qui est indiqué dans ce fichier là.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-03-02%20à%2016.42.40.png)
  
- Donc ici **tsc** on voit que cela c'est bien passé **il a compilé notre code dans le build**, on peut supprimer le dossier je vais clear mon terminal en faisent **ts clear** et on lance a commande **tsc** dons j'appuie sur entrer puis on voit qu'il a compilé **dans le dossier bulbes notre fichier index.ts** qui est **stipulé dans src**.
![img.gnd](../MESIMAGES/nodejs/Capture%20d’écran%202021-03-02%20à%2016.47.19.png)
  
> On voit que la configuration c'est bien déroulée et on n'a pas eu à devoir écrire toutes les options, à la place **on utilise juste la commande tsc**, et il va utiliser les **options qui sont définies dans ts config**.
> 
> LIEN DE LA VIDÉO =>[https://www.youtube.com/watch?v=gUeO7Vg3GWU&feature=youtu.be]
-----