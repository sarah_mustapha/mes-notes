# Markdown

`Il existe 6 niveaux de titre (niveau 1 à 6) on utilise le symbole #`.

 Ex:

```
# Titre niveau 1
## Titre niveau 2
### Titre nieau 3

```

# Gras

- Pour mettre le texte en **Gras** on utilise le symbole **

# Italique

- Pour mettre le texte en **Italique** on utilise le symbole *

- **citation** : ..
----
## Fonctionnement GitLab et webstorm

- **Gitlab** : Outils de Gestion de Projet Collaboratives.

- **Markdown** : Outil pour faire des notes.

- **README** : Fichier par défaut, consigne sur le projet

- **Groupe** : pour voir les projet et modif.

- **Projet** : Liste de projet auquel on a accès admin ou proprio.

- **Ripo** : Projet

- **Merge** : Demander à ce que les modifications soit sur GITLAB.

- **Commits** : Modification enregistrer sur Webstorm.

- Pour que ce soit visible sur Gitlab -> **Push**.

- **Branch** => Travailler sur une Branch pendant le travail d'équipe.

**Master** : PROPRE

----
`Chef de Projet qui assemble les branch afin de mettre le travail au propre`
## CRÉATION D'UN NOUVEAU PROJET :

>1. New Project
>2. Nom du projet
>3. Projet Pro ou Groups

----
##  RÉCUPÉRATION D'UN PROJET WEBSTORM:

- Clone (copier le lien)

- Webstrom GFVS

- Coller

----

## CRÉATION D'UNE BRANCHE WEBSTORM

1. Master

2. New Branche

3. commits

4.push

----

## RENDRE UNE BRANCH DÉFINITIVE
>
>1. gitlab
>
>2. marge request
>
>3. Supprimer la branche
>
>4. Webstorm
>
>5. Pull (flèche bleu)
>
>6. Supprimer banche dans webstorm


----
### RACCOURCI

- **Commit** : Ctrl-K

- **Push** : Ctrl-Maj-K

- **Pull** : Ctrl-T

----
## VIDÉOS DES COURS

- Créer un nouveau projet sur Gitlab -> https://youtu.be/pqF-B6ENeEE

- Récupération du projet Gitlab sur Webstorm -> https://youtu.be/Yq5cQn-cC7I

- Création des branches sur un projet (Gitlab/Webstorm) -> https://youtu.be/EfUT5zm30PA

- Les conflits lors de Merge Request -> https://youtu.be/L2X5DrDpp_w

- Prise de notes en Markdown -> https://youtu.be/UD1gUgf8-6A
----

## SITE MISE EN PAGE MARKDOWN

> Outils de mise en page -> https://www.ionos.fr/digitalguide/sites-internet/developpement-web/markdown/