# LES FLEX BOX

>**FLEXBOX** => Permet de mettre une mise en page plus en plus beau.

>Mettre un **display : flex;** ou un **display :block** dans la fiche CSS car cela nous permet d'aménager **la DIV**.
----

## Conteneur

![img.pnd](../MESIMAGES/flexboxcontenair-2.png)

- **Justify-content** -> Aligne les éléments horizontalement

- **flex-start** : Les éléments s'alignent au côté gauche du conteneur.

- **flex-end** : Les éléments s'alignent au côté droit du conteneur.

- **center** : Les éléments s'alignent au centre du conteneur.

- **space-between** : Les éléments s'affichent avec un espace égal entre eux.

- **space-around** : Les éléments s'affichent avec un espacement égal à l'entour d'eux.

- **space-evenly** : Les éléments s'affichent avec un espace identique.

> **Flex-direction** -> Cette propriété CSS définit la direction dans laquelle les éléments sont placé dans le conteneur.
----
- **row** : Les éléments sont disposés dans la même direction que le texte.

- **row-reverse** : Les éléments sont disposés dans la direction opposée au texte.

- **column** : Les éléments sont disposés de haut en bas.

- **column-reverse** : Les éléments sont disposés de bas en haut.
---
> **Flex-wrap** -> Permet de mettre les box sur la même ligne ou de les mettre sur plusieurs lignes ou d'inverser les box.

- **nowrap** : Tous les éléments sont tenus sur une seule ligne.

- **wrap** : Les éléments s'enveloppent sur plusieurs lignes au besoin.

- **wrap-reverse** : Les éléments s'enveloppent sur plusieurs lignes dans l'ordre inversé.
----
> **Flex-flow** -> RACCOURCI des deux propriétés **Flex-direction** et **Flex wrap**.


/ Rappel \ ATTENTION CE RACCOURCI ACCEPTE LES VALEURS DES DEUX PROPRIÉTÉS SÉPARÉ D'UN ESPACE

----
> **Align-items** -> Cette propriété CSS **aligne les éléments verticalement**

- **flex-start** : Les éléments **s'alignent au haut du conteneur**.

- **flex-end** : Les éléments **s'alignent au bas du conteneur**.

- **center** : Les éléments **s'alignent au centre vertical du conteneur**.

- **baseline** : Les éléments **s'alignent à la **ligne de base** du conteneur**.

- **stretch** : Les éléments sont **étirés pour s'adapter au conteneur**.
----

>**Align-content** -> Définir comment plusieurs lignes sont espacées de l'une à l'autre

- **flex-start** : Les lignes sont amassées dans **le haut** du conteneur.

- **flex-end** : Les lignes sont amassées dans **le bas** du conteneur.

- **center** : Les lignes sont amassées dans **le centre vertical** du conteneur.

- **space-between** : Les lignes s'affichent avec **un espace égal** entre eux.

- **space-around** : Les lignes s'affichent avec un espace égal autour d'eux.

- **stretch** : Les lignes sont étirées pour s'adapter au conteneur.
----
>**Ceci peut être mélangeant, mais align-content détermine l'espace entre les lignes, alors que align-items détermine comment les éléments dans leur ensemble sont alignées à l'intérieur du conteneur. Quand il n'y a qu'une seule ligne, align-content n'a aucun effet**.

----
## ITEMS

![img.pnd](../MESIMAGES/flexboxitems.png)

> **ITEMS** -> Concerne les boîtes en elle même.
---
> **Order**-> Propriété pour **inverser** les** éléments individuel**. Par défaut les éléments ont une valeur de **0**, mais on peut utiliser cette propriété pour changer la valeur à un **entier positif +** ou **négatif -**.

> **Align-self** -> Propriété pour un élément individuel. Cette propriété accepte les mêmes valeurs que align-items mais s'applique uniquement à l'élément ciblé.
----
## VIDEO ET EXERCICE DU COUR

- Les flexbox -> [https://youtu.be/CR9kKzlqNYw]

- Lien site de la vidéo -> [https://css-tricks.com/snippets/css/a-guide-to-flexbox/]

- Exercice -> [https://flexboxfroggy.com/#fr]


