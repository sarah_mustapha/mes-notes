# BOOTSTRAP

----
- Sommaire

## DEFINITION

>
>- BOOTSTRAP => est un Logiciel HTML CSS qui est le plus utilisé, mise en page de site internet baser sur une grille de 12 colonnes.

>- ROW => permet de Constituer 12 COLONNES.

>- GOUTIERE -> Entre chaque colonnes.

>- RESPONSIVE -> Mise en page qui CHANGE en fonction de l'écran

>- COMPOSANT -> Éléments de Bootstrap pré fais.

>- MEDIA QUERIES -> Ce qui permet de faire des responsive en CSS.

----

## INSTALLATION BOOTSTRAP

- [www.Getbootstrap.com]

- Getstarted

- Copier le url CSS et BUNDLE OU SEPARATE,

-Lien : [https://getbootstrap.com/]

----

## UTILISATION BOOTSTRAP

> L'utilisation de bootstrap, s'effectue en utilsant des class dans des<div></div>.
----

## GRILLES BOOTSRAP

> Une grille Bootstrap commence toujours avec un **container**.

Exemple : <div class="container"></div>

>  METTRE UN CONTAINER VA LAISSER UNE MARGE AUTOMATIQUE, POUR L'ENLEVER METTRE FLUID
Exemple : div class= "container-fluid"></div>
! ATTENTION : UN SEUL CONTAINER PAR PAGE HTML

1. **CONTAINER**

2. **ROW**

3. **COL**

>Une div.container peut inclure plusieurs (div.Row) , et une div.
Row peut contenir plusieurs div, col-.

![img.png](../MESIMAGES/container.png)

> ATTENTION : SI ON NE DÉFINIT PAS LA TAILLE DES COLS ELLES S'ADAPTENT ET PRENDRONT LA MÊME TAILLE
Exemple : Pour définir le nombre de colonnes qu'elle devra prendre mettre col- avec un chiffre.
----

# PADDING OU MARGIN
![img.png](../MESIMAGES/padding.png)

# RESPONSIVE

![img.png](../MESIMAGES/responsive.png)
----
#MEDIA QUERIES

- Mettre responsive en CSS avec media queries

![img.pnd](../MESIMAGES/mediaq.png)
> ATTENTION: EN CSS ON PEUT METTRE LE MINIMUM LE MAXIMUM OU ENTRE LES DEUX


### PROPRIETE IMPORTANTE A SAVOIR
> 
**h-100** : Garde la col sur toute la ligne du row

**w-100** : Retour à la ligne

**/ ! \ important** : Mettre une propriété css a la place de celle de bootstrap

**goutière** : Mettre des goutière avec padding et margin

----

# COULEUR BOOTSTRAP

![img.pnd](../MESIMAGES/couleur.png)

> ATTENTION CE SONT LES MEME COULEUR QUI PEUVENT ÊTRE UTILISÉ POUR DU TEXTE EN CHANGEANT LE BG- PAR TEXT-

---

#VIDEO DU COUR

- Bootstrap installation et grid ->[https://youtu.be/2LVG_Q91HKs]

- Bootstrap components et media queries -> [https://youtu.be/Na7Cmm_UjAo]

### LIENS UTILES
```
[https://openclassrooms.com/fr/courses/6391096-creez-des-sites-web-responsive-avec-bootstrap-4]
[https://mdbootstrap.com/docs/]