# Bases

# Introduction

- 5 Tutorials sur (www.youtube.com) with explication

- Creation du compute **GIFT**

- Test first project

- Test second project

- **Push** and **commit** projects

- Creation et suppression des **BRANCHES**

- **Application des consignees then screened**

# La Master

- clean page
----

## CRÉATION D'UN PROJECT

- Création du Compte **GITLAB** sur  lien} (www.gitlab.com)

- Télécharger le logiciel **WEBSTORM**

- Créé un Project dans **GITLAB**

- Copier le **URL**

- Coller le **URL** dans **WEBSTORM**

- Ouvrir le projet dans **WEBSTORM** que l'on n'a créé dans **GITLAB**

- Créé une fiche **HTML** et **CSS** en faisant **CLICK DROIT** dans son fichier.

- Cliqué dur son **FICHIER** click droit sur **HTM** et cliqué sur **STYLESHEET**.

- Faire une **BALISE LINK* pour que le **HTML** lit le **CSS**
----

## Vidéo

- Les bases du HTML -> [https://www.youtube.com/watch?v=tXBF1el1yfA&list=PLDeebGAUwdpHqeKK5wm8X77K1dCBs0tLM&index=1]

- Intégré une google font -> [https://youtu.be/VZMGCRJJc5E]

----
## Les vidéos des cours

- Créer un nouveau projet sur Gitlab -> [https://youtu.be/pqF-B6ENeEE]


- Recuperation de projet Gitlab sur Webstorm -> [https://youtu.be/Yq5cQn-cC7I]

- Créer des branches sur un projet (Gitlab/Webstorm) -> [https://youtu.be/EfUT5zm30PA]

- Les conflits lors de Merge Request -> [https://youtu.be/L2X5DrDpp_w]

- Prise de notes en Markdown -> [https://youtu.be/UD1gUgf8-6A]

# SITE MISE EN PAGE MARKDOWN

- Outils de mise en page -> [https://www.ionos.fr/digitalguide/sites-internet/developpement-web/markdown/]

## COMPOSITION HTML / SIGNIFICATION


####< Le *BODY* est constitué de 3 parties (header/corp/footer )

- **Header** = La tête de la page

- **Body** = le corp de la page

- **Footer** = le bas de la page
----
## BALISE HTML

- **h1** = le Titre

- **img** = L'image

- **a** = Le Lien

- **b** = Le Gras

- **Strong** = Le gras dans une phrase

- **div** = La section

- **p** = Le Texte

- **href** = L'URL

- **alt** = Description de la Photo

- **ul / li** = Liste Non Numéroter

- **ol /li** = Liste Numéroter

----

## COMPOSITON DU CSS / UTILITÉ

####< Le **CSS** permet de **STYLISÉ** mette de la **COULEUR** mettre en **ITALIQUE**

## PROPRIÉTÉ ET VALEURS CSS (de base)

- **color**

- **font-family** = La Police

- **font-size** = La Taille

- **class** = Propriété différente

- **font-weight** = La GRAISSE de la Police

- **widht** = La Largeur

- **height** = La Hauteur

- **background-color** = Couleur dans un Texte

- **border** : bordure taille état couleur 
  
- **border radius** : arrondir les angles 

- **padding** : marge à l'intérieur 
  
- **margin** : marge à l'extérieur 
  
- **text-decoration** : souligne 
  
- **box-shadow** : mettre une ombre 
  
- **a-hover** : changer la couleur du lien
  
- **transition** : en seconde
  
- **list-style-type** : enlever les puce de liste par défaut mettre (none)

----
### Liens des videos

 Les bases du CSS partie 1 : [https://www.youtube.com/watch?v=X6imkF9sASg&list=PLDeebGAUwdpHqeKK5wm8X77K1dCBs0tLM&index=2]


 Les bases du CSS partie 2 : [https://www.youtube.com/watch?v=YJuGCcSS9OI&list=PLDeebGAUwdpHqeKK5wm8X77K1dCBs0tLM&index=3]
``````

