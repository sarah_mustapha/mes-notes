# Mes notes

## Gitlab html / css

- [Introduction Markdown](base%20gitlab,html,css/markdown,note.md)

- [Bases gitlab html / css](base gitlab,html,css/basesgitlab.md)

- [Introduction Flexbox](base%20gitlab,html,css/flexbox.md)

- [Introduction Bootstrap](base%20gitlab,html,css/bootsrap.md)

----

## Javascript

- [Introduction javascript](javascript/notejavascript.md)

- [Introduction String](javascript/string.md)

- [Introduction Array](javascript/Array.md)

- [Introduction condition](javascript/condition,if,while.md)

- [Introduction Fonction](javascript/LesFONCTIONS.md)

- [Introduction Objects](javascript/LesObjects.md)

- [Introduction Tableaux,spread](javascript/manipulation,tableaux,spread.md)
  
- [Introduction Les Classes](javascript/LesClasses.md)
  
- [Introduction Les DOM](javascript/LesDom.md)

----

## Node JS et Typescript 

- [Node JS et Typescript](Nodejs,Typescript/typescript.md)

-----

## Angular

- [Introduction Arrow function](Angular/Arrow%20function.md)

